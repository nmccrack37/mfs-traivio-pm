package com.cobrapm.forms;

public class FormDetailLayoutRecord {
	public int ColumnSeq;
	public int LayoutSeq;
	public int PropSeq;
	
	public FormDetailLayoutRecord(
					int ColumnSeqin,
					int LayoutSeqin,
					int PropSeqin){
		ColumnSeq = ColumnSeqin;
		LayoutSeq = LayoutSeqin;
		PropSeq = PropSeqin;
	}

	public FormDetailLayoutRecord() {
		ColumnSeq = 0;
		LayoutSeq = 0;
		PropSeq = 0;
	}	
}
