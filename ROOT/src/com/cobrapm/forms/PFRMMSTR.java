package com.cobrapm.forms;

import java.util.ArrayList;
import java.util.List;


public class PFRMMSTR {
	public int FRID;
	public String FRNAME;
	public String FREXTNL;
	public String FREXTHSH;
	public String FRPORT;
	public String FRPORTID;
	public int FRPORTLBL;
	public boolean FRPASSIGN;
    public List<PFRMPROP> PFRMPROP;	
    public ArrayList<FormLayoutRecord> layouts;
	
	public PFRMMSTR(int FRIDin,
					String FRNAMEin,
					String FREXTNLin,
					String FREXTHSHin,
					String FRPORTin,
					String FRPORTIDin,
					int FRPORTLBLin,
					boolean FRPASSIGNin,
					List<PFRMPROP> PFRMPROPin,
					ArrayList<FormLayoutRecord> layoutsin){
		FRID = FRIDin;
		FRNAME = FRNAMEin;
		FREXTNL = FREXTNLin;
		FREXTHSH = FREXTHSHin;
		FRPORT = FRPORTin;
		FRPORTID = FRPORTIDin;
		FRPORTLBL = FRPORTLBLin;
		FRPASSIGN = FRPASSIGNin;
		PFRMPROP = PFRMPROPin;
		layouts = layoutsin;
	}

	public PFRMMSTR() {
		FRID = 0;
		FRNAME = "";
		FREXTNL = "";
		FREXTHSH = "";
		FRPORT = "";
		FRPORTID = "";
		FRPORTLBL = 0;
		FRPASSIGN = false;
		PFRMPROP = null;
		layouts = new ArrayList<FormLayoutRecord>();
	}	
}
