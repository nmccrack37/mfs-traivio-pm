package com.cobrapm.forms;

import java.util.List;


public class PFRMRESM {
	public PFRMMSTR PFRMMSTR;
	public List<PFRMRESP> PFRMRESP;
	
	public PFRMRESM(PFRMMSTR PFRMMSTRin, 
					List<PFRMRESP> PFRMRESPin,
					String libin){
		PFRMMSTR = PFRMMSTRin;
		PFRMRESP = PFRMRESPin;
	}

	public PFRMRESM() {
		PFRMMSTR = null;
		PFRMRESP = null;
	}
}
