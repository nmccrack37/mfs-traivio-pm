package com.cobrapm.forms;


public class PFRMRESP {
	
	public int FEID;
	public int FERID;
	public int FESEQ;
	public String FEDATA;   
	public String FSSTAMP;
	public String FSNAME;
	public String FSEMAIL;
	
	public PFRMRESP(int FEIDin, 
				    int FERIDin,
				    int FESEQEin,
				    String FEDATAin,
				    String FSSTAMPin,
				    String FSNAMEin,
				    String FSEMAILin){
		FEID = FEIDin;
		FERID = FERIDin;
		FESEQ = FESEQEin;
		FEDATA = FEDATAin;
		FSSTAMP = FSSTAMPin;
		FSNAME = FSNAMEin;
		FSEMAIL = FSEMAILin;
	}

	public PFRMRESP() {
		FEID = 0;
		FERID = 0;
		FESEQ = 0;
		FEDATA = "";
		FSSTAMP = "";
		FSNAME = "";
		FSEMAIL = "";
	}
}
