package com.cobrapm.process;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"selected","EstCompDateFmt",})

public class ProcessMainRecord {
	public int TKNO;
	public String TKTYPE;
	public String AssignedTo;
	public String ShortDesc;
	public String LongDesc;
	public String SubType;
	public String Status;
	public String Freq;
	public String Rollover;
	public String RolloverFreq;
	public String OddMthsFreq;
	public String Approved;
	public BigDecimal Amount;
	public String CaseNumber;
	public String Severity;
	public String EstStartDate;
	public String ActStartDate;
	public String SynchronizeYN;
	public String EstCompDate;
	public String ActCompDate;
	public int ExpDuration;
	public int ActDuration;
	public String CompBeforeRoll;
	public int PreviousTKNO;
	public int PRID;
	public String Closed;
	public String NextProcess;
	public String NextProcConfirm;
	public String Cat;
	public String SubCat;
	public int RecurInterval;
	public int WeekDay;
	public String Depend;
	public int Days;
	public int DayofMonth;
	public String SYSCRT;
	public String SYSUPD;
	public String SYSCRTBY;
	public String SYSUPDBY;
	public String PGMCRTBY;
	public String PGMUPDBY;
	public String ProcessType;
	public String NextStepEstCompDate;
	public String NextStepActCompDate;
	public String RecentNote;
	public String AssignedBy;
	public String AssignedByTime;
	public String StopRecurDate;
	public String OriginalUser;
	public String OriginalUserTime;
	public String CurUser;
	public String CurUserTime;
	public int RecurMonth;
	public int NumOfSubProc;
	public int SubTKNO;
	public int SubTSKNO;
	public int SubTSKSNO;
	public BigDecimal Estimate;
	public String EstHoursOvrride;
	public String LastUpdateBy;
	public String CreatedBy;
	public String SubCat2;
	public String SubCat3;
	public String ProcessSubType;
	public String Category;
	public String SubCategory;
	public String SubCategory2;
	public String SubCategory3;
	public String Frequency;
	public String Mode;
	public String newNote;
	public ArrayList<ProcessTaskRecord> Tasks;
	public ArrayList<PPMREL> relTos;
	public ArrayList<UDFData> UDFs;
	
	public ProcessMainRecord(	int TKNOin,
								String TKTYPEin,
								String AssignedToin,
								String ShortDescin,
								String LongDescin,
								String SubTypein,
								String Statusin,
								String Freqin,
								String Rolloverin,
								String RolloverFreqin,
								String OddMthsFreqin,
								String Approvedin,
								BigDecimal Amountin,
								String CaseNumberin,
								String Severityin,
								String EstStartDatein,
								String ActStartDatein,
								String SynchronizeYNin,
								String EstCompDatein,
								String ActCompDatein,
								int ExpDurationin,
								int ActDurationin,
								String CompBeforeRollin,
								int PreviousTKNOin,
								int PRIDin,
								String Closedin,
								String NextProcessin,
								String NextProcConfirmin,
								String Catin,
								String SubCatin,
								int RecurIntervalin,
								int WeekDayin,
								String Dependin,
								int Daysin,
								int DayofMonthin,
								String SYSCRTin,
								String SYSUPDin,
								String SYSCRTBYin,
								String SYSUPDBYin,
								String PGMCRTBYin,
								String PGMUPDBYin,
								String ProcessTypein,
								String NextStepEstCompDatein,
								String NextStepActCompDatein,
								String RecentNotein,
								String AssignedByin,
								String AssignedByTimein,
								String StopRecurDatein,
								String OriginalUserin,
								String OriginalUserTimein,
								String CurUserin,
								String CurUserTimein,
								int RecurMonthin,
								int NumOfSubProcin,
								int SubTKNOin,
								int SubTSKNOin,
								int SubTSKSNOin,
								BigDecimal Estimatein,
								String EstHoursOvrridein,
								String LastUpdateByin,
								String CreatedByin,
								String SubCat2in,
								String SubCat3in,
								String ProcessSubTypein,
								String Categoryin,
								String SubCategoryin,
								String SubCategory2in,
								String SubCategory3in,
								String Frequencyin,
								String Modein,
								String newNotein,
								ArrayList<ProcessTaskRecord> Tasksin,
								ArrayList<PPMREL> relTosin,
								ArrayList<UDFData> UDFsin){
		
		TKNO = TKNOin;
		TKTYPE = TKTYPEin;
		AssignedTo = AssignedToin;
		ShortDesc = ShortDescin;
		LongDesc = LongDescin;
		SubType = SubTypein;
		Status = Statusin;
		Freq = Freqin;
		Rollover = Rolloverin;
		RolloverFreq = RolloverFreqin;
		OddMthsFreq = OddMthsFreqin;
		Approved = Approvedin;
		Amount = Amountin;
		CaseNumber = CaseNumberin;
		Severity = Severityin;
		EstStartDate = EstStartDatein;
		ActStartDate = ActStartDatein;
		SynchronizeYN = SynchronizeYNin;
		EstCompDate = EstCompDatein;
		ActCompDate = ActCompDatein;
		ExpDuration = ExpDurationin;
		ActDuration = ActDurationin;
		CompBeforeRoll = CompBeforeRollin;
		PreviousTKNO = PreviousTKNOin;
		PRID = PRIDin;
		Closed = Closedin;
		NextProcess = NextProcessin;
		NextProcConfirm = NextProcConfirmin;
		Cat = Catin;
		SubCat = SubCatin;
		RecurInterval = RecurIntervalin;
		WeekDay = WeekDayin;
		Depend = Dependin;
		Days = Daysin;
		DayofMonth = DayofMonthin;
		SYSCRT = SYSCRTin;
		SYSUPD = SYSUPDin;
		SYSCRTBY = SYSCRTBYin;
		SYSUPDBY = SYSUPDBYin;
		PGMCRTBY = PGMCRTBYin;
		PGMUPDBY = PGMUPDBYin;
		ProcessType = ProcessTypein;
		NextStepEstCompDate = NextStepEstCompDatein;
		NextStepActCompDate = NextStepActCompDatein;
		RecentNote = RecentNotein;
		AssignedBy = AssignedByin;
		AssignedByTime = AssignedByTimein;
		StopRecurDate = StopRecurDatein;
		OriginalUser = OriginalUserin;
		OriginalUserTime = OriginalUserTimein;
		CurUser = CurUserin;
		CurUserTime = CurUserTimein;
		RecurMonth = RecurMonthin;
		NumOfSubProc = NumOfSubProcin;
		SubTKNO = SubTKNOin;
		SubTSKNO = SubTSKNOin;
		SubTSKSNO = SubTSKSNOin;
		Estimate = Estimatein;
		EstHoursOvrride = EstHoursOvrridein;
		LastUpdateBy = LastUpdateByin;
		CreatedBy = CreatedByin;
		SubCat2 = SubCat2in;
		SubCat3 = SubCat3in;
		ProcessSubType = ProcessSubTypein;
		Category = Categoryin;
		SubCategory = SubCategoryin;
		SubCategory2 = SubCategory2in;
		SubCategory3 = SubCategory3in;
		Frequency = Frequencyin;
		Mode = Modein;
		newNote = newNotein;
		Tasks = Tasksin;
		relTos = relTosin;
		UDFs = UDFsin;
	}

	public ProcessMainRecord() {
		TKNO = 0;
		TKTYPE = "";
		AssignedTo = "";
		ShortDesc = "";
		LongDesc = "";
		SubType = "";
		Status = "";
		Freq = "";
		Rollover = "";
		RolloverFreq = "";
		OddMthsFreq = "";
		Approved = "";
		Amount = BigDecimal.ZERO;
		CaseNumber = "";
		Severity = "";
		EstStartDate = "";
		ActStartDate = "";
		SynchronizeYN = "";
		EstCompDate = "";
		ActCompDate = "";
		ExpDuration = 0;
		ActDuration = 0;
		CompBeforeRoll = "";
		PreviousTKNO = 0;
		PRID = 0;
		Closed = "";
		NextProcess = "";
		NextProcConfirm = "";
		Cat = "";
		SubCat = "";
		RecurInterval = 0;
		WeekDay = 0;
		Depend = "";
		Days = 0;
		DayofMonth = 0;
		SYSCRT = "";
		SYSUPD = "";
		SYSCRTBY = "";
		SYSUPDBY = "";
		PGMCRTBY = "";
		PGMUPDBY = "";
		ProcessType = "";
		NextStepEstCompDate = "";
		NextStepActCompDate = "";
		RecentNote = "";
		AssignedBy = "";
		AssignedByTime = "";
		StopRecurDate = "";
		OriginalUser = "";
		OriginalUserTime = "";
		CurUser = "";
		CurUserTime = "";
		RecurMonth = 0;
		NumOfSubProc = 0;
		SubTKNO = 0;
		SubTSKNO = 0;
		SubTSKSNO = 0;
		Estimate = BigDecimal.ZERO;
		EstHoursOvrride = "";
		LastUpdateBy = "";
		CreatedBy = "";
		SubCat2 = "";
		SubCat3 = "";
		ProcessSubType = "";
		Category = "";
		SubCategory = "";
		SubCategory2 = "";
		SubCategory3 = "";
		Frequency = "";
		Mode = "";
		newNote = "";
		Tasks = new ArrayList<ProcessTaskRecord>();
		relTos = new ArrayList<PPMREL>();
		UDFs = new ArrayList<UDFData>();
	}
}
