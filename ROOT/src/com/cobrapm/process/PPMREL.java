package com.cobrapm.process;

public class PPMREL {
	
	public int TLTKNO;
	public int TLTSKNO;
	public int TLTSKSNO;   
	public String RecType;
	public int RecID;
	public int RelID;
	public String Desc;
	
	public PPMREL(	int TLTKNOin, 
				    int TLTSKNOin,
				    int TLTSKSNOin,
				    String RecTypein,
				    int RecIDin,
				    int RelIDin,
				    String Descin){
		TLTKNO = TLTKNOin;
		TLTSKNO = TLTSKNOin;
		TLTSKSNO = TLTSKSNOin;
		RecType = RecTypein;
		RecID = RecIDin;
		RelID = RelIDin;
		Desc = Descin;
	}

	public PPMREL() {
		TLTKNO = 0;
		TLTSKNO = 0;
		TLTSKSNO = 0;
		RecType = "";
		RecID = 0;
		RelID = 0;
		Desc = "";
	}
}
