package com.cobrapm.doa;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"checked"})
public class UDFRecord {
	public int USEQ;
	public String UNAMID;
	public String UTYPE;
	public String UDISP;
	public String UREQ;
	public String UWIDTH;
	
	public UDFRecord(int USEQin,
					String UNAMIDin,
					String UTYPEin,
					String UDISPin,
					String UREQin,
					String UWIDTHin){
		USEQ = USEQin;
		UNAMID = UNAMIDin;
		UTYPE = UTYPEin;
		UDISP = UDISPin;
		UREQ = UREQin;
		UWIDTH = UWIDTHin;
	}

	public UDFRecord() {
		USEQ = 0;
		UNAMID = "";
		UTYPE = "";
		UDISP = "";
		UREQ = "";
		UWIDTH = "";
	}	
}
