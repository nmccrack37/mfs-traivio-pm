package com.cobrapm.doa;

import org.joda.time.DateTime;

public class ProcessCreateRecord {
	public String Title;
	public String PorT;
	public String Type;
	public String Assigned;
	public DateTime Start;
	public DateTime Due;
	public DateTime Remind;
	public int TKNO;
	public int TSTSKNO;
	public int TSTSKSNO;
	
	public ProcessCreateRecord(	String Titlein,
								String PorTin,
								String Typein,
								String Assignedin,
								DateTime Startin,
								DateTime Duein,
								DateTime Remindin,
								int TKNOin,
								int TSTSKNOin,
								int TSTSKSNOin){
		
		Title = Titlein;
		PorT = PorTin;
		Type = Typein;
		Assigned = Assignedin;
		Start = Startin;
		Due = Duein;
		Remind = Remindin;
		TKNO = TKNOin;
		TSTSKNO = TSTSKNOin;
		TSTSKSNO = TSTSKSNOin;
	}

	public ProcessCreateRecord() {
		Title = "";
		PorT = "";
		Type = "";
		Assigned = "";
		Start = null;
		Due = null;
		Remind = null;
		TKNO = 0;
		TSTSKNO = 0;
		TSTSKSNO = 0;
	}
}
