package com.cobrapm.doa;

public class NotOptionRecord {
	public boolean NotifyYN;
	public boolean InAppYN;
	public boolean EmailYN;
	
	public NotOptionRecord(
			boolean NotifyYNin,
			boolean InAppYNin,
			boolean EmailYNin){
		NotifyYN = NotifyYNin;
		InAppYN = InAppYNin;
		EmailYN = EmailYNin;
	}

	public NotOptionRecord() {
		NotifyYN = false;
		InAppYN = false;
		EmailYN = false;
	}	
}
