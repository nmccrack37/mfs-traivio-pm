package com.cobrapm.doa;

import java.util.ArrayList;

import org.codehaus.jackson.type.TypeReference;


public class GroupRecord {
	public String GRPNAME;
	public ArrayList<String> GRPMEMBERs;
	public Boolean selected;
	public String origName;
	
	public GroupRecord(	String GRPNAMEin,
						ArrayList<String> GRPMEMBERin,
						Boolean selectedin,
						String origNamein){
		
		GRPNAME = GRPNAMEin;
		GRPMEMBERs = GRPMEMBERin;
		selected = selectedin;
		origName = origNamein;
	}

	public GroupRecord() {
		GRPNAME = "";
		GRPMEMBERs = null;
		selected = false;
		origName = "";
	}
}
