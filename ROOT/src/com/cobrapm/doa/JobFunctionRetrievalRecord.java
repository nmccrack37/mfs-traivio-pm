package com.cobrapm.doa;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.cobrapm.authentication.SecurityRecord;

@JsonIgnoreProperties({"selected","isAdmin","isProcess","isNotes","isForms"})
public class JobFunctionRetrievalRecord {

	public String KAJOBROLE;
	public String KAJOBDESC;
	public String KAMANGR;
	public String GRPNAME;
	public String DisplayText;
	public SecurityRecord taskRights;
	
	public JobFunctionRetrievalRecord(	
									String KAJOBROLEin,
									String KAJOBDESCin,
									String KAMANGRin,
									String GRPNAMEin,
									String DisplayTextin,
									SecurityRecord taskRightsin){
		
		KAJOBROLE = KAJOBROLEin;
		KAJOBDESC = KAJOBDESCin;
		KAMANGR = KAMANGRin;
		GRPNAME = GRPNAMEin;
		DisplayText = DisplayTextin;
		taskRights = taskRightsin;
	}

	public JobFunctionRetrievalRecord() {
		KAJOBROLE = "";
		KAJOBDESC = "";
		KAMANGR = "";
		GRPNAME = "";
		DisplayText = "";
		taskRights = null;
	}
}
