package com.cobrapm.doa;

public class TaskTypeRecord {

	public String TSTYPE;
	public String TSTYPED;
	public int TSDYCMP;
	
	public TaskTypeRecord(	String TSTYPEin,
							String TSTYPEDin,
							int TSDYCMPin){
		
		TSTYPE = TSTYPEin;
		TSTYPED = TSTYPEDin;
		TSDYCMP = TSDYCMPin;
	}

	public TaskTypeRecord() {
		TSTYPE = "";
		TSTYPED = "";
		TSDYCMP = 0;
	}
}
