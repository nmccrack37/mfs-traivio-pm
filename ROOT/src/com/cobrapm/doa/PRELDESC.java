package com.cobrapm.doa;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"$$hashKey"})
public class PRELDESC {
	
	public int RECID;
	public int PRDID;
	public String PRDESC;   
	
	public PRELDESC(int RECIDin, 
				    int PRDIDin,
				    String PRDESCin){
		RECID = RECIDin;
		PRDID = PRDIDin;
		PRDESC = PRDESCin;
	}

	public PRELDESC() {
		RECID = 0;
		PRDID = 0;
		PRDESC = "";
	}
}
