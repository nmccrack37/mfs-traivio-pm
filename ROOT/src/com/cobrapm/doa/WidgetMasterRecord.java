package com.cobrapm.doa;

import java.util.ArrayList;

public class WidgetMasterRecord {
	public int SeqNum;
	public String Title;
	public String PorT;
	public String Assigned;
	public String Role;
	public String ProcessType;
	public String Cat;
	public String Cat1;
	public String Cat2;
	public String Cat3;
	public String Color;
	public String Mode;
	public String ShowNotStart;
	public int Count;
	public int CountOverdue;
	public int CountDueToday;
	public boolean isDisabled;
	public int status;
	public int Order;
	public ArrayList<UserDisplayRecord> columns;
	
	public WidgetMasterRecord(	int SeqNumin,
								String Titlein,
								String PorTin,
								String Assignedin,
								String Rolein,
								String ProcessTypein,
								String Catin,
								String Cat1in,
								String Cat2in,
								String Cat3in,
								String Colorin,
								String Modein,
								String ShowNotStartin,
								int Countin,
								int CountOverduein,
								int CountDueTodayin,
								boolean isDisabledin,
								int statusin,
								int Orderin,
								ArrayList<UserDisplayRecord> columnsin){
		
		SeqNum = SeqNumin;
		Title = Titlein;
		PorT = PorTin;
		Assigned = Assignedin;
		Role = Rolein;
		ProcessType = ProcessTypein;
		Cat = Catin;
		Cat1 = Cat1in;
		Cat2 = Cat2in;
		Cat3 = Cat3in;
		Color = Colorin;
		Mode = Modein;
		ShowNotStart = ShowNotStartin;
		Count = Countin;
		CountOverdue = CountOverduein;
		CountDueToday = CountDueTodayin;
		isDisabled = isDisabledin;
		status = statusin;
		Order = Orderin;
		columns = columnsin;
	}

	public WidgetMasterRecord() {
		SeqNum = 0;
		Title = "";
		PorT = "";
		Assigned = "";
		Role = "";
		ProcessType = "";
		Cat = "";
		Cat1 = "";
		Cat2 = "";
		Cat3 = "";
		Color = "";
		Mode = "";
		ShowNotStart = "";
		Count = 0;
		CountOverdue = 0;
		CountDueToday = 0;
		isDisabled = false;
		status = 0;
		Order = 0;
		columns = new ArrayList<UserDisplayRecord>();
	}
}
