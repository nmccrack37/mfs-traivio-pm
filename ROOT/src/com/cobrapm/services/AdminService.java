package com.cobrapm.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.AuthenticationHelper;
import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Encrypt;
import com.cobrapm.authentication.SecurityRecord;
import com.cobrapm.doa.DefinedFieldsRecord;
import com.cobrapm.doa.GroupRecord;
import com.cobrapm.doa.JobFunctionRetrievalRecord;
import com.cobrapm.doa.NotOptionRecord;
import com.cobrapm.doa.NotificationRecord;
import com.cobrapm.doa.PINFORecord;
import com.cobrapm.doa.PRELDESC;
import com.cobrapm.doa.ProcessTypeRecord;
import com.cobrapm.doa.TaskTypeRecord;
import com.cobrapm.doa.UDFRecord;
import com.cobrapm.doa.UserDisplayRecord;
import com.cobrapm.forms.PFRMMSTR;
import com.cobrapm.forms.PFRMPROP;
import com.cobrapm.forms.PFRMRESM;
import com.cobrapm.forms.PFRMRESP;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
   
@Path("AdminService")  
public class AdminService { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private AuthenticationHelper authenticate = new AuthenticationHelper();
	private static Logger log = Logger.getLogger(AdminService.class.getName());
	
     @GET  
     @Path("/loadGroups/{datasource}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String loadGroups(@PathParam("datasource") String lib) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	ArrayList<GroupRecord> GROUPs = new ArrayList<GroupRecord>();
    	ArrayList<JobFunctionRetrievalRecord> JOBs = new ArrayList<JobFunctionRetrievalRecord>();
    	
    	String json = "";
    	String json2 = "";
    	String jsonAll = "";
    	try{
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn; 		    			
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}
    	try{
    	
    	log.info("...retrieving all groups");
		
		CallableStatement stmt = null;
	    String query = "CALL SPRLGPCR00()";	
	    
	    try {
	        stmt = conn.prepareCall(query);
	        stmt.execute();	
	        
	        ResultSet rs = stmt.getResultSet();
		
	        while(rs.next()){
	        	GroupRecord Group = new GroupRecord();
	        	Group.GRPNAME = rs.getString(1);
	        	Group.origName = rs.getString(1);
	        	GROUPs.add(Group);
	        }

	    } catch (SQLException e ) {
		   	log.warn(e);
		} finally {
		    if (stmt != null) { stmt.close(); }
		}	
    	
    	log.info("...retrieving job functions");
		
		stmt = null;
	    query = "CALL SPRLJFCR00()";	
	    
	    try {
	        stmt = conn.prepareCall(query);
	        stmt.execute();	
	        
	        ResultSet rs = stmt.getResultSet();
		
	        while(rs.next()){
	        	JobFunctionRetrievalRecord JOB = new JobFunctionRetrievalRecord();
	        	JOB.KAJOBROLE = rs.getString(1);
				JOB.KAJOBDESC =  rs.getString(2);
				JOB.GRPNAME =  rs.getString(4);
				JOBs.add(JOB);
	        }

	    } catch (SQLException e ) {
		   	log.warn(e);
		} finally {
		    if (stmt != null) { stmt.close(); }
		}
	    
	    for(GroupRecord grpTemp: GROUPs){
	    	grpTemp.GRPMEMBERs = new ArrayList<String>();
    		for(JobFunctionRetrievalRecord jobTmp: JOBs){
    			if(jobTmp.GRPNAME != null){
    				if(jobTmp.GRPNAME.equals(grpTemp.GRPNAME)){
    					grpTemp.GRPMEMBERs.add(jobTmp.KAJOBDESC);
    				}
    			}
    		}
    	}
	    
	    for(JobFunctionRetrievalRecord jobTmp: JOBs){
	    	stmt = null;
		    query = "CALL SPSECURR00(?)";	
		    
		    try {
		    	stmt = conn.prepareCall(query);
			    stmt.setString(1, jobTmp.KAJOBROLE);
			    stmt.execute();	
			        
			    ResultSet rs = stmt.getResultSet();
					        
			        
			    SecurityRecord resultTaskRight = new SecurityRecord();	
			        
			    while (rs.next()) {		        		 
//			    	SecurityRecord resultTaskRight = new SecurityRecord();
				    switch(rs.getString(1).trim()){
				    	case "ADM" :
				    		resultTaskRight.ADM = rs.getString(2);
				    		break;
				    	case "PROC" :
				    		resultTaskRight.PROC = rs.getString(2);
				    		break;
				    	case "PNP" :
				    		resultTaskRight.PNP = rs.getString(2);
				    		break;
				    	case "PNT" :
				    		resultTaskRight.PNT = rs.getString(2);
				    		break;
				    	case "PPTM" :
				    		resultTaskRight.PPTM = rs.getString(2);
				    		break;
				    	case "PTTM" :
				    		resultTaskRight.PTTM = rs.getString(2);
				    		break;
				    	case "PSI" :
				    		resultTaskRight.PSI = rs.getString(2);
				    		break;
				    	case "PPC" :
				    		resultTaskRight.PPC = rs.getString(2);
				    		break;
				    	case "PEPM" :
				    		resultTaskRight.PEPM = rs.getString(2);
				    		break;
				    	case "PREA" :
				    		resultTaskRight.PREA = rs.getString(2);
				    		break;
				    	case "NOTE" :
				    		resultTaskRight.NOTE = rs.getString(2);
				    		break;
				    	case "NOTM" :
				    		resultTaskRight.NOTM = rs.getString(2);
				    		break;
				    	case "FORM" :
				    		resultTaskRight.FORM = rs.getString(2);
				    		break;
				    	case "FRMM" :
				    		resultTaskRight.FRMM = rs.getString(2);
				    		break;
				    }          
				}   
			    jobTmp.taskRights = resultTaskRight;
			        
			    rs.close();

		    } catch (SQLException e ) {
			   	log.warn(e);
			} finally {
			    if (stmt != null) { stmt.close(); }
			}
	    }
	    
	    json = new Gson().toJson(GROUPs);
	    json2 = new Gson().toJson(JOBs);
	    jsonAll = "["+json+","+json2+"]";
     	} catch (SQLException e ) {
	    	log.warn(e);
	    }
		return jsonAll;
      }
     
     @POST
 	@Path("/updateSecurity")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String updateSecurity(String masterString){
 	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		ArrayList<JobFunctionRetrievalRecord> masterRecords = new ArrayList<JobFunctionRetrievalRecord>();
// 		Encrypt encrypt = new Encrypt();
 		
 		log.info("..mapping JSON string");
// 		log.info("..." + masterString);
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
// 	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 	    String users_tmp = jobject.get("users").toString();
 		
     	ObjectMapper mapper = new ObjectMapper();
     	try {
     		 masterRecords = mapper.readValue(users_tmp, new TypeReference<ArrayList<JobFunctionRetrievalRecord>>(){});
 		} catch (JsonParseException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (JsonMappingException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (IOException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
 		
 		String json = "";
 		
 	    try
 	    	{	    		    	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..calling SP: SPSECURU00 to updateSecurity");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPSECURU00(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
 			   try {
 				  stmt = conn.prepareCall(query);
 			    for(JobFunctionRetrievalRecord masterRecord: masterRecords){

 			        stmt.setString(1, masterRecord.KAJOBROLE);
 			        stmt.setString(2, masterRecord.taskRights.ADM);
 			        stmt.setString(3, masterRecord.taskRights.PROC);
 			        stmt.setString(4, masterRecord.taskRights.PNP);
 			        stmt.setString(5, masterRecord.taskRights.PNT);
 			        stmt.setString(6, masterRecord.taskRights.PPTM);
 			        stmt.setString(7, masterRecord.taskRights.PTTM);
 			        stmt.setString(8, masterRecord.taskRights.PSI);
 			        stmt.setString(9, masterRecord.taskRights.PPC);
 			        stmt.setString(10,masterRecord.taskRights.PEPM);
 			        stmt.setString(11,masterRecord.taskRights.PREA);
 			        stmt.setString(12,masterRecord.taskRights.NOTE);
 			        stmt.setString(13,masterRecord.taskRights.NOTM);
 			        stmt.setString(14,masterRecord.taskRights.FORM);
 			        stmt.setString(15,masterRecord.taskRights.FRMM);

 			        stmt.executeUpdate();			        
 			    }	
 			        stmt.close();
 			        
 			        
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			        if (stmt != null) { stmt.close(); }
 			    }
 			    
 			 connectionHelper.closeConnection(conn);
 	    	}
 	    
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}

    @POST
 	@Path("/getSecurity")
    @Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String getSecurity(String masterString){
 		CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		SecurityRecord securityRecord = new SecurityRecord();
 		
 		String json = "";
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = "";
 	   String userid = "";
 	   try
    	{	
 		   lib = jobject.get("datasource").toString();
 		   lib = lib.substring(1, lib.length()-1);
 		   userid = jobject.get("userid").toString();
 	    	userid = userid.substring(1, userid.length()-1);
    	}catch(Exception ex){
    		return json;
    	}
 		
 		if(lib.equals("undefined")){
 			return json;
 		}
 		
 	    try
 	    	{	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..calling SP: SPSECURR00");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPSECURR00(?)";
 			    
 			    try {
 			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

 			        stmt.setString(1, userid);
 			       
 			        stmt.execute();			        
 			        
 			        ResultSet rs = stmt.getResultSet();
 			        
// 			       SecurityRecord resultTaskRight = new SecurityRecord();	
 			        
 			        while (rs.next()) {
 			        	switch(rs.getString(1).trim()){
				    	case "ADM" :
				    		securityRecord.ADM = rs.getString(2);
				    		break;
				    	case "PROC" :
				    		securityRecord.PROC = rs.getString(2);
				    		break;
				    	case "PNP" :
				    		securityRecord.PNP = rs.getString(2);
				    		break;
				    	case "PNT" :
				    		securityRecord.PNT = rs.getString(2);
				    		break;
				    	case "PPTM" :
				    		securityRecord.PPTM = rs.getString(2);
				    		break;
				    	case "PTTM" :
				    		securityRecord.PTTM = rs.getString(2);
				    		break;
				    	case "PSI" :
				    		securityRecord.PSI = rs.getString(2);
				    		break;
				    	case "PPC" :
				    		securityRecord.PPC = rs.getString(2);
				    		break;
				    	case "PEPM" :
				    		securityRecord.PEPM = rs.getString(2);
				    		break;
				    	case "PREA" :
				    		securityRecord.PREA = rs.getString(2);
				    		break;
				    	case "NOTE" :
				    		securityRecord.NOTE = rs.getString(2);
				    		break;
				    	case "NOTM" :
				    		securityRecord.NOTM = rs.getString(2);
				    		break;
				    	case "FORM" :
				    		securityRecord.FORM = rs.getString(2);
				    		break;
				    	case "FRMM" :
				    		securityRecord.FRMM = rs.getString(2);
				    		break;
				    }
 			        }
 			        
 			        
 			        rs.close();
 			        stmt.close();
 			        
 			       log.info("....Task records added"); 
 			        
 			        json = new Gson().toJson(securityRecord);
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			        if (stmt != null) { stmt.close(); }
 			    }
 			    connectionHelper.closeConnection(conn);
 	    	}
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}
    
    @POST
 	@Path("/getNotifications")
    @Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String getNotifications(String masterString){
 		CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		ArrayList<NotificationRecord> notificationRecords = new ArrayList<NotificationRecord>();
 		
 		String json = "";
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = "";
 	   String userid = "";
 	   try
    	{	
 		   lib = jobject.get("datasource").toString();
 		   lib = lib.substring(1, lib.length()-1);
 		   userid = jobject.get("userid").toString();
 	    	userid = userid.substring(1, userid.length()-1);
    	}catch(Exception ex){
    		return json;
    	}
 		
 		if(lib.equals("undefined")){
 			return json;
 		}
 		
 	    try
 	    	{	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..calling SP: SPUNOTCR00");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPUNOTCR00(?)";
 			    
 			    try {
 			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

 			        stmt.setString(1, userid);
 			       
 			        stmt.execute();			        
 			        
 			        ResultSet rs = stmt.getResultSet();
 			        
// 			       SecurityRecord resultTaskRight = new SecurityRecord();	
 			        
 			        while (rs.next()) {
 			        	NotificationRecord notificationRecord = new NotificationRecord();
 			        	NotOptionRecord optionRecord = new NotOptionRecord();
 			        	notificationRecord.UserID = rs.getString(1).trim();
 			        	switch(rs.getString(2).trim()){
 			        		case "PNOTE2" :
 			        			notificationRecord.Type = rs.getString(2).trim();
 			        			if(rs.getString(3).trim().equals("Y")){
 			        				optionRecord.NotifyYN = true;
 			        			} else {
 			        				optionRecord.NotifyYN = false;
 			        			}
 			        			if(rs.getString(4).trim().equals("Y")){
 			        				optionRecord.InAppYN = true;
 			        			} else {
 			        				optionRecord.InAppYN = false;
 			        			}
 			        			if(rs.getString(5).trim().equals("Y")){
 			        				optionRecord.EmailYN = true;
 			        			} else {
 			        				optionRecord.EmailYN = false;
 			        			}
 			        			notificationRecord.optionRecord = optionRecord;
 			        			break;
 			        		case "PPMT" :
 			        			notificationRecord.Type = rs.getString(2).trim();
 			        			if(rs.getString(3).trim().equals("Y")){
 			        				optionRecord.NotifyYN = true;
 			        			} else {
 			        				optionRecord.NotifyYN = false;
 			        			}
 			        			if(rs.getString(4).trim().equals("Y")){
 			        				optionRecord.InAppYN = true;
 			        			} else {
 			        				optionRecord.InAppYN = false;
 			        			}
 			        			if(rs.getString(5).trim().equals("Y")){
 			        				optionRecord.EmailYN = true;
 			        			} else {
 			        				optionRecord.EmailYN = false;
 			        			}
 			        			notificationRecord.optionRecord = optionRecord;
 			        			break;
 			        		}
 			        	notificationRecords.add(notificationRecord);
 			        }
 			        
 			        
 			        rs.close();
 			        stmt.close();
 			        
 			       log.info("....Task records added"); 
 			        
 			        json = new Gson().toJson(notificationRecords);
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			        if (stmt != null) { stmt.close(); }
 			    }
 			    connectionHelper.closeConnection(conn);
 	    	}
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}
     
     @GET  
     @Path("/validateForm/{datasource}/{formid}/{formhash}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String validateForm(@PathParam("datasource") String lib, @PathParam("formid") int formid, 
    		 @PathParam("formhash") String formhash) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	PFRMMSTR PFRMMSTR = new PFRMMSTR();
    	PFRMPROP PFRMPROP = new PFRMPROP();
    	ArrayList<PFRMPROP> PFRMPROPs = new ArrayList<PFRMPROP>();
    	PFRMRESP PFRMRESP = new PFRMRESP();
    	ArrayList<PFRMRESP> PFRMRESPs = new ArrayList<PFRMRESP>();
    	PFRMRESM PFRMRESM = new PFRMRESM();
    	
    	String json = "";
    	
    	try{
//    		Encrypt decryptLibrary = new Encrypt();
//    		String lib = decryptLibrary.decryptLibrary(libEncrypted);
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn; 
  			
  			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SFRMMSCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMMSTR.FRID = rs.getInt(1);	
		        	PFRMMSTR.FRNAME = rs.getString(2).trim();
		        	PFRMMSTR.FREXTNL = rs.getString(3).trim();
		        	PFRMMSTR.FREXTHSH = rs.getString(4).trim();
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
		    if(PFRMMSTR.FRID == 0){
		    	return "{\"ERROR\": \"Form does not exist\"}";
		    }	   
		    
			log.info("..retrieved record. retrieving detail records");
							
			stmt = null;
		    query = "CALL SFRMPRCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMPROP = new PFRMPROP();
		        	PFRMPROP.FPID = rs.getInt(1);	
		        	PFRMPROP.FPSEQ = rs.getInt(2);
		        	PFRMPROP.FPNAME = rs.getString(3).trim();
		        	PFRMPROP.FPTYPE = rs.getString(4).trim();
		        	PFRMPROP.FPREQ = rs.getString(5).trim();
		        	PFRMPROPs.add(PFRMPROP);
		        }		        		        
		        
		        rs.close();
		        stmt.close();
		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
					
			log.info("..retrieved " + PFRMPROPs.size() + " detail records. Retreiving field options.");
									
			try {
				for(int i=0; i<PFRMPROPs.size(); i++){
					stmt = null;
				    query = "CALL SFRMPOCR00(?,?)";		
				    
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			        stmt.setInt(1,  PFRMPROPs.get(i).FPID);
			        stmt.setInt(2,  PFRMPROPs.get(i).FPSEQ);
			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	String reciever = rs.getString(1).trim();
			        	PFRMPROPs.get(i).FOVALUES.add(reciever);
			        }
			        
			        rs.close();
			        stmt.close();
			        
				}	        		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
				
			PFRMMSTR.PFRMPROP = PFRMPROPs;
			PFRMRESM.PFRMMSTR = PFRMMSTR;
			
			log.info("..retrieved field options.");
				 
			connectionHelper.closeConnection(conn);
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(PFRMRESM);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  	
    	return json;
      }
     
    @POST
  	@Path("/addNewUser")
  	@Consumes(MediaType.APPLICATION_JSON)
  	@Produces(MediaType.APPLICATION_JSON)
  	public String addNewUser(String masterString){
  	CONNECTOR CONNECTOR = new CONNECTOR(); 
  		Connection conn = null;
  		
  		
  		log.info("..mapping JSON string");
  		
  		JsonElement jelement = new JsonParser().parse(masterString);
  	    JsonObject  jobject = jelement.getAsJsonObject();
  	    String lib = jobject.get("lib").toString();
  	    lib = lib.substring(1, lib.length()-1);
  	    String fname = jobject.get("fname").toString();
  	    fname = fname.substring(1, fname.length()-1);
  	    String email = jobject.get("email").toString();
  	    email = email.substring(1, email.length()-1);
  	    String lname = jobject.get("lname").toString();
  	    lname = lname.substring(1, lname.length()-1);
  	    String security = jobject.get("security").toString();
  	    security = security.substring(1, security.length()-1);
  	    String group = jobject.get("group").toString();
  	    group = group.substring(1, group.length()-1);
  	    String pass = jobject.get("pass").toString();
  	    pass = pass.substring(1, pass.length()-1);
  	    
  	    Encrypt PasswordEncryption = new Encrypt();
  	    String encryptedPassword = PasswordEncryption.PasswordEncryption(pass);
  	    
  	    lib = PasswordEncryption.decryptLibrary(lib);
  	    security = PasswordEncryption.decryptLibrary(security);
  	    group = PasswordEncryption.decryptLibrary(group);
  		
  	    String json = "";
  	    
  	    String active = "Y";
  	    String libDecrypt = PasswordEncryption.decryptLibrary(lib);
  	    int blanks = 0;
  	    
  	    String securityDecode = "";
  	    switch(security){
  	  		case "0" :
  	  			securityDecode = "N";
  	  			break;
  	  		case"1" :
  	  			securityDecode = "M";
  	  			break;
  	  		case "2" :
  	  			securityDecode = "Y";
  	  			break;
  	    }
  	    
  	    try
  	    	{	    		    	
  		        CONNECTOR = connectionHelper.getConnection("PMCOMMON");
  	  			conn = CONNECTOR.conn;
  		        
  	  			log.info("..calling SP to create USER");
  		        
  				CallableStatement stmt = null;
  			    String query = "CALL CBUSER(?,?,?,?,?,?,?,?,?,?)";
  			   try {
  				  stmt = conn.prepareCall(query);
  				  
  				  stmt.registerOutParameter(10, Types.VARCHAR);
  			      stmt.setString(1, email);
  			      stmt.setString(2, fname);
  			      stmt.setString(3, lname);
  			      stmt.setString(4, encryptedPassword);
  			      stmt.setString(5, active);
  			      stmt.setString(6, libDecrypt);
  			      stmt.setString(7, securityDecode);
  			      stmt.setInt(8, blanks);
 			      stmt.setInt(9, blanks);

  			      stmt.executeUpdate();			        
  			      stmt.close();
  			        
  			        
  			    } catch (SQLException e ) {
  			    	log.warn(e);
  			    } finally {
  			        if (stmt != null) { stmt.close(); }
  			    }
  			    
  			 connectionHelper.closeConnection(conn);
  	    	}
  	    
  	    catch(Exception ex)
  	    {
  			json = "An error occured during the authenticate process.";
  			connectionHelper.closeConnection(conn);
  		    return(json);
  	    }
  	    return(json);
  	}
    
    @POST
 	@Path("/updateGroups")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String updateGroups(String masterString){
 	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		GroupRecord masterRecord = new GroupRecord();
// 		Encrypt encrypt = new Encrypt();
 		
 		log.info("..mapping JSON string");
// 		log.info("..." + masterString);
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
// 	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 		
     	ObjectMapper mapper = new ObjectMapper();
     	try {
     		masterRecord = mapper.readValue(jobject.get("group").toString(), new TypeReference<GroupRecord>(){});
 		} catch (JsonParseException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (JsonMappingException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (IOException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
 		
 		String json = "";
 		
 	    try
 	    	{	    		    	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..updating Group");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPRLGPRU01(?,?)";
 			    if(masterRecord.origName != ""){
 			    	try {
 			    		stmt = conn.prepareCall(query);
 				  
 			    		stmt.setString(1, masterRecord.GRPNAME);
 			    		stmt.setString(2, masterRecord.origName);

 			    		stmt.executeUpdate();			        	
 			    		stmt.close();
 			        
 			    	} catch (SQLException e ) {
 			    		log.warn(e);
 			    	} finally {
 			    		if (stmt != null) { stmt.close(); }
 			    	}
 			    }
 			    	
 			   stmt = null;
			   query = "CALL SPRLGPRU00(?,?,?,?,?,?)";
			   
			   try {
				  stmt = conn.prepareCall(query);
				  
				  for(String jobrole: masterRecord.GRPMEMBERs){
					  stmt.registerOutParameter(6, Types.VARCHAR);
					  stmt.setString(1, masterRecord.GRPNAME);
					  stmt.setString(2, jobrole);
					  stmt.setString(3, "I");
					  stmt.setInt(4, 0);
					  stmt.setInt(5, 0);

					  stmt.executeUpdate();			        	
				  }
				  stmt.close();
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
 			    
 			 connectionHelper.closeConnection(conn);
 	    	}
 	    
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}
    
    @POST
 	@Path("/updateUserProf")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String updateUserProf(String masterString){
 	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		
 		log.info("..mapping JSON string");
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 	    String fname = jobject.get("fname").toString();
 	    fname = fname.substring(1, fname.length()-1);
 	    String lname = jobject.get("lname").toString();
	    lname = lname.substring(1, lname.length()-1);
	    String email = jobject.get("email").toString();
	    email = email.substring(1, email.length()-1);
 		
 		String json = "";
 		
 	    try
 	    	{	    		    	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..updating User Information");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPUSERRU00(?,?,?,?)";
 			    try {
 			    	stmt = conn.prepareCall(query);
 				  
 			    	stmt.setString(1, userid);
 			    	stmt.setString(2, email);
 			    	stmt.setString(3, fname);
 			    	stmt.setString(4, lname);

 			    	stmt.executeUpdate();			        	
 			    	stmt.close();
 			        
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			    	if (stmt != null) { stmt.close(); }
 			    }
 			    
 			 connectionHelper.closeConnection(conn);
 	    	}
 	    
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}
    
    @POST
 	@Path("/updateNotifications")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String updateNotifications(String masterString){
 	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		ArrayList<NotificationRecord> masterRecords = new ArrayList<NotificationRecord>();
 		
 		log.info("..mapping JSON string");
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 	    
 	    String notif_tmp = jobject.get("notifications").toString();
		
    	ObjectMapper mapper = new ObjectMapper();
    	try {
    		 masterRecords = mapper.readValue(notif_tmp, new TypeReference<ArrayList<NotificationRecord>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 		
 		String json = "";
 		
 	    try
 	    	{	    		    	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..calling SP: SPUNOTRU00 to update Notifications");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPUNOTRU00(?,?,?,?,?)";

 			    try {
 			    	stmt = conn.prepareCall(query);
 			    	for(NotificationRecord masterRecord: masterRecords){
 			    		String notify = "";
 			    		String inapp = "";
 			    		String email = "";
 			    		
 			    		if(masterRecord.optionRecord.NotifyYN){
 			    			notify = "Y";
 			    		} else {
 			    			notify = "N";
 			    		}
 			    		if(masterRecord.optionRecord.InAppYN){
 			    			inapp = "Y";
 			    		} else {
 			    			inapp = "N";
 			    		}
 			    		if(masterRecord.optionRecord.EmailYN){
 			    			email = "Y";
 			    		} else {
 			    			email = "N";
 			    		}
 			    		
 			    		stmt.setString(1, userid);
 			    		stmt.setString(2, masterRecord.Type);
 			    		stmt.setString(3, notify);
 			    		stmt.setString(4, inapp);
 			    		stmt.setString(5, email);

 			    		stmt.executeUpdate();	
 			    	}
 			    	stmt.close();
 			        
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			    	if (stmt != null) { stmt.close(); }
 			    }
 			    
 			 connectionHelper.closeConnection(conn);
 	    	}
 	    
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    return(json);
 	}
    
    @POST
 	@Path("/getRequiredFields")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String getRequiredFields(String masterString){
 	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		ArrayList<DefinedFieldsRecord> defFields = new ArrayList<DefinedFieldsRecord>();
    	ArrayList<UDFRecord> userdefFields = new ArrayList<UDFRecord>();
 		
 		log.info("..mapping JSON string");
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 		
 		String json1 = "";
 		String json2 = "";
 		String jsonAll = "";
   	
 		try {	
 			    		
 			CONNECTOR = connectionHelper.getConnection(lib);
 			conn = CONNECTOR.conn;
 			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPDFFLCR00()";				
		    				    			    
		    try {
		    	stmt = conn.prepareCall(query);
 				stmt.execute();	            			        
       
 				ResultSet rs = stmt.getResultSet();
 				while (rs.next()) {
 					DefinedFieldsRecord DefFieldRec = new DefinedFieldsRecord();
	        	
 					DefFieldRec.DFSEQ = rs.getInt("DFSEQ");	
 					DefFieldRec.DFNAMID = rs.getString("DFNAMID").trim();
 					DefFieldRec.DFDISP = rs.getString("DFDISP");
 					DefFieldRec.DFREQ = rs.getString("DFREQ");
 					DefFieldRec.DFWIDTH = rs.getString("DFWIDTH");
 					switch(DefFieldRec.DFNAMID){
 			  	  		case "Status": case "Process Name": case "Task Name": case "End Date": case "Related To": case "Assigned" :
 			  	  			break;
 			  	  		default :
 			  	  			defFields.add(DefFieldRec);
 			  	  			break;
 					}
 				}
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
 			
 			log.info("..retrieved " + defFields.size() + " process defined records");
 			
 	  		json1 = new Gson().toJson(defFields);
 	  		
 	  		//TO DO: Get user defined ones
 	  		stmt = null;
			query = "CALL SPUDFCR00()";				
   				    			    
			try {
//s  				dispRecords.add(new UserDisplayRecord());
				stmt = conn.prepareCall(query);
				stmt.execute();	            			        
   
				ResultSet rs = stmt.getResultSet();
				while (rs.next()) {
					UDFRecord UDFieldRec = new UDFRecord();
       	
					UDFieldRec.USEQ = rs.getInt("USEQ");	
					UDFieldRec.UNAMID = rs.getString("UNAMID").trim();
					UDFieldRec.UTYPE = rs.getString("UTYPE").trim();
					UDFieldRec.UDISP = rs.getString("UDISP");
					UDFieldRec.UREQ = rs.getString("UREQ");
					UDFieldRec.UWIDTH = rs.getString("UWIDTH");
       	
					userdefFields.add(UDFieldRec);
				}
       
				rs.close();
				stmt.close();
	        
			} catch (SQLException e ) {
				log.warn(e);
				return "Error";
			} finally {
				if (stmt != null) { stmt.close(); }
			}
			
			log.info("..retrieved " + userdefFields.size() + " user defined records");
			
	  		json2 = new Gson().toJson(userdefFields);
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}
   	
   	connectionHelper.closeConnection(conn);	
 		
   	jsonAll = "["+json1+","+json2+"]";

 		return jsonAll;
    }
    
    @POST
 	@Path("/updateRequired")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String updateRequired(String masterString){
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
 		ArrayList<DefinedFieldsRecord> defFields = new ArrayList<DefinedFieldsRecord>();
    	ArrayList<UDFRecord> userdefFields = new ArrayList<UDFRecord>();
// 		Encrypt encrypt = new Encrypt();
 		
 		log.info("..mapping JSON string");
// 		log.info("..." + masterString);
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String userid = jobject.get("userid").toString();
 	    userid = userid.substring(1, userid.length()-1);
 	    String fields_tmp = jobject.get("fields").toString();
 	   String ufields_tmp = jobject.get("ufields").toString();
 		
     	ObjectMapper mapper = new ObjectMapper();
     	try {
     		defFields = mapper.readValue(fields_tmp, new TypeReference<ArrayList<DefinedFieldsRecord>>(){});
 		} catch (JsonParseException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (JsonMappingException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (IOException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
     	
     	try {
     		userdefFields = mapper.readValue(ufields_tmp, new TypeReference<ArrayList<UDFRecord>>(){});
 		} catch (JsonParseException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (JsonMappingException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		} catch (IOException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
 		
 		String json = "";
 		
 	    try
 	    	{	    		    	
 		        CONNECTOR = connectionHelper.getConnection(lib);
 	  			conn = CONNECTOR.conn;
 		        
 	  			log.info("..calling SP: SPDFFLRU00 to update Process Required");
 		        
 				CallableStatement stmt = null;
 			    String query = "CALL SPDFFLRU00(?,?)";
 			   try {
 				  stmt = conn.prepareCall(query);
 				  for(DefinedFieldsRecord masterRecord: defFields){
 			        stmt.setInt(1, masterRecord.DFSEQ);
 			        stmt.setString(2, masterRecord.DFREQ);

 			        stmt.executeUpdate();			        
 			    }	
 			        stmt.close();
 			        
 			        
 			    } catch (SQLException e ) {
 			    	log.warn(e);
 			    } finally {
 			        if (stmt != null) { stmt.close(); }
 			    }
 			   
 			   log.info("..calling SP: SPUDFRU00 to update Process Required");
		        
				stmt = null;
			    query = "CALL SPUDFRU00(?,?,?,?,?,?,?)";
			   try {
				  stmt = conn.prepareCall(query);
				  for(UDFRecord masterRecord: userdefFields){

			        stmt.setInt(1, masterRecord.USEQ);
			        stmt.setString(2, masterRecord.UNAMID);
			        stmt.setString(3, masterRecord.UTYPE);
			        stmt.setString(4, masterRecord.UDISP);
			        stmt.setString(5, masterRecord.UREQ);
			        stmt.setString(6, masterRecord.UWIDTH);
			        stmt.setString(7, "U");

			        stmt.executeUpdate();			        
			    }	
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
 			    
 	    	}
 	    
 	    catch(Exception ex)
 	    {
 	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
 	        authorizationRecord.returnErrorMessage = ex.getMessage();
 	    	
 			log.error(authorizationRecord.returnMessage);
 			log.error(ex.getMessage());			
 			
 			authorizationRecord.isError = true;
 			json = "An error occured during the authenticate process.";
 			connectionHelper.closeConnection(conn);
 		    return(json);
 	    }
 	    connectionHelper.closeConnection(conn);
 	    return(json);
 	}
     
     @GET  
     @Path("/encryptLib/{datasource}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String encryptLib(@PathParam("datasource") String lib) throws IOException {  
    	
    	 Encrypt encryptLibrary = new Encrypt();
    	 String returnString = encryptLibrary.encryptLibrary(lib);
    	
    	return returnString;
      }
      		
}  
