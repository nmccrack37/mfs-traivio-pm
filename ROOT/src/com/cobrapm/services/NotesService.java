package com.cobrapm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.ModuleTaskRecord;
import com.cobrapm.doa.PRELDESC;
import com.cobrapm.doa.UpdateTagsRecord;
import com.cobrapm.notes.DeleteNoteRecord;
import com.cobrapm.notes.InsertNoteRecord;
import com.cobrapm.notes.PNOTE2ATTRec;
import com.cobrapm.notes.PNOTE2REL;
import com.cobrapm.notes.PNOTE2Rec;
import com.cobrapm.notes.UpdateNoteRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileInputStream;
import com.ibm.as400.access.IFSFileOutputStream;

@Path("NotesService")
public class NotesService {
	
	ModuleTaskRecord CURRENT_APPLICATION = new ModuleTaskRecord();

	static Properties props;
	static Properties resourceBundle;
	static Connection conn = null;
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(NotesService.class.getName());
	
//	@GET
//	@Path("/getAllTags/{lib}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String getAllTags(@PathParam("lib")String lib){
//		CONNECTOR CONNECTOR = new CONNECTOR(); 
//		Connection conn = null;
//		
//		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
//		PRELDESC PRELDESC = new PRELDESC();
//		ArrayList<PRELDESC> PRELDESCS = new ArrayList<PRELDESC>();
 //   	
	@POST
	@Path("/getAllTags")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllTags(String masterString){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		PRELDESC PRELDESC = new PRELDESC();
		ArrayList<PRELDESC> PRELDESCS = new ArrayList<PRELDESC>();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);

		String json = "";
		
	    try
	    	{	
//	    		log.info("...datasource: " + lib);
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SPRINFCR01");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPRINFCR01()";
			    
			    try {
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	PRELDESC = new PRELDESC();
			        	PRELDESC.RECID = rs.getInt(1);
			        	PRELDESC.PRDID = rs.getInt(2);
			        	PRELDESC.PRDESC = rs.getString(3).trim();
			        	PRELDESCS.add(PRELDESC);
			        }
			        
			        rs.close();
			        stmt.close();
			        
			        log.info("..Retrieved " + PRELDESCS.size() + " Tags Records");
			        
			        json = new Gson().toJson(PRELDESCS);
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    connectionHelper.closeConnection(conn);
	    	}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/getNotesByProcess")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getNotesByProcess(String masterString){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		PNOTE2REL PNOTE2Rel = new PNOTE2REL();
		ArrayList<PNOTE2REL> PNOTE2Rels = new ArrayList<PNOTE2REL>();
		PNOTE2Rec PNOTE2Rec = new PNOTE2Rec();
		ArrayList<PNOTE2Rec> PNOTE2Recs = new ArrayList<PNOTE2Rec>();
		PNOTE2ATTRec PNOTE2ATTRec = new PNOTE2ATTRec();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    int keyn1 = jobject.get("KEYN1").getAsInt();
	    int keyn2 = jobject.get("KEYN2").getAsInt();
	    int keyn3 = jobject.get("KEYN3").getAsInt();
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
	  			
	  			log.info("..calling SP: SPNRELCR00 to retrieve all Note ID's and Descriptions");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPNRELCR00()";
			    
			    try {
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	PNOTE2Rel = new PNOTE2REL();
			        	PNOTE2Rel.N5ID = rs.getInt(1);
			        	PNOTE2Rel.RECID = rs.getInt(2);
			        	PNOTE2Rel.PRDID = rs.getInt(3);
			        	PNOTE2Rel.DESC = rs.getString(4);
			        	PNOTE2Rels.add(PNOTE2Rel);
			        }
			        
			        rs.close();
			        stmt.close();
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
		        
	  			log.info("..calling SP: SNOTE2CR02");
		        
				stmt = null;
			    query = "CALL SNOTE2CR02(?,?,?)";
			    
			    try {
			    	 stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			    	 stmt.setInt(1, keyn1);
			    	 stmt.setInt(2, keyn2);
			    	 stmt.setInt(3, keyn3);
			    	 stmt.execute();			        
			    	 int i = 1;
			    	 ResultSet rs = stmt.getResultSet();
			    	 while (rs.next()) {
			    		 PNOTE2Rec = new PNOTE2Rec();
			    		 PNOTE2Rec.N5ID = rs.getInt(1);
			    		 PNOTE2Rec.N5IDCMTSEQ = rs.getInt(2);
			    		 PNOTE2Rec.N5TEXT = rs.getString(3);
			    		 PNOTE2Rec.N5PINNED = rs.getString(4);
			    		 PNOTE2Rec.SYSCRT = rs.getTimestamp(5);
			    		 PNOTE2Rec.SYSCRTBY = rs.getString(6);
			    		 PNOTE2Rec.ProcessDesc = rs.getString(7);
			    		 PNOTE2Rec.SYSUPD = rs.getTimestamp(8);
			    		 PNOTE2Rec.SYSUPDBY = rs.getString(9);
			    		 PNOTE2Rec.count = i;
			    		 for(PNOTE2REL prelRec: PNOTE2Rels){
		    				 if(prelRec.N5ID == PNOTE2Rec.N5ID){
		    					 PNOTE2Rec.PNOTE2RELs.add(prelRec);
		    				 }
		    			 }
			    		 PNOTE2Recs.add(PNOTE2Rec);
			    		 i++;
			    	 }
			    	rs.close();
			        stmt.close();
			        log.info("..Retrieved " + PNOTE2Recs.size() + " Note Records");

			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
	    
			    log.info("..Checking for attachments...");
			    
			    stmt = null;
			    query = "CALL SNOTE2CR01(?)";
			    
			    // Create an AS400 object.  This new directory will 
			    // be created on this system. 		
				Context initialContext;
				String dbhost = "";  					  
				String userid = "";  
				String password = "";  
				try {
					initialContext = (Context)new InitialContext().lookup("java:comp/env");		
				    dbhost = (String)initialContext.lookup("dbhost");  					  
					userid = (String)initialContext.lookup("dbuser");  
					password = (String)initialContext.lookup("dbpassword"); 
				} catch (NamingException e) {
					System.out.println(e.getMessage());
				}
				  
			    AS400 sys = new AS400(dbhost, userid, password);
			    
			    try{
			    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			    	for(PNOTE2Rec note2rec: PNOTE2Recs){
			    		stmt.setInt(1, note2rec.N5ID);
			    		stmt.execute();			        
	        
			    		ResultSet rs = stmt.getResultSet();
			    		while (rs.next()) {
			    			PNOTE2ATTRec = new PNOTE2ATTRec();
			    			PNOTE2ATTRec.NANOTEID = rs.getInt(1);
			    			PNOTE2ATTRec.NAID = rs.getInt(2);
			    			PNOTE2ATTRec.NAPATH = rs.getString(3);
			    			PNOTE2ATTRec.NAFILENAME = rs.getString(4);
			    			PNOTE2ATTRec.NAFILENAUN = rs.getString(5);
			    			note2rec.PNOTE2ATTRecs.add(PNOTE2ATTRec);
			    		}
	    		
			    		rs.close();
			    		
			    		for(PNOTE2ATTRec note2attrec: note2rec.PNOTE2ATTRecs){
			    			System.out.println("getting files from IFS");
			    			
			    			String directorypath = "COBRAIFS" + IFSFile.separator + lib + IFSFile.separator + "Notes_Attachments";
			    			
			    			log.info("...File Path: " + directorypath);
			    		   
			    			IFSFile dir = new IFSFile(sys, directorypath);
			    			IFSFile[] directoryListing = null;
			    			try {
			    				directoryListing = dir.listFiles();
			    			} catch (IOException e) {
			    				System.out.println(e.getMessage());
			    			}
		    
			    			System.out.println(directoryListing.length);
		    
			    			String temppath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			    			temppath = temppath.substring(0, temppath.indexOf("WEB-INF"));		        		
			    			temppath = temppath + "temp" ;
			    			
//			    			String temppath = "C:\\Workspaces\\Cobra\\Portal (Java)\\NOTES\\WebContent\\temp";

			    			for (IFSFile child : directoryListing) {
			    				// Create a file object that represents the directory.
			    				String filename = child.getName(); 
			    				if(filename.equals(note2attrec.NAFILENAUN)){
			    					System.out.println(filename);
				    				temppath = temppath + File.separator + filename;
				    				try {	  
				    					IFSFileInputStream source = new IFSFileInputStream(child);
				    					byte[] buffer = new byte[(int) child.length()];	
				    					source.read(buffer);
				    					FileOutputStream fos = new FileOutputStream(temppath);
				    					fos.write(buffer);
				    					source.close();
				    					fos.close();
				    					log.info("...file found");
				    					note2attrec.NAPATH = "./temp/" + filename;
				    				} catch (AS400SecurityException | IOException e) {
				    					System.out.println("error message " + e.getMessage());
				    				}
				    				String ext = FilenameUtils.getExtension(filename);
				    				switch(ext){
				    					case "xls": case "xlsx":  case "xlsm":
				    						note2attrec.NAIMAGEPATH = "./images/stored/spreadsheet.png";
				    						break;
				    					case "doc": case "docx":
				    						note2attrec.NAIMAGEPATH = "./images/stored/doc.png";
				    						break;
				    					case "pdf":
				    						note2attrec.NAIMAGEPATH = "./images/stored/pdf.png";
				    						break;
				    					case "msg":
				    						note2attrec.NAIMAGEPATH = "./images/stored/email.png";
				    						break;
				    					default:
				    						note2attrec.NAIMAGEPATH = note2attrec.NAPATH;
				    						break;
				    				}
				    				log.info("...imagepath: " + note2attrec.NAIMAGEPATH);
			    				}
			    			}
			    		}
	          }
			    stmt.close();
			  json = new Gson().toJson(PNOTE2Recs);
			  connectionHelper.closeConnection(conn);
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
		}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			connectionHelper.closeConnection(conn);
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/getNotes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getNotes(String masterString){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<PRELDESC> PRELDESCs = new ArrayList<PRELDESC>();
		PNOTE2Rec PNOTE2Rec = new PNOTE2Rec();
		ArrayList<PNOTE2Rec> PNOTE2Recs = new ArrayList<PNOTE2Rec>();
		PNOTE2REL PNOTE2Rel = new PNOTE2REL();
		ArrayList<PNOTE2REL> PNOTE2Rels_tmp = new ArrayList<PNOTE2REL>();
		PNOTE2ATTRec PNOTE2ATTRec = new PNOTE2ATTRec();
		List<Integer> NoteIDs = new ArrayList<Integer>();
		
		log.info("..mapping JSON string");
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    String descsOut_tmp = jobject.get("DescsOut").toString();

	    ObjectMapper mapper = new ObjectMapper();
    	try {
			PRELDESCs = mapper.readValue(descsOut_tmp, new TypeReference<ArrayList<PRELDESC>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
    	log.info("..." + PRELDESCs);
		String json = "";

	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SPNRELCR00 to retrieve all Note ID's and Descriptions");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPNRELCR00()";
			    if(PRELDESCs.size() > 0){
			    	try {
			    		stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			    		stmt.execute();			        
			        
			    		ResultSet rs = stmt.getResultSet();
			    		while (rs.next()) {
			    			PNOTE2Rel = new PNOTE2REL();
			    			PNOTE2Rel.N5ID = rs.getInt(1);
			    			PNOTE2Rel.RECID = rs.getInt(2);
			    			PNOTE2Rel.PRDID = rs.getInt(3);
			    			PNOTE2Rel.DESC = rs.getString(4);
			    			PNOTE2Rels_tmp.add(PNOTE2Rel);
			    		}
			    		
			    		List<String> checkRel = new ArrayList<String>();
			    		for(PRELDESC prelRec: PRELDESCs){
			    			for(PNOTE2REL testRel : PNOTE2Rels_tmp){
			    				if(prelRec.RECID == testRel.RECID
			    						&& prelRec.PRDID == testRel.PRDID){
			    					checkRel.add(Integer.toString(testRel.N5ID));
			    				}
			    			}
			    		}
			    		Iterator<String> i = checkRel.iterator();
			    		while(i.hasNext()){
			    			String tmpString = i.next();
			    			if(Collections.frequency(checkRel,tmpString) == PRELDESCs.size()){
			    				NoteIDs.add(Integer.parseInt(tmpString));
			    			}
			    			i.remove();
			    		}
			        
			    		rs.close();
			    		stmt.close();
			        	
			    	} catch (SQLException e ) {
			    		log.warn(e);
			    	} finally {
			    		if (stmt != null) { stmt.close(); }
			    	}
			    }
			    
			    log.info("...retrieved " + NoteIDs.size() + " PNOTE2REL records, now building Note array using SNOTE2CR00");
			    
			    stmt = null;
			    query = "CALL SNOTE2CR00(?)";
			    
			    try {
			    	 stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			    	 for(int i: NoteIDs){
			    		 stmt.setInt(1, i);
			    		 stmt.execute();			        
			    		 
			    		 int k = 1;
			    		 ResultSet rs = stmt.getResultSet();
			    		 while (rs.next()) {
			    			 PNOTE2Rec = new PNOTE2Rec();
			    			 PNOTE2Rec.N5ID = rs.getInt(1);
			    			 PNOTE2Rec.N5IDCMTSEQ = rs.getInt(2);
			    			 PNOTE2Rec.N5KEYN1 = rs.getInt(3);
			    			 PNOTE2Rec.N5KEYN2 = rs.getInt(4);
			    			 PNOTE2Rec.N5KEYN3 = rs.getInt(5);
			    			 PNOTE2Rec.N5TEXT = rs.getString(6);
			    			 PNOTE2Rec.N5PINNED = rs.getString(7);
			    			 PNOTE2Rec.SYSCRT = rs.getTimestamp(8);
			    			 PNOTE2Rec.SYSCRTBY = rs.getString(9);
			    			 PNOTE2Rec.ProcessDesc = rs.getString(10);
			    			 PNOTE2Rec.SYSUPD = rs.getTimestamp(11);
			    			 PNOTE2Rec.SYSUPDBY = rs.getString(12);
			    			 PNOTE2Rec.count = k;
			    			 PNOTE2Rec.editForm = "editForm"+k;
			    			 for(PNOTE2REL prelRec: PNOTE2Rels_tmp){
			    				 if(prelRec.N5ID == PNOTE2Rec.N5ID){
			    					 PNOTE2Rec.PNOTE2RELs.add(prelRec);
			    				 }
			    			 }
			    			 PNOTE2Recs.add(PNOTE2Rec);
			    			 k++;
			    		 }
			    		
			    		 rs.close();
			    	 }
			        stmt.close();
			        log.info("..Retrieved " + PNOTE2Recs.size() + " Note Records");

			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
	    
			    log.info("..Checking for attachments...");
			    
			    stmt = null;
			    query = "CALL SNOTE2CR01(?)";
			    
			    // Create an AS400 object.  This new directory will 
			    // be created on this system. 		
				Context initialContext;
				String dbhost = "";  					  
				String userid = "";  
				String password = "";  
				try {
					initialContext = (Context)new InitialContext().lookup("java:comp/env");		
				    dbhost = (String)initialContext.lookup("dbhost");  					  
					userid = (String)initialContext.lookup("dbuser");  
					password = (String)initialContext.lookup("dbpassword"); 
				} catch (NamingException e) {
					System.out.println(e.getMessage());
				}
				  
			    AS400 sys = new AS400(dbhost, userid, password);
			    
			    try{
			    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			    	for(PNOTE2Rec note2rec: PNOTE2Recs){
			    		stmt.setInt(1, note2rec.N5ID);
			    		stmt.execute();			        
	        
			    		ResultSet rs = stmt.getResultSet();
			    		while (rs.next()) {
			    			PNOTE2ATTRec = new PNOTE2ATTRec();
			    			PNOTE2ATTRec.NANOTEID = rs.getInt(1);
			    			PNOTE2ATTRec.NAID = rs.getInt(2);
			    			PNOTE2ATTRec.NAPATH = rs.getString(3);
			    			PNOTE2ATTRec.NAFILENAME = rs.getString(4);
			    			PNOTE2ATTRec.NAFILENAUN = rs.getString(5);
			    			note2rec.PNOTE2ATTRecs.add(PNOTE2ATTRec);
			    		}
	    		
			    		rs.close();
			    		
			    		for(PNOTE2ATTRec note2attrec: note2rec.PNOTE2ATTRecs){
			    			System.out.println("getting files from IFS");
			    			
			    			String directorypath = "COBRAIFS" + IFSFile.separator + lib + IFSFile.separator + "Notes_Attachments";
			    			
			    			log.info("...File Path: " + directorypath);
			    		   
			    			IFSFile dir = new IFSFile(sys, directorypath);
			    			IFSFile[] directoryListing = null;
			    			try {
			    				directoryListing = dir.listFiles();
			    			} catch (IOException e) {
			    				System.out.println(e.getMessage());
			    			}
		    
			    			System.out.println(directoryListing.length);
		    
			    			String temppath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
			    			temppath = temppath.substring(0, temppath.indexOf("WEB-INF"));		        		
			    			temppath = temppath + "temp" ;
			    			
//			    			String temppath = "C:\\Workspaces\\Cobra\\Portal (Java)\\NOTES\\WebContent\\temp";

			    			for (IFSFile child : directoryListing) {
			    				// Create a file object that represents the directory.
			    				String filename = child.getName(); 
			    				if(filename.equals(note2attrec.NAFILENAUN)){
			    					System.out.println(filename);
				    				temppath = temppath + File.separator + filename;
				    				try {	  
				    					IFSFileInputStream source = new IFSFileInputStream(child);
				    					byte[] buffer = new byte[(int) child.length()];	
				    					source.read(buffer);
				    					FileOutputStream fos = new FileOutputStream(temppath);
				    					fos.write(buffer);
				    					source.close();
				    					fos.close();
				    					log.info("...file found");
				    					note2attrec.NAPATH = "./temp/" + filename;
				    				} catch (AS400SecurityException | IOException e) {
				    					System.out.println("error message " + e.getMessage());
				    				}
				    				String ext = FilenameUtils.getExtension(filename);
				    				switch(ext){
				    					case "xls": case "xlsx":  case "xlsm":
				    						note2attrec.NAIMAGEPATH = "./images/stored/spreadsheet.png";
				    						break;
				    					case "doc": case "docx":
				    						note2attrec.NAIMAGEPATH = "./images/stored/doc.png";
				    						break;
				    					case "pdf":
				    						note2attrec.NAIMAGEPATH = "./images/stored/pdf.png";
				    						break;
				    					case "msg":
				    						note2attrec.NAIMAGEPATH = "./images/stored/email.png";
				    						break;
				    					default:
				    						note2attrec.NAIMAGEPATH = note2attrec.NAPATH;
				    						break;
				    				}
				    				log.info("...imagepath: " + note2attrec.NAIMAGEPATH);
			    				}
			    			}
			    		}
	          }
			    stmt.close();
			  json = new Gson().toJson(PNOTE2Recs);
			  connectionHelper.closeConnection(conn);
	    } catch (SQLException e ) {
	    	log.warn(e);
	    } finally {
	        if (stmt != null) { stmt.close(); }
	    }
        }
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			connectionHelper.closeConnection(conn);
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/insertNotes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String insertNotes(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		InsertNoteRecord masterRecord = new InsertNoteRecord();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    masterRecord.lib = jobject.get("lib").toString();
	    masterRecord.lib = masterRecord.lib.substring(1, masterRecord.lib.length()-1);
//	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
	    masterRecord.userid = jobject.get("userid").toString();
	    masterRecord.userid = masterRecord.userid.substring(1, masterRecord.userid.length()-1);
	    masterRecord.isPinned = jobject.get("isPinned").toString();
	    masterRecord.isPinned = masterRecord.isPinned.substring(1, masterRecord.isPinned.length()-1);
	    masterRecord.noteText = jobject.get("noteText").toString();
	    String descsOut_tmp = jobject.get("descsOut").toString();
	    masterRecord.KEYN1 = jobject.get("KEYN1").getAsInt();
	    masterRecord.KEYN2 = jobject.get("KEYN2").getAsInt();
	    masterRecord.KEYN3 = jobject.get("KEYN3").getAsInt();
	    masterRecord.documentFolder = jobject.get("documentFolder").toString();
	    masterRecord.documentFolder = masterRecord.documentFolder.substring(1, masterRecord.documentFolder.length()-1);
		
		try {
			masterRecord.noteText = URLDecoder.decode(masterRecord.noteText, "UTF-8");
			masterRecord.noteText = masterRecord.noteText.substring(1, masterRecord.noteText.length()-1);
			if(masterRecord.noteText.startsWith("<p>")){
				masterRecord.noteText = masterRecord.noteText.substring(3, masterRecord.noteText.length()-4);
			}
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//    	log.info(noteText);
    	ObjectMapper mapper = new ObjectMapper();
    	try {
    		 masterRecord.descsOut = mapper.readValue(descsOut_tmp, new TypeReference<ArrayList<PRELDESC>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(masterRecord.lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SNOTE2RU06 to insert Notes");
		        
	  			int N5ID = 0;
	  			String filenameunq = "";
				CallableStatement stmt = null;
			    String query = "CALL SNOTE2RU06(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			    
			    Date currentDate = new Date();
			    Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			    log.info(currentTimestamp);
			    try {
			        stmt = conn.prepareCall(query);
			        
			        String blanks = "";
			        String samenote = "N";
			        String program = "SNOTE2RU06";
			        
			        stmt.registerOutParameter(16, Types.INTEGER);
			        stmt.registerOutParameter(17, Types.INTEGER);
			        stmt.registerOutParameter(18, Types.CHAR);
			        stmt.setInt(1, masterRecord.KEYN1);
			        stmt.setInt(2, masterRecord.KEYN2);
			        stmt.setInt(3, masterRecord.KEYN3);
			        stmt.setString(4, masterRecord.noteText);
			        stmt.setString(5, masterRecord.isPinned);
			        stmt.setString(6, blanks);
			        stmt.setString(7, blanks);
			        stmt.setString(8, blanks);
			        stmt.setString(9, samenote);
			        stmt.setTimestamp(10, currentTimestamp);
			        stmt.setTimestamp(11, currentTimestamp);
			        stmt.setString(12,masterRecord.userid);
			        stmt.setString(13,masterRecord.userid);
			        stmt.setString(14,program);
			        stmt.setString(15,program);
			        stmt.setInt(16,0);
			        stmt.setInt(17,0);

			        stmt.executeUpdate();			        
			        
			        N5ID = stmt.getInt(16);
			        log.info("...Note ID: " + N5ID);
			        
			        String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
					fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));
					fullpath = fullpath + "uploads/" + masterRecord.documentFolder;	
	
//			        String fullpath = "C:\\Workspaces\\Cobra\\Portal (Java)\\NOTES\\WebContent\\WEB-INF\\uploads";
			        log.info("...Full Path: " + fullpath);
					File dir = new File(fullpath);
					File[] directoryListing = dir.listFiles();
					if (directoryListing != null) {	
						for (File child : directoryListing) {
							String filename = child.getName();
							char findStr = '.';
							int lastIndex = 0;

							while(lastIndex != -1) {
								for (int i = filename.length()-1; i >= 0; i--){
								    char c = filename.charAt(i);        
								    //Process char
								    if(c == findStr){
								    	String ext = filename.substring(i,filename.length());
								    	String start = filename.substring(0,i);
								    	Date newDate = new Date();
								    	String ts = String.valueOf(newDate.getTime());
								    	filenameunq = start+ts+ext;
								    	log.info("..." + filenameunq);
								    	lastIndex = -1;
								    	break;
								    }
								}
							}
							
							String path = "/COBRAIFS/" + masterRecord.lib + "/Note_Attachments/";
							
					        samenote = "Y";
					        
					        stmt.registerOutParameter(16, Types.INTEGER);
					        stmt.registerOutParameter(17, Types.INTEGER);
					        stmt.registerOutParameter(18, Types.CHAR);
					        stmt.setInt(1, masterRecord.KEYN1);
					        stmt.setInt(2, masterRecord.KEYN2);
					        stmt.setInt(3, masterRecord.KEYN3);
					        stmt.setString(4, blanks);
					        stmt.setString(5, masterRecord.isPinned);
					        stmt.setString(6, path);
					        stmt.setString(7, filename);
					        stmt.setString(8, filenameunq);
					        stmt.setString(9, samenote);
					        stmt.setTimestamp(10, currentTimestamp);
					        stmt.setTimestamp(11, currentTimestamp);
					        stmt.setString(12,masterRecord.userid);
					        stmt.setString(13,masterRecord.userid);
					        stmt.setString(14,program);
					        stmt.setString(15,program);
					        stmt.setInt(16,N5ID);
					        stmt.setInt(17,0);

					        stmt.executeUpdate();
						}
					}
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    log.info("..calling SP: SPNRELRU00 to insert Note Tags");
		        
				stmt = null;
			    query = "CALL SPNRELRU00(?,?,?,?)";

			    try {
			        stmt = conn.prepareCall(query);
			        
			        for(PRELDESC prelRec: masterRecord.descsOut){
			        	stmt.registerOutParameter(4, Types.CHAR);
			        	stmt.setInt(1, N5ID);
			        	stmt.setInt(2, prelRec.RECID);
			        	stmt.setInt(3, prelRec.PRDID);

			        	stmt.executeUpdate();
			        }
			        
			        stmt.close();
			        
			        authorizationRecord.isError = false;
			        json = new Gson().toJson(authorizationRecord);			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    log.info("..Added children. Uploading documents");
			    
				String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
				fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
				fullpath = fullpath + "uploads/" + masterRecord.documentFolder;	
			    
//			    String fullpath = "C:\\Workspaces\\Cobra\\Portal (Java)\\NOTES\\WebContent\\WEB-INF\\uploads";
												
				File dir = new File(fullpath);
				File[] directoryListing = dir.listFiles();
				  if (directoryListing != null) {					  					
					  
					// Create an AS400 object.  This new directory will 
			        // be created on this system.
					Context initialContext = (Context)new InitialContext().lookup("java:comp/env");
				    String dbhost = (String)initialContext.lookup("dbhost");  					  
					String dbuserid = (String)initialContext.lookup("dbuser");  
					String password = (String)initialContext.lookup("dbpassword");  
					  
			        AS400 sys = new AS400(dbhost, dbuserid, password);
			        
			        // /$Library
			        String directorypath = "COBRAIFS"+IFSFile.separator+ masterRecord.lib;  
			        createDirectory(sys, directorypath);
			        
			        // /$Library/Forms
			        directorypath = "COBRAIFS"+IFSFile.separator+ masterRecord.lib + IFSFile.separator + "Notes_Attachments";   
			        createDirectory(sys, directorypath);
			        
				    for (File child : directoryListing) {
				        // Create a file object that represents the directory.
				    	String filename = child.getName(); 
						FileInputStream sourceStream = new FileInputStream(fullpath + IFSFile.separator + filename);
				        IFSFileOutputStream destination = new IFSFileOutputStream(sys,  directorypath + IFSFile.separator + filenameunq);
				        byte[] buffer = new byte[(int)child.length()];
				        sourceStream.read(buffer);
				        destination.write(buffer);
				   	 	destination.close(); 
				   	 	sourceStream.close();
				    }

				    FileUtils.deleteDirectory(dir);				    
				    sys.disconnectAllServices();
				  }
				  connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/updateNotes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateNotes(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		UpdateNoteRecord masterRecord = new UpdateNoteRecord();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    masterRecord.lib = jobject.get("lib").toString();
	    masterRecord.lib = masterRecord.lib.substring(1, masterRecord.lib.length()-1);
//	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
	    masterRecord.userid = jobject.get("userid").toString();
	    masterRecord.userid = masterRecord.userid.substring(1, masterRecord.userid.length()-1);
	    masterRecord.noteText = jobject.get("noteText").toString();
	    masterRecord.noteID = jobject.get("noteID").getAsInt();
	    masterRecord.mode = jobject.get("mode").toString();
	    masterRecord.mode = masterRecord.mode.substring(1, masterRecord.mode.length()-1);
		
		try {
			masterRecord.noteText = URLDecoder.decode(masterRecord.noteText, "UTF-8");
			masterRecord.noteText = masterRecord.noteText.substring(1, masterRecord.noteText.length()-1);
			if(masterRecord.noteText.startsWith("<p>")){
				masterRecord.noteText = masterRecord.noteText.substring(3, masterRecord.noteText.length()-4);
			}
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(masterRecord.lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SNOTE2RU07 to Update/Delete Notes");
		        
	  			int N5ID = 0;
	  			String filenameunq = "";
				CallableStatement stmt = null;
			    String query = "CALL SNOTE2RU07(?,?,?,?,?,?)";
			    
			    Date currentDate = new Date();
			    Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			    try {
			        stmt = conn.prepareCall(query);
			        
			        String blanks = "";
			        String samenote = "N";
			        String program = "SNOTE2RU07";

			        stmt.setInt(1, masterRecord.noteID);
			        stmt.setString(2, masterRecord.noteText);
			        stmt.setString(3, masterRecord.mode);
			        stmt.setTimestamp(4, currentTimestamp);
			        stmt.setString(5,masterRecord.userid);
			        stmt.setString(6,program);

			        stmt.executeUpdate();			        
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
				 
			    connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/deleteNotes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteNotes(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		DeleteNoteRecord masterRecord = new DeleteNoteRecord();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    masterRecord.lib = jobject.get("lib").toString();
	    masterRecord.lib = masterRecord.lib.substring(1, masterRecord.lib.length()-1);
//	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
	    masterRecord.userid = jobject.get("userid").toString();
	    masterRecord.userid = masterRecord.userid.substring(1, masterRecord.userid.length()-1);
	    masterRecord.noteID = jobject.get("noteID").getAsInt();
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(masterRecord.lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SNOTE2RU07 to Update/Delete Notes");
		        
	  			int N5ID = 0;
	  			String filenameunq = "";
				CallableStatement stmt = null;
			    String query = "CALL SNOTE2RU07(?,?,?,?,?,?)";
			    
			    Date currentDate = new Date();
			    Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			    try {
			        stmt = conn.prepareCall(query);
			        
			        String blanks = "";
			        String samenote = "N";
			        String program = "SNOTE2RU07";
			        String mode = "D";

			        stmt.setInt(1, masterRecord.noteID);
			        stmt.setString(2, blanks);
			        stmt.setString(3, mode);
			        stmt.setTimestamp(4, currentTimestamp);
			        stmt.setString(5,masterRecord.userid);
			        stmt.setString(6,program);

			        stmt.executeUpdate();			        
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
				 
			    connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/updatesTagsForNote")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updatesTagsForNote(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<String> newStrings = new ArrayList<String>();
		UpdateTagsRecord masterRecord = new UpdateTagsRecord();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		String currentTags = "";
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    masterRecord.lib = jobject.get("lib").toString();
	    masterRecord.lib = masterRecord.lib.substring(1, masterRecord.lib.length()-1);
//	    masterRecord.lib = encrypt.decryptLibrary(masterRecord.lib);
	    masterRecord.userid = jobject.get("userid").toString();
	    masterRecord.userid = masterRecord.userid.substring(1, masterRecord.userid.length()-1);
	    masterRecord.noteID = jobject.get("noteID").getAsInt();
	    currentTags = jobject.get("tags").toString();
		
		log.info("..mapping JSON string");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	log.info("...Tags" + currentTags);
    	try {
    		masterRecord.tags = mapper.readValue(currentTags, new TypeReference<ArrayList<PRELDESC>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(masterRecord.lib);
	  			conn = CONNECTOR.conn;
	  			
	  			log.info("..calling SP: SPNRELRD00");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPNRELRD00(?,?)";

			    try {
			        stmt = conn.prepareCall(query);

			        stmt.registerOutParameter(2, Types.CHAR);
			        stmt.setInt(1, masterRecord.noteID);

			        stmt.executeUpdate();
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    try {
			    
			        log.info("..calling SP: SPNRELRU00 to insert Note Tags");
		        
			        stmt = null;
			        query = "CALL SPNRELRU00(?,?,?,?)";

			        stmt = conn.prepareCall(query);
			        
			        for(PRELDESC prelRec: masterRecord.tags){
			        	stmt.registerOutParameter(4, Types.CHAR);
			        	stmt.setInt(1, masterRecord.noteID);
			        	stmt.setInt(2, prelRec.RECID);
			        	stmt.setInt(3, prelRec.PRDID);

			        	stmt.executeUpdate();
			        }
			        
			        stmt.close();
			        
			        authorizationRecord.isError = false;
			        json = new Gson().toJson(authorizationRecord);			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    log.info("..Added children");
			    connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@GET
	@Path("/updateTags/{lib}/{currentTags}")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateTags(@PathParam("lib")String lib, @PathParam("currentTags")String currentTagsIn){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		PRELDESC PRELDESC = new PRELDESC();
		ArrayList<PRELDESC> PRELDESCS = new ArrayList<PRELDESC>();
		ArrayList<String> newStrings = new ArrayList<String>();
		
		log.info("..mapping JSON string");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	log.info("...Tags" + currentTagsIn);
    	try {
			newStrings = mapper.readValue(currentTagsIn, new TypeReference<ArrayList<String>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		log.info("..." + newStrings);
		String json = "";
		
//		Encrypt encrypt = new Encrypt();
//		lib = encrypt.decryptLibrary(lib);
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SPRINFRU01 to insert Tags");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPRINFRU01(?,?,?,?,?)";
			    
			    int RecID = 1;
			    int PrdID = 0;
			    String Mode = "I";
			    
			    for(String stringInsert:newStrings){
			    	log.info("...insert string: " + stringInsert);
			    	try {
			    		stmt = conn.prepareCall(query);
			        
			    		stmt.registerOutParameter(5, Types.CHAR);
			    		stmt.setInt(1, RecID);
			    		stmt.setInt(2, PrdID);
			    		stmt.setString(3, stringInsert);
			    		stmt.setString(4, Mode);

			    		stmt.executeUpdate();			        
					
			    		stmt.close();
			        
			        
			    	} catch (SQLException e ) {
			    		log.warn(e);
			    	} finally {
			    		if (stmt != null) { stmt.close(); }
			    	}
			    }
			    
			    authorizationRecord.isError = false;	
	    	}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    
	    try
    	{	
    		log.info("...datasource: " + lib);
	        CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
	        
  			log.info("..calling SP: SPRINFCR01");
	        
			CallableStatement stmt = null;
		    String query = "CALL SPRINFCR01()";
		    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PRELDESC = new PRELDESC();
		        	PRELDESC.RECID = rs.getInt(1);
		        	PRELDESC.PRDID = rs.getInt(2);
		        	PRELDESC.PRDESC = rs.getString(3).trim();
		        	PRELDESCS.add(PRELDESC);
		        }
		        
		        rs.close();
		        stmt.close();
		        
		        log.info("..Retrieved " + PRELDESCS.size() + " Tags Records");
		        
		        json = new Gson().toJson(PRELDESCS);
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
		    connectionHelper.closeConnection(conn);
    	}
	    catch(Exception ex)
	    {
	    	authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	    	authorizationRecord.returnErrorMessage = ex.getMessage();
    	
	    	log.error(authorizationRecord.returnMessage);
	    	log.error(ex.getMessage());			
		
	    	authorizationRecord.isError = true;
	    	json = "An error occured during the authenticate process.";
	    	connectionHelper.closeConnection(conn);
	    	return(json);
	    }
	    return(json);
	}
	
	
	
	@GET
	@Path("/insertComment/{lib}/{userid}/{noteText}/{KEYN1}/{KEYN2}/{KEYN3}/{N5ID}")
	@Produces(MediaType.APPLICATION_JSON)
	public String insertComment(@PathParam("lib")String lib, @PathParam("userid") String userid, @PathParam("noteText")String noteTextIn, 
							@PathParam("KEYN1")int keyn1In,
							@PathParam("KEYN2")int keyn2In, @PathParam("KEYN3")int keyn3In, @PathParam("N5ID")int n5idin){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
//		Encrypt encrypt = new Encrypt();
		
		String json = "";
		
//		lib = encrypt.decryptLibrary(lib);
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SNOTE2RU06 to insert Notes");
		        
				CallableStatement stmt = null;
			    String query = "CALL SNOTE2RU06(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			    
			    Date currentDate = new Date();
			    Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			    log.info(currentTimestamp);
			    try {
			        stmt = conn.prepareCall(query);
			        
			        int Seq = 1;
			        String blanks = "";
			        String samenote = "Y";
			        String program = "SNOTE2RU06";
			        
			        stmt.registerOutParameter(16, Types.INTEGER);
			        stmt.registerOutParameter(17, Types.INTEGER);
			        stmt.registerOutParameter(18, Types.CHAR);
			        stmt.setInt(1, keyn1In);
			        stmt.setInt(2, keyn2In);
			        stmt.setInt(3, keyn3In);
			        stmt.setString(4, noteTextIn);
			        stmt.setString(5, samenote);
			        stmt.setString(6, blanks);
			        stmt.setString(7, blanks);
			        stmt.setString(8, blanks);
			        stmt.setString(9, samenote);
			        stmt.setTimestamp(10, currentTimestamp);
			        stmt.setTimestamp(11, currentTimestamp);
			        stmt.setString(12,userid);
			        stmt.setString(13,userid);
			        stmt.setString(14,program);
			        stmt.setString(15,program);
			        stmt.setInt(16,n5idin);
			        stmt.setInt(17,Seq);

			        stmt.executeUpdate();			        
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    log.info("..successfully inserted new comment for Note ID: " + n5idin);
			    
			    authorizationRecord.isError = false;
		        json = new Gson().toJson(authorizationRecord);
		        connectionHelper.closeConnection(conn);
	    	}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	public void createDirectory(AS400 sys, String path){
		// Create a file object that represents the directory.
	        IFSFile aDirectory = new IFSFile(sys, path);

	        // Create the directory.
	        try {
				if (aDirectory.mkdir())
				   log.info("Create directory was successful");
				else
				{
				   // The create directory failed.
				   // If the object already exists, find out if it is a 
				   // directory or file, then display a message.
				   if (aDirectory.exists())
				   {
				      if (aDirectory.isDirectory())
				         log.info("Directory already exists");
				      else
				         log.info("Directory with this name already exists");
				   }
				   else
				      log.info("Create Directory failed");
				}
			} catch (IOException e) {			
				log.warn(e.getMessage());
			}
		  
	  }
}

