package com.cobrapm.services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.ModuleTaskRecord;
import com.cobrapm.websocket.MessageRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("NotificationService")
public class NotificationService {
	
	ModuleTaskRecord CURRENT_APPLICATION = new ModuleTaskRecord();

	static Properties props;
	static Properties resourceBundle;
	static Connection conn = null;
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(NotificationService.class.getName());
	
	@POST
	@Path("/retrieveNotifications")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String retrieveNotifications(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<MessageRecord> masterRecord = new ArrayList<MessageRecord>();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    String userid = jobject.get("userid").toString();
	    userid = userid.substring(1, userid.length()-1);
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SPMSGQCR01 to get Notifications");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPMSGQCR01(?)";
			    int counter = 0;

			    try {
			    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			        stmt.setString(1, userid);

			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while(rs.next()){
			        	counter++;
			        	MessageRecord tmp = new MessageRecord();
			        	tmp.MsgType = rs.getString(1).trim();
 						if(tmp.MsgType.equals("PNOTE2")){
 							tmp.MsgTitle = "New Note";
 						}
 						tmp.MsgText = rs.getString(2).trim();
 						try{
 							tmp.MsgKey1 = rs.getString(4).trim();
 							tmp.MsgKey2 = rs.getString(5).trim();
 						}catch(Exception e){
 					  	
 						}
 						tmp.MsgRead = rs.getString(6).trim();
 						masterRecord.add(tmp);
 					}
			        
			        rs.close();
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    log.info("..Retrieved "+ counter +" Notifications");
			    
			    log.info("..calling SP: SPNRELCR01 to getNotes Tags");
		        
			    stmt = null;
			    for(MessageRecord rec : masterRecord){
			    	switch(rec.MsgType){
 						case "PNOTE2" :
 							if(rec.MsgKey1 != ""){
 								query = "CALL SPNRELCR01(?)";
 								try {
 									stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
 									stmt.setInt(1, Integer.parseInt(rec.MsgKey1));		
 									stmt.execute();
 						
 									ResultSet rs = stmt.getResultSet();
 									int count = 0;
 									while(rs.next()){
 										if(count == 0){
 											rec.MsgText += " Related To " + rs.getString(1).trim();
 											count ++;
 										} else {
 											rec.MsgText += ", " + rs.getString(1).trim();
 										}
 									}
 			   
 									stmt.close();
 			        
 								} catch (Exception e ) {
 							log.info(e);
 								} finally {
 									if (stmt != null) { stmt.close(); }
 								}
 							}
			    	}
			    }
				 connectionHelper.closeConnection(conn);
				 json = new Gson().toJson(masterRecord);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/setNotifications")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String setNotifications(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<MessageRecord> masterRecord = new ArrayList<MessageRecord>();
//		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    String userid = jobject.get("userid").toString();
	    userid = userid.substring(1, userid.length()-1);
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SPMSGQRU01 to set Notifications");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPMSGQRU01(?)";
			    
			    try {
			    	stmt = conn.prepareCall(query);

			        stmt.setString(1, userid);

			        stmt.executeUpdate();			        
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    connectionHelper.closeConnection(conn);
	    	}
			    catch(Exception ex)
			    {
			        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
			        authorizationRecord.returnErrorMessage = ex.getMessage();
			    	
					log.error(authorizationRecord.returnMessage);
					log.error(ex.getMessage());			
					
					authorizationRecord.isError = true;
					json = "An error occured during the authenticate process.";
					connectionHelper.closeConnection(conn);
				    return(json);
			    }
			return(json);
	   }
}	

