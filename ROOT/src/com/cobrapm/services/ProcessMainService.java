package com.cobrapm.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.doa.JobFunctionRetrievalRecord;
import com.cobrapm.doa.PINFORecord;
import com.cobrapm.doa.ProcessDisplayRecord;
import com.cobrapm.doa.ProcessTypeRecord;
import com.cobrapm.process.PPMREL;
import com.cobrapm.process.ProcessMainRecord;
import com.cobrapm.process.ProcessTaskRecord;
import com.cobrapm.process.UDFData;
import com.cobrapm.process.UDFSubData;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.xml.internal.ws.org.objectweb.asm.Type;
   
@Path("ProcessMainService")  
public class ProcessMainService { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(ProcessMainService.class.getName());
	
	 @POST
	 @Path("/getProcess")
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public String getProcess(String masterString){
	    	 
		CONNECTOR CONNECTOR = new CONNECTOR(); 
	   	Connection conn = null;
	   	
	   	ProcessMainRecord ProcessMainRecord = new ProcessMainRecord();
    	
	   		
	   	log.info("..mapping JSON string");
	   		
	   	JsonElement jelement = new JsonParser().parse(masterString);
	   	JsonObject  jobject = jelement.getAsJsonObject();
	   	String lib = jobject.get("lib").toString();
	   	lib = lib.substring(1, lib.length()-1);
	   	String userid = jobject.get("user").toString();
	   	userid = userid.substring(1, userid.length()-1);
	   	int TKNO = jobject.get("TKNO").getAsInt();
	    
    	String json = "";
    	
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPPMPCR00(?)";				
		    				    			    
		    try {
		    	
		        stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, TKNO);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	ProcessMainRecord.TKNO = rs.getInt("TKNO");
		        	ProcessMainRecord.TKTYPE = rs.getString("TKTYPE");
		        	ProcessMainRecord.AssignedTo = rs.getString("TKASSIGN");
		        	ProcessMainRecord.ShortDesc = rs.getString("TKTYPSD").trim();
		        	ProcessMainRecord.LongDesc = rs.getString("TKTYPLD");
		        	ProcessMainRecord.SubType = rs.getString("TKSUBTYP");
		        	ProcessMainRecord.Status = rs.getString("TKSTAT");
		        	ProcessMainRecord.Freq = rs.getString("TKFREQ");
		        	ProcessMainRecord.Rollover = rs.getString("TKROLLOVER");
		        	ProcessMainRecord.RolloverFreq = rs.getString("TKROLLFREQ");
		        	ProcessMainRecord.OddMthsFreq = rs.getString("TKTSKOM");
		        	ProcessMainRecord.Approved = rs.getString("TKAPPROVED");
		        	ProcessMainRecord.Amount = rs.getBigDecimal("TKAMOUNT");
		        	ProcessMainRecord.CaseNumber = rs.getString("TKCASE");
		        	ProcessMainRecord.Severity = rs.getString("TKSEVERITY");
		        	ProcessMainRecord.EstStartDate =  rs.getString("TKESTST");
		        	ProcessMainRecord.ActStartDate = rs.getString("TKACTST");
		        	ProcessMainRecord.SynchronizeYN = rs.getString("TKSYNC");
		        	ProcessMainRecord.EstCompDate = rs.getString("TKESTCMP");
		        	ProcessMainRecord.ActCompDate = rs.getString("TKACTCMP");
		        	ProcessMainRecord.ExpDuration = rs.getInt("TKEXPDUR");
		        	ProcessMainRecord.ActDuration = rs.getInt("TKACTDUR");
		        	ProcessMainRecord.CompBeforeRoll = rs.getString("TKCMPB4RO");
		        	ProcessMainRecord.PreviousTKNO = rs.getInt("TKPREVNO");
		        	ProcessMainRecord.PRID = rs.getInt("TKPRID");
		        	ProcessMainRecord.Closed = rs.getString("TKCLOSED");
		        	ProcessMainRecord.NextProcess = rs.getString("TKNXTTYPE");
		        	ProcessMainRecord.NextProcConfirm = rs.getString("TKNXTCFM");
		        	ProcessMainRecord.Cat = rs.getString("TKCAT");
		        	ProcessMainRecord.SubCat = rs.getString("TKSUBCAT");
		        	ProcessMainRecord.RecurInterval = rs.getInt("TKINTERVAL");
		        	ProcessMainRecord.WeekDay = rs.getInt("TKWKDAY");
		        	ProcessMainRecord.Depend = rs.getString("TKDEPEND");
		        	ProcessMainRecord.Days = rs.getInt("TKFREQDAYS");
		        	ProcessMainRecord.DayofMonth = rs.getInt("TKMODAY");
		        	ProcessMainRecord.SYSCRT = rs.getString("SYSCRT");
		        	ProcessMainRecord.SYSUPD = rs.getString("SYSUPD");
		        	ProcessMainRecord.SYSCRTBY = rs.getString("SYSCRTBY");
		        	ProcessMainRecord.SYSUPDBY = rs.getString("SYSUPDBY");
		        	ProcessMainRecord.PGMCRTBY = rs.getString("PGMCRTBY");
		        	ProcessMainRecord.PGMUPDBY = rs.getString("PGMUPDBY");
		        	ProcessMainRecord.ProcessType = rs.getString("TKTMPNM");
		        	ProcessMainRecord.RecentNote = rs.getString("@LATESTNOTE");
		        	ProcessMainRecord.AssignedBy =  rs.getString(66);
		        	ProcessMainRecord.AssignedByTime =  rs.getString("TKASBYTS");
		        	ProcessMainRecord.StopRecurDate =  rs.getString("TKSTOPDT");
		        	ProcessMainRecord.OriginalUser = rs.getString("TKORGUSR");
		        	ProcessMainRecord.OriginalUserTime = rs.getString("TKORGTS");
		        	ProcessMainRecord.CurUser = rs.getString("TKCURUSR");
		        	ProcessMainRecord.CurUserTime = rs.getString("TKCURTS");
		        	ProcessMainRecord.RecurMonth = rs.getInt("TKMONTH");
		        	ProcessMainRecord.NumOfSubProc = rs.getInt("TKSPRUNN");
		        	ProcessMainRecord.SubTKNO = rs.getInt("TKSPTKNO");
		        	ProcessMainRecord.SubTSKNO = rs.getInt("TKSPTSKNO");
		        	ProcessMainRecord.SubTSKSNO = rs.getInt("TKSPTSKSNO");
		        	ProcessMainRecord.Estimate = rs.getBigDecimal("TKESTHRS");
		        	ProcessMainRecord.EstHoursOvrride = rs.getString("TKESTHRSO");
		        	ProcessMainRecord.CreatedBy = rs.getString(64);
		        	ProcessMainRecord.LastUpdateBy = rs.getString(65);
		        	ProcessMainRecord.SubCat2 = rs.getString("TKSUBCAT2");
		        	ProcessMainRecord.SubCat3 = rs.getString("TKSUBCAT3");
		        	ProcessMainRecord.ProcessSubType = rs.getString(69);
		        	ProcessMainRecord.Category = rs.getString(70);
		        	ProcessMainRecord.SubCategory = rs.getString(71);
		        	ProcessMainRecord.SubCategory2 = rs.getString(72);
		        	ProcessMainRecord.SubCategory3 = rs.getString(73);
		        	ProcessMainRecord.Frequency = rs.getString(74);
		        	log.info("Freq: " + ProcessMainRecord.Frequency);
//		        	
//		        	if(ProcessMainRecord.AssignedTo != null){
//		        		ProcessMainRecord.AssignedTo =  ProcessMainRecord.AssignedTo.trim();
//		        	}
//		        	if(ProcessMainRecord.ShortDesc != null){
//		        		ProcessMainRecord.ShortDesc =  ProcessMainRecord.ShortDesc.trim();
//		        	}    	
//		        	if(ProcessMainRecord.LongDesc != null){
//		        		ProcessMainRecord.LongDesc =  ProcessMainRecord.LongDesc.trim();
//		        	}
//		        	if(ProcessDisplayRecord.TaskName != null){
//		        		ProcessDisplayRecord.TaskName =  ProcessDisplayRecord.TaskName.trim();
//		        	}
//		        	if(ProcessDisplayRecord.RelatedTo != null){
//		        		ProcessDisplayRecord.RelatedTo =  ProcessDisplayRecord.RelatedTo.trim();
//		        	}
//		        	if(ProcessDisplayRecord.HardDeadline != null){
//		        		if(ProcessDisplayRecord.HardDeadline.equals("N")){
//		        			ProcessDisplayRecord.HardDeadline = "No";
//		        		} else {
//		        			ProcessDisplayRecord.HardDeadline = "Yes";
//		        		}
//		        	}
//		        	ProcessDisplayRecord.rownumber = rownumber;
//		        	ProcessDisplayRecords.add(ProcessDisplayRecord);
//		        	rownumber += 1;
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
		        
		    log.info("Retried Process Record, now retrieving Relateds");
		    
		    log.info("..calling SP: SPRELCR00 to retrieve all Note ID's and Descriptions");
	        
			stmt = null;
		    query = "CALL SPRELCR00(?,?,?)";
		    
		    try {
		    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    	stmt.setInt(1, ProcessMainRecord.TKNO);
		    	stmt.setInt(2, 0);
		    	stmt.setInt(3, 0);
		    	
		    	stmt.execute();			        
		        
		    	ResultSet rs = stmt.getResultSet();
		    	String relText = "";
		    	while (rs.next()) {
		    		PPMREL tmpRecord = new PPMREL();
		    		tmpRecord.TLTKNO = rs.getInt(1);
		    		tmpRecord.TLTSKNO = rs.getInt(2);
		    		tmpRecord.TLTSKSNO = rs.getInt(3);
		    		tmpRecord.RecType = rs.getString(4);
		    		tmpRecord.RecID = rs.getInt(5);
		    		tmpRecord.RelID = rs.getInt(6);
		    		tmpRecord.Desc = rs.getString(7);
		    		ProcessMainRecord.relTos.add(tmpRecord);
		    	}
		    	rs.close();
		    	stmt.close();
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		    	if (stmt != null) { stmt.close(); }
		    }  
		    
		    log.info("..Retrieving Tasks for Process");
			
			stmt = null;
		    query = "CALL SPPMTCR00(?)";				
		    				    			    
		    try {
		    	
		        stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, TKNO);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	ProcessTaskRecord tmpRecord = new ProcessTaskRecord();
		        	tmpRecord.TSTKNO = rs.getInt("TSTKNO");
		        	tmpRecord.TSTSKNO = rs.getInt("TSTSKNO");
		        	tmpRecord.TSTSKSNO = rs.getInt("TSTSKSNO");
		        	tmpRecord.TSTYPE = rs.getString("TSTYPE");
		        	tmpRecord.Assigned = rs.getString("TSASSIGN");
		        	tmpRecord.Group = rs.getString("TSQUEUE");
		        	tmpRecord.Desc = rs.getString("TSTYPED").trim();
		        	tmpRecord.Frequency = rs.getString("TSFREQ");
		        	tmpRecord.Rollover = rs.getString("TSROLLOVER");
		        	tmpRecord.Status = rs.getString("TSSTAT");
		        	tmpRecord.EstStartDate =  rs.getString("TSSTARTE");
		        	tmpRecord.ActStartDate = rs.getString("TSSTARTA");
		        	tmpRecord.DaysCompInd = rs.getString("TSDYCMPIND");
		        	tmpRecord.DaysComp = rs.getInt("TSDYCMP");
		        	tmpRecord.DueDate = rs.getString("TSDUEDT");
		        	tmpRecord.ActCompDate = rs.getString("TSACTCMP");
		        	tmpRecord.ExpDuration = rs.getInt("TSEXPDUR");
		        	tmpRecord.ActDuration = rs.getInt("TSACTDUR");
		        	tmpRecord.ReasonCodeTable = rs.getString("TSRCTB");
		        	tmpRecord.ReasonCodeReq = rs.getString("TSRCRQ");
		        	tmpRecord.ReasonCode = rs.getString("TSRCSEL");
		        	tmpRecord.Severity = rs.getString("TSSEVERITY");
		        	tmpRecord.previousStatus = rs.getString("TSPRID");
		        	tmpRecord.Order = rs.getInt("TSORDER");
		        	tmpRecord.Interval = rs.getInt("TSINTERVAL");
		        	tmpRecord.UserTaskStatus = rs.getString("TSUSTAT");
		        	tmpRecord.AutoStart = rs.getString("TSASOC");
		        	tmpRecord.Depend = rs.getString("TSDEPEND");
		        	tmpRecord.NextRecurReleased = rs.getString("TSNXREL");
		        	tmpRecord.TaskID = rs.getInt("TSTASKID");
		        	tmpRecord.WeekDay = rs.getString("TSWKDAY");
		        	tmpRecord.MonthDay = rs.getInt("TSMODAY");
		        	tmpRecord.FreqDays = rs.getInt("TSFREQDAYS");
		        	tmpRecord.Closed = rs.getString("TSCLOSED");
		        	tmpRecord.EstHours = rs.getBigDecimal("TSESTHRS");
		        	tmpRecord.SYSCRT = rs.getString("SYSCRT");
		        	tmpRecord.SYSUPD = rs.getString("SYSUPD");
		        	tmpRecord.SYSCRTBY = rs.getString("SYSCRTBY");
		        	tmpRecord.SYSUPDBY = rs.getString("SYSUPDBY");
		        	tmpRecord.PGMCRTBY = rs.getString("PGMCRTBY");
		        	tmpRecord.PGMUPDBY = rs.getString("PGMUPDBY");
		        	tmpRecord.RecentNote = rs.getString("@LATESTNOTE");
		        	tmpRecord.AssignedBy =  rs.getString("TSASBYUS");
		        	tmpRecord.AssignedByTime =  rs.getString("TSASBYTS");
		        	tmpRecord.StopRecurDate =  rs.getString("TSSTOPDT");
		        	tmpRecord.OriginalUser = rs.getString("TSORGUSR");
		        	tmpRecord.OriginalUserTime = rs.getString("TSORGTS");
		        	tmpRecord.CurUser = rs.getString("TSCURUSR");
		        	tmpRecord.CurUserTime = rs.getString("TSCURTS");
		        	tmpRecord.RecurMonth = rs.getInt("TSMONTH");
		        	tmpRecord.SupProcType = rs.getString("TSSPTKTYPE");
		        	tmpRecord.SubTKNO = rs.getInt("TSSPTKNO");
		        	tmpRecord.Cat = rs.getString("TSCAT");
		        	tmpRecord.SubCat = rs.getString("TSSUBCAT");
		        	tmpRecord.SubCat2 = rs.getString("TSSUBCAT2");
		        	tmpRecord.SubCat3 = rs.getString("TSSUBCAT3");
		        	
//		        	if(ProcessMainRecord.AssignedTo != null){
//		        		ProcessMainRecord.AssignedTo =  ProcessMainRecord.AssignedTo.trim();
//		        	}
//		        	if(ProcessMainRecord.LongDesc != null){
//		        		ProcessMainRecord.LongDesc =  ProcessMainRecord.LongDesc.trim();
//		        	}
//		        	if(ProcessDisplayRecord.TaskName != null){
//		        		ProcessDisplayRecord.TaskName =  ProcessDisplayRecord.TaskName.trim();
//		        	}
//		        	if(ProcessDisplayRecord.RelatedTo != null){
//		        		ProcessDisplayRecord.RelatedTo =  ProcessDisplayRecord.RelatedTo.trim();
//		        	}
//		        	if(ProcessDisplayRecord.HardDeadline != null){
//		        		if(ProcessDisplayRecord.HardDeadline.equals("N")){
//		        			ProcessDisplayRecord.HardDeadline = "No";
//		        		} else {
//		        			ProcessDisplayRecord.HardDeadline = "Yes";
//		        		}
//		        	}
//		        	ProcessDisplayRecord.rownumber = rownumber;
//		        	ProcessDisplayRecords.add(ProcessDisplayRecord);
//		        	rownumber += 1;
		        	ProcessMainRecord.Tasks.add(tmpRecord);
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
		    
		    log.info("..getting User Defined Records for Process");
	        
			stmt = null;
		    query = "CALL SPUDFDCR00(?)";
		    
		    try {
		    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    	stmt.setInt(1, ProcessMainRecord.TKNO);
		    	
		    	stmt.execute();			        
		        
		    	ResultSet rs = stmt.getResultSet();

		    	while (rs.next()) {
		    		UDFData tmpRecord = new UDFData();
		    		tmpRecord.DTKNO = rs.getInt(1);
		    		tmpRecord.DTSKNO = rs.getInt(2);
		    		tmpRecord.DTSKSNO = rs.getInt(3);
		    		tmpRecord.DSEQ = rs.getInt(4);
		    		tmpRecord.DATA = rs.getString(5);
		    		tmpRecord.DSVAL = rs.getString(6);
		    		ProcessMainRecord.UDFs.add(tmpRecord);
		    	}
		    	rs.close();
		    	stmt.close();
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		    	if (stmt != null) { stmt.close(); }
		    }  
		    
			connectionHelper.closeConnection(conn);
			
			json = new Gson().toJson(ProcessMainRecord);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return json;
      }   	
     
     @GET  
     @Path("/setStatus/{lib}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/{ProcessDisplayRecords}/{status}/{subproc}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String setStatus(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout, @PathParam("ProcessDisplayRecords") String ProcessDisplayRecordString,
    		 @PathParam("status") String status, @PathParam("subproc") boolean subproc) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	try
		{	
  			
    		String displayDecode = "";
    		try {
    			displayDecode = URLDecoder.decode(ProcessDisplayRecordString, "UTF-8");
    		} catch (UnsupportedEncodingException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
    		
    		ObjectMapper mapper = new ObjectMapper();
    		ArrayList<ProcessDisplayRecord> ProcessDisplayRecords = mapper.readValue(displayDecode, new TypeReference<ArrayList<ProcessDisplayRecord>>(){});
//    		ProcessDisplayRecord PDrec = new ProcessDisplayRecord();
    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "";				
		    
		    String statusNum = "";
		    switch(status){
		    	case "S": 
		    		statusNum = "0";
		    		break;
		    	case "C": 
		    		statusNum = "1";
		    		break;
		    	case "I": 
		    		statusNum = "3";
		    		break;
		    	case "H": 
		    		statusNum = "5";
		    		break;
 		    }
		    
		    for(ProcessDisplayRecord PDrec: ProcessDisplayRecords){
		    	try {
		    		query = "CALL SPMTRU09(?,?,?,?,?,?,?,?)";
		    		java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());		
		    		int MODULEID = 0;
		    		int TASKID = 0;
		    	
		    		stmt = conn.prepareCall(query);
		    		stmt.setInt(1, PDrec.TKNO);
		    		stmt.setInt(2, PDrec.TSTSKNO);
		    		stmt.setInt(3, PDrec.TSTSKSNO);
		    		stmt.setString(4, statusNum);
		    		stmt.setDate(5, timeNow);
		    		stmt.setString(6, status);
		    		stmt.setInt(7, MODULEID);
		    		stmt.setInt(8, TASKID);
		    		stmt.executeUpdate();	        
		    		stmt.close();
		    		
		    		if(subproc && PDrec.SubProc != null){
		    			query = "CALL SPPMTRU02(?,?,?,?)";
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, PDrec.TKNO);
			    		stmt.setInt(2, PDrec.TSTSKNO);
			    		stmt.setInt(3, PDrec.TSTSKSNO);
			    		stmt.setString(4, PDrec.SubProc);
			    		stmt.executeUpdate();	        
			    		stmt.close();
		    		}
		        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    	
		    log.info("..started task records");		    
		    
			connectionHelper.closeConnection(conn);	
			
	
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		
  		return "Success";
     }   
     
     @GET  
     @Path("/getJobFunctions/{lib}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/")  
     @Produces(MediaType.APPLICATION_JSON)
     public String getJobFunctions(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	JobFunctionRetrievalRecord JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
    	ArrayList<JobFunctionRetrievalRecord> JobFunctionRetrievalRecords = new ArrayList<JobFunctionRetrievalRecord>();
//    	Encrypt encrypt = new Encrypt();
    	
    	String json = "";
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	try
		{	
  			    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPRLJFCR00()";				
		    				    			    
		    try {
		    	
	    		stmt = conn.prepareCall(query);			        
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
		        	
		        	JobFunctionRetrievalRecord.KAJOBROLE = rs.getString("KAJOBROLE");	
		        	JobFunctionRetrievalRecord.KAJOBDESC = rs.getString("KAJOBDESC");
		        	JobFunctionRetrievalRecord.KAMANGR = rs.getString("KAMANGR");
		        	JobFunctionRetrievalRecord.GRPNAME = rs.getString("GRPNAME");
		        	
		        	JobFunctionRetrievalRecord.DisplayText = JobFunctionRetrievalRecord.KAJOBDESC;	        	
		        	
		        	JobFunctionRetrievalRecords.add(JobFunctionRetrievalRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved job function records");
		    
			connectionHelper.closeConnection(conn);	
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(JobFunctionRetrievalRecords);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		
  		return json;
     }
     
     public String setStatus(String TKSTAT, java.util.Date T4BACTST, java.util.Date TKESTCMP, java.util.Date T4BACTCMP, int T4BWNDAYS, int T4BOTDAYS){
    	 
    	String FUTURE_STATUS = "4";
    	String HOLD_STATUS = "5";
        java.util.Date CURRENTDATE = new java.util.Date();  
    	 
    	if (TKSTAT.equals(FUTURE_STATUS)){					
 			return("Future");
    	}
    	if (TKSTAT.equals(HOLD_STATUS)){
    		return("Paused");
    	}
 		if (T4BACTST == null){
 			return("Not Started");
 		}
 		if (TKESTCMP == null){
 			return("On Time");
 		}
 		if (T4BACTCMP == null){
 			if (TKSTAT.equals(FUTURE_STATUS)){
 				return("Future");
 			} else {
 				int od = 0;
				try {						
					DateTime dt1 = new DateTime(CURRENTDATE);
					DateTime dt2 = new DateTime(TKESTCMP); 						
					od = Days.daysBetween(dt1, dt2).getDays();	
				} catch (Exception e){
					od = 999;
				}
 				if (CURRENTDATE.after(TKESTCMP)){
 					if (T4BWNDAYS < 0){
 						return("Behind Schedule");
 					} else {
 						if (od <= T4BOTDAYS && od > 0){
 							return("Near Due");
 						} else {
 							return("On Time");
 						}
 					}
 				} else {
 					return("On Time");
 				}
 			}
 		} else {
 			int od  = 0;
 			try {
 				DateTime dt1 = new DateTime(T4BACTCMP);
				DateTime dt2 = new DateTime(TKESTCMP); 						
				od = Days.daysBetween(dt1, dt2).getDays();	
 			} catch (Exception e){
 				od = 999;
 			}
 			if (od > T4BWNDAYS){
 				return("Completed Behind Schedule");
 			} else {
 				if (od > T4BOTDAYS) {
 					return("Completed In Danger");
 				} else {
 					return("Completed On Time");
 				}
 			}
 		}
     }
     
     @POST
   	@Path("/updateProcess")
   	@Consumes(MediaType.APPLICATION_JSON)
   	@Produces(MediaType.APPLICATION_JSON)
   	public String updateProcess(String masterString){
   	CONNECTOR CONNECTOR = new CONNECTOR(); 
   		Connection conn = null;
   		
   		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
   		ProcessMainRecord ProcessMainRecord = new ProcessMainRecord();
   		
   		log.info("..mapping JSON string");
   		
   		JsonElement jelement = new JsonParser().parse(masterString);
   	    JsonObject  jobject = jelement.getAsJsonObject();
   	    String lib = jobject.get("lib").toString();
   	    lib = lib.substring(1, lib.length()-1);
   	    String userid = jobject.get("userid").toString();
   	    userid = userid.substring(1, userid.length()-1);
   	    String Proc_tmp = jobject.get("Process").toString();
   	    
   	    ObjectMapper mapper = new ObjectMapper();
   	    try {
   	    	ProcessMainRecord = mapper.readValue(Proc_tmp, new TypeReference<ProcessMainRecord>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
   		
   	    java.sql.Date currentDate = new Date(Calendar.getInstance().getTimeInMillis());	
	    Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
	    BigDecimal estHrs = BigDecimal.ZERO;
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date tmpDate = null;
		try {
			tmpDate = formatter.parse("01/01/0001");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    java.sql.Date nullDate = new Date(tmpDate.getTime());
	    
   		String json = "";
    	
   		try {	
   			    		
   			CONNECTOR = connectionHelper.getConnection(lib);
   			conn = CONNECTOR.conn;
   			
   			//To Do - Handle Recurring Task stuff
  			log.info("..calling stored procedure");
  			CallableStatement stmt = null;
  		    String query = "";
  		    
  			for(ProcessTaskRecord taskRecord : ProcessMainRecord.Tasks){
  				 stmt = null;
  				 query = "CALL SPMTRU07(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  				//Sequential?
  				try {
  					if(taskRecord.Mode.equals("")){
  						taskRecord.Mode = "U";
  					}
  		  		    if(taskRecord.Status != "5"){
  		  		    	taskRecord.previousStatus = taskRecord.Status;
  		  		    }
  		  		    if(taskRecord.previousStat.equals("5")){
  		  		    	taskRecord.Status = taskRecord.previousStatus;
  		  		    }
  		  		    if(taskRecord.EstHours != null){
  		  		    	estHrs = estHrs.add(taskRecord.EstHours);
  		  		    }
  		  		    if(taskRecord.Mode.equals("I")){
  		  		    	taskRecord.SYSCRT = currentTimestamp.toString();
  		  		    	taskRecord.SYSCRTBY = userid;
  		  		    	taskRecord.PGMCRTBY = "SPMTRU07";
  		  		    }
  		  		    taskRecord.SYSUPD = currentTimestamp.toString();
	  		    	taskRecord.SYSUPDBY = userid;
	  		    	taskRecord.PGMUPDBY = "SPMTRU07";
	  		    	if(taskRecord.EstStartDate.equals("")){
	  		    		taskRecord.EstStartDate = nullDate.toString();
	  		    	}
	  		    	if(taskRecord.ActStartDate.equals("")){
	  		    		taskRecord.ActStartDate = nullDate.toString();
	  		    	}
	  		    	if(taskRecord.DueDate.equals("")){
	  		    		taskRecord.DueDate = nullDate.toString();
	  		    	}
	  		    	if(taskRecord.ActCompDate.equals("")){
	  		    		taskRecord.ActCompDate = nullDate.toString();
	  		    	}
	  		    	if(taskRecord.Status.equals("1") && taskRecord.ActCompDate.equals("0001-01-01")){
	  		    		taskRecord.ActCompDate = currentDate.toString();
	  		    	}
	  		    	if(taskRecord.Status.equals("0") && ProcessMainRecord.ActStartDate.equals("")){
  						ProcessMainRecord.ActStartDate = taskRecord.ActStartDate;
  					}
  		  		    
  					stmt = conn.prepareCall(query);	
  					
  					stmt.registerOutParameter(3, Type.INT);
  					stmt.registerOutParameter(4, Type.INT);
  					
  					stmt.setString(1, taskRecord.Mode);
  					stmt.setInt(2, taskRecord.TSTKNO);
  					stmt.setInt(3, taskRecord.TSTSKNO);
  					stmt.setInt(4, taskRecord.TSTSKSNO);
  					stmt.setString(5, taskRecord.TSTYPE);
  					stmt.setString(6, taskRecord.Assigned);
  					stmt.setString(7, taskRecord.Group);
  					stmt.setString(8, taskRecord.Desc);
  					stmt.setString(9, taskRecord.Frequency);
  					stmt.setString(10, taskRecord.Rollover);
  					stmt.setString(11, taskRecord.Status);
  					stmt.setString(12, taskRecord.EstStartDate);
  					stmt.setString(13, taskRecord.ActStartDate);
  					stmt.setString(14, taskRecord.DaysCompInd);
  					stmt.setInt(15, taskRecord.DaysComp);
  					stmt.setString(16, taskRecord.DueDate);
  					stmt.setString(17, taskRecord.ActCompDate);
  					stmt.setInt(18, taskRecord.ExpDuration);
  					stmt.setInt(19, taskRecord.ActDuration);
//  					stmt.setString(20, taskRecord.ReasonCode);
 // 					stmt.setString(21, taskRecord.ReasonCodeReq);
  //					stmt.setString(22, taskRecord.ReasonCodeTable);
  					stmt.setString(20, "");
   					stmt.setString(21, "");
 					stmt.setString(22, "");
  					stmt.setString(23, taskRecord.Severity);
  					stmt.setString(24, taskRecord.previousStatus);
  					stmt.setInt(25, 0);
  					stmt.setInt(26, 0);
  					stmt.setInt(27, taskRecord.Order);
  					stmt.setInt(28, taskRecord.Interval);
  					stmt.setString(29, taskRecord.UserTaskStatus);
  					stmt.setString(30, taskRecord.AutoStart);
  					stmt.setString(31, taskRecord.Depend);
  					stmt.setString(32, taskRecord.NextRecurReleased);
  					stmt.setString(33, "");
  					stmt.setInt(34, taskRecord.TaskID);
  					stmt.setString(35, taskRecord.WeekDay);
  					stmt.setInt(36, taskRecord.MonthDay);
  					stmt.setInt(37, taskRecord.FreqDays);
  					stmt.setString(38, taskRecord.Closed);
  					stmt.setString(39, taskRecord.StopRecurDate);
  					stmt.setInt(40, taskRecord.MonthDay);
  					stmt.setString(41, taskRecord.SupProcType);
  					stmt.setInt(42, taskRecord.SubTKNO);
  					stmt.setString(43, taskRecord.Cat.replaceAll("\\s+", ""));
  					stmt.setString(44, taskRecord.SubCat.replaceAll("\\s+", ""));
  					stmt.setString(45, taskRecord.SubCat2.replaceAll("\\s+", ""));
  					stmt.setString(46, taskRecord.SubCat3.replaceAll("\\s+", ""));
  					stmt.setBigDecimal(47, taskRecord.EstHours);
  					stmt.setString(48, taskRecord.SYSCRT);
  					stmt.setString(49, taskRecord.SYSUPD);
  					stmt.setString(50, taskRecord.SYSCRTBY);
  					stmt.setString(51, taskRecord.SYSUPDBY);
  					stmt.setString(52, taskRecord.PGMCRTBY);
  					stmt.setString(53, "SPMTRU07");
  					stmt.setInt(54, 0);
  					stmt.setInt(55, 0);
 	    		
  					stmt.executeUpdate();	            			        		        
  					stmt.close();
  			        
  				} catch (SQLException e ) {
  					log.warn(e);
  					return "Error";
  				} finally {
  					if (stmt != null) { stmt.close(); }
  				}			
   			
  				log.info("..Update Process Number: " + ProcessMainRecord.TKNO);
  				if(taskRecord.Depend.equals("Y")){
  					stmt = null;
  		  		    query = "CALL SPMTRU08(?,?,?,?,?)";
  					// Did this used to be an open task? And we're not putting it on hold?
  					if(taskRecord.previousStat.equals("0") && !taskRecord.Status.equals("5")
  							&& !taskRecord.Status.equals("4")){
  						try {
  			 	    		stmt = conn.prepareCall(query);	
  			 	    		stmt.registerOutParameter(5, Types.CHAR);
  			 	    		stmt.setInt(1, taskRecord.TSTKNO);
  			 	    		stmt.setInt(2, taskRecord.Order);
  			 	    		stmt.setInt(3, 0);
  			 	    		stmt.setInt(4, 0);
  			 	    		
  			 		        stmt.execute();
  			 		        ResultSet rs = stmt.getResultSet();
  			 		        if(stmt.getString(5).equals("Y")){
  			 		        	while (rs.next()) {
  			 		        		for(ProcessTaskRecord taskRec2 : ProcessMainRecord.Tasks){
  			 		        			if(taskRec2.TSTSKNO == rs.getInt(1) && taskRec2.TSTSKSNO == rs.getInt(2)){
  			 		        				taskRec2.Status = rs.getString(3);
  			 		        				taskRec2.EstStartDate = rs.getString(4);
  			 		        				taskRec2.ActStartDate = rs.getString(5);
  			 		        				taskRec2.DueDate = rs.getString(6);
  			 		        				taskRec2.ActCompDate = rs.getString(7);
  			 		        				try{
  			 		        					taskRec2.ExpDuration = rs.getInt(8);
  			 		        					taskRec2.ActDuration = rs.getInt(9);
  			 		        				} catch(NumberFormatException exc){
  			 		        					//Do Nothing
  			 		        				}
  			 		        			}	
  			 		        		}
  			 		        	}
  			 		        }	
  			 		        rs.close();
  			 		        stmt.close();
  			  			        
  			  		    } catch (SQLException e ) {
  			  		    	log.warn(e);
  			  		    	return "Error";
  			  		    } finally {
  			  		        if (stmt != null) { stmt.close(); }
  			  		    }						
  					}
  				}
  				//TODO here deal with sub procs 
  			}				
  		    //TODO deal with recurring procs
  			if(ProcessMainRecord.Freq.equals("2")){
  				
  			} else {
  				if(ProcessMainRecord.EstHoursOvrride != "Y"){
  					ProcessMainRecord.Estimate = estHrs;
  				}
  				if(ProcessMainRecord.Status.equals("0")){
  					boolean checker = false;
  					for(ProcessTaskRecord tmpRecord : ProcessMainRecord.Tasks){
  						if(tmpRecord.Status != "1" && tmpRecord.Status != "3"){
  							checker = true;
  						}
  					}
  					if(!checker){
  						ProcessMainRecord.Status = "1";
  						ProcessMainRecord.ActCompDate = currentDate.toString();
  					}
  				}
  				try {
  					stmt = null;
  		  		    query = "CALL SPMPRU08(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  		  		    
  					stmt = conn.prepareCall(query);	
  					stmt.setString(1, "U");
  					stmt.setString(2, ProcessMainRecord.Status);
  					stmt.setInt(3, ProcessMainRecord.TKNO);
  					stmt.setString(4, ProcessMainRecord.TKTYPE);
  					stmt.setString(5, ProcessMainRecord.AssignedTo);
  					stmt.setString(6, ProcessMainRecord.ShortDesc);
  					stmt.setString(7, ProcessMainRecord.LongDesc);
  					stmt.setString(8, ProcessMainRecord.SubType);
  					stmt.setString(9, ProcessMainRecord.Status);
  					stmt.setString(10, ProcessMainRecord.Freq);
  					stmt.setString(11, ProcessMainRecord.Rollover);
  					stmt.setString(12, ProcessMainRecord.RolloverFreq);
  					stmt.setString(13, ProcessMainRecord.OddMthsFreq);
  					stmt.setString(14, ProcessMainRecord.Approved);
  					stmt.setBigDecimal(15, ProcessMainRecord.Amount);
  					stmt.setString(16, ProcessMainRecord.CaseNumber);
  					stmt.setString(17, ProcessMainRecord.Severity);
  					stmt.setString(18, ProcessMainRecord.EstStartDate);
  					stmt.setString(19, ProcessMainRecord.SynchronizeYN);
  					stmt.setString(20, ProcessMainRecord.EstCompDate);
  					stmt.setString(21, ProcessMainRecord.ActStartDate);
  					stmt.setString(22, ProcessMainRecord.ActCompDate);
  					stmt.setString(23, ProcessMainRecord.NextProcess);
  					stmt.setString(24, ProcessMainRecord.NextProcConfirm);
  					stmt.setString(25, ProcessMainRecord.Cat);
  					stmt.setString(26, ProcessMainRecord.SubCat);
  					stmt.setInt(27, ProcessMainRecord.RecurInterval);
  					stmt.setInt(28, ProcessMainRecord.WeekDay);
  					stmt.setString(29, ProcessMainRecord.Depend);
  					stmt.setInt(30, ProcessMainRecord.Days);
  					stmt.setInt(31, ProcessMainRecord.DayofMonth);
  					stmt.setString(32, ProcessMainRecord.StopRecurDate);
  					stmt.setInt(33, ProcessMainRecord.RecurMonth);
  					stmt.setString(34, ProcessMainRecord.SubCat2);
  					stmt.setString(35, ProcessMainRecord.SubCat3);
  					stmt.setString(36, "");
  					stmt.setBigDecimal(37, ProcessMainRecord.Estimate);
  					stmt.setString(38, ProcessMainRecord.EstHoursOvrride);
  					stmt.setString(39, ProcessMainRecord.SYSCRT);
  					stmt.setString(40, currentTimestamp.toString());
  					stmt.setString(41, ProcessMainRecord.SYSCRTBY);
  					stmt.setString(42, userid);
  					stmt.setString(43, ProcessMainRecord.PGMCRTBY);
  					stmt.setString(44, "SPMPRU08");
  					stmt.setInt(45, 0);
  					stmt.setInt(46, 0);
 	    		
  					stmt.executeUpdate();	            			        		        
  					stmt.close();
  			        
  				} catch (SQLException e ) {
  					log.warn(e);
  					return "Error";
  				} finally {
  					if (stmt != null) { stmt.close(); }
  				}			
   			
  				log.info("..Update Process Number: " + ProcessMainRecord.TKNO);
  			}
  			
  			//TODO next part of recur...force it!
   			//TODO Kick off that next Sub proc!
  			log.info("..calling stored procedure to update Related To's");
  			stmt = null;
	  		query = "CALL SPRELDR00(?,?,?,?)";
  			try {
		  		stmt = conn.prepareCall(query);	
		  		stmt.registerOutParameter(4, Type.CHAR);
	 	    	stmt.setInt(1, ProcessMainRecord.TKNO);
	 	    	stmt.setInt(2, 0);
	 	    	stmt.setInt(3, 0);
	 	    	stmt.executeUpdate();
		  		
	 	    	stmt.close();
	 	    	
	 	    	stmt = null;
		  		query = "CALL SPRELRU00(?,?,?,?,?,?,?)";
		  		
		  		for(PPMREL tmpRecord : ProcessMainRecord.relTos){
		  			stmt = conn.prepareCall(query);	
		  			stmt.setInt(1, ProcessMainRecord.TKNO);
		  			stmt.setInt(2, 0);
		  			stmt.setInt(3, 0);
		  			stmt.setString(4, tmpRecord.RecType);
		  			stmt.setInt(5, tmpRecord.RecID);
		  			stmt.setInt(6, tmpRecord.RelID);
		  			stmt.registerOutParameter(7, Type.CHAR);
		  			stmt.executeUpdate();
	 	    	
		  			stmt.close();
		  		}
	 	    
  			} catch (SQLException e ) {
				log.warn(e);
				return "Error";
			} finally {
				if (stmt != null) { stmt.close(); }
			}
  			
  			log.info("..calling stored procedure to update UDF's");
  			stmt = null;
	  		query = "CALL SPUDFDRU00(?,?,?,?,?,?)";
  			try {
		  		
		  		for(UDFData tmpRecord : ProcessMainRecord.UDFs){
		  			if(tmpRecord.Mode.equals("U") || tmpRecord.Mode.equals("I")){
		  				stmt = conn.prepareCall(query);	
		  				stmt.setInt(1, ProcessMainRecord.TKNO);
		  				stmt.setInt(2, 0);
		  				stmt.setInt(3, 0);
		  				stmt.setInt(4, tmpRecord.DSEQ);
		  				stmt.setString(5, tmpRecord.DATA);
		  				stmt.setString(6, tmpRecord.Mode);
		  				stmt.executeUpdate();
	 	    	
		  				stmt.close();
		  			}
		  		}
	 	    
  			} catch (SQLException e ) {
				log.warn(e);
				return "Error";
			} finally {
				if (stmt != null) { stmt.close(); }
			}
	 	    	
  		} catch(Exception ex) {
  			log.warn(ex);
  			connectionHelper.closeConnection(conn);
  			return "Error";
  		}
     	
     	connectionHelper.closeConnection(conn);	

   		return json;
      }
     
     @POST
  	@Path("/getPINFOCodes")
  	@Consumes(MediaType.APPLICATION_JSON)
  	@Produces(MediaType.APPLICATION_JSON)
  	public String getPINFOCodes(String masterString){
  	CONNECTOR CONNECTOR = new CONNECTOR(); 
  		Connection conn = null;
  		
  		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
  		ArrayList<PINFORecord> ProcSubTypes = new ArrayList<PINFORecord>();
  		ArrayList<PINFORecord> Frequencies = new ArrayList<PINFORecord>();
  		ArrayList<PINFORecord> Cats = new ArrayList<PINFORecord>();
  		ArrayList<PINFORecord> SubCats = new ArrayList<PINFORecord>();
  		ArrayList<PINFORecord> SubCat2s = new ArrayList<PINFORecord>();
  		ArrayList<PINFORecord> SubCat3s = new ArrayList<PINFORecord>();
  		ArrayList<ProcessTypeRecord> ProcTypes = new ArrayList<ProcessTypeRecord>();
  		ArrayList<UDFSubData> UDFValues = new ArrayList<UDFSubData>();
  		
  		log.info("..mapping JSON string");
  		
  		JsonElement jelement = new JsonParser().parse(masterString);
  	    JsonObject  jobject = jelement.getAsJsonObject();
  	    String lib = jobject.get("lib").toString();
  	    lib = lib.substring(1, lib.length()-1);
  	    String userid = jobject.get("userid").toString();
  	    userid = userid.substring(1, userid.length()-1);
  		
  		String json1 = "";
  		String json2 = "";
  		String json3 = "";
  		String json4 = "";
  		String json5 = "";
  		String json6 = "";
  		String json7 = "";
  		String json8 = "";
  		String jsonAll = "";
    	
  		try {	
  			    		
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
 			log.info("..calling stored procedure");
 			
 			CallableStatement stmt = null;
 		    String query = "CALL SPINFOCR04(?,?)";				
 		    				    			    
 		    try {
	    		stmt = conn.prepareCall(query);	
	    		stmt.setString(1, "7C");
	    		stmt.setString(2, "");
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	ProcSubTypes.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.setString(1, "7D");
	    		stmt.setString(2, "");
		        stmt.execute();	 
		        
		        rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	Cats.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.setString(1, "7E");
	    		stmt.setString(2, "");
		        stmt.execute();	 
		        
		        rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	SubCats.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.setString(1, "7G");
	    		stmt.setString(2, "");
		        stmt.execute();	 
		        
		        rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	SubCat2s.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.setString(1, "7H");
	    		stmt.setString(2, "");
		        stmt.execute();	 
		        
		        rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	SubCat3s.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.setString(1, "T7");
	    		stmt.setString(2, "");
		        stmt.execute();	 
		        
		        rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	Frequencies.add(pinfoRecord);
		        }	        
		        rs.close();
		        
		        stmt.close();
 			        
 		    } catch (SQLException e ) {
 		    	log.warn(e);
 		    	return "Error";
 		    } finally {
 		        if (stmt != null) { stmt.close(); }
 		    }			
  			
  			log.info("..retrieved PINFO Records");
  			
  	  		json1 = new Gson().toJson(ProcSubTypes);
  	  		json2 = new Gson().toJson(Cats);
  	  		json3 = new Gson().toJson(SubCats);
  	  		json4 = new Gson().toJson(SubCat2s);
  	  		json5 = new Gson().toJson(SubCat3s);
  	  		json6 = new Gson().toJson(Frequencies);
  	  		
  	  		stmt = null;
  	  		query = "CALL SPMPTCR02(?,?,?)";				
	    				    			    
  	  		try {
  	  			stmt = conn.prepareCall(query);
  	  			stmt.setString(1, "");
  	  			stmt.setString(2, "N");
  	  			stmt.setString(3, "B");
  	  			stmt.execute();	            			        
        
  	  			ResultSet rs = stmt.getResultSet();
  	  			while (rs.next()) {
  	  				ProcessTypeRecord ProcessTypeRecord = new ProcessTypeRecord();
	        	
  	  				ProcessTypeRecord.TKTYPE = rs.getString("TKTYPE");	
  	  				ProcessTypeRecord.TKTMPNM = rs.getString("TKTMPNM");      	
  	  				
  	  				ProcTypes.add(ProcessTypeRecord);
  	  			}
	        
  	  			rs.close();
  	  			stmt.close();
		        
  	  		} catch (SQLException e ) {
  	  			log.warn(e);
  	  			return "Error";
  	  		} finally {
  	  			if (stmt != null) { stmt.close(); }
  	  		}			
		
  	  		log.info("..retrieved process type records");
 			
 	  		json7 = new Gson().toJson(ProcTypes);
 	  		
 	  		stmt = null;
  	  		query = "CALL SPUDFSCR00()";				
	    				    			    
  	  		try {
  	  			stmt = conn.prepareCall(query);
  	  			stmt.execute();	            			        
        
  	  			ResultSet rs = stmt.getResultSet();
  	  			while (rs.next()) {
  	  				UDFSubData tmpRecord = new UDFSubData();
	        	
  	  				tmpRecord.DPSEQ = rs.getInt(1);	
  	  				tmpRecord.DSSEQ = rs.getInt(2); 
  	  				tmpRecord.DSVAL = rs.getString(3).trim();
  	  				
  	  				UDFValues.add(tmpRecord);
  	  			}
	        
  	  			rs.close();
  	  			stmt.close();
		        
  	  		} catch (SQLException e ) {
  	  			log.warn(e);
  	  			return "Error";
  	  		} finally {
  	  			if (stmt != null) { stmt.close(); }
  	  		}			
		
  	  		log.info("..retrieved UDF DropDown records");
 			
 	  		json8 = new Gson().toJson(UDFValues);
 		} catch(Exception ex) {
 			log.warn(ex);
 			connectionHelper.closeConnection(conn);
 			return "Error";
 		}
    	
    	connectionHelper.closeConnection(conn);	
  		
    	jsonAll = "["+json1+","+json2+","+json3+","+json4+","+json5+","+json6+","+json7+","+json8+"]";

  		return jsonAll;
     }
}  
