package com.cobrapm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Encrypt;
import com.cobrapm.forms.PFRMEXPORT;
import com.cobrapm.forms.PFRMRESP;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileInputStream;
   
@Path("ExportService")  
public class ExportService { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(ExportService.class.getName());
	
     @SuppressWarnings("null")
	 @GET  
     @Path("/export/{lib}/{formid}/{formname}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String export(@PathParam("lib") String lib, @PathParam("formid") String formid, @PathParam("formname") String formname, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
 	
    	ArrayList<PFRMRESP> PFRMRESPs = new ArrayList<PFRMRESP>();
    	ArrayList<PFRMEXPORT> PFRMEXPORTs = new ArrayList<PFRMEXPORT>();
    	
    	ArrayList<String> filenames = new ArrayList<String>();
    	
//    	Encrypt encrypt = new Encrypt();
 //   	lib = encrypt.decryptLibrary(lib);
    	
    	String fullpath = "";
    	String filename = "";
  		try
		{									
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SFRMRECR00(?)";					    				    			    		    
									
			try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMRESP PFRMRESP = new PFRMRESP();
		        	PFRMRESP.FSSTAMP = rs.getString(1).trim();	
		        	PFRMRESP.FESEQ = rs.getInt(2);
		        	PFRMRESP.FEDATA = rs.getString(3).trim();
		        	PFRMRESPs.add(PFRMRESP);
		        }
		        
		        rs.close();
		        stmt.close();
			                		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
			log.info("..retrieved responses.");	

			stmt = null;
		    query = "CALL SFRMEXCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMEXPORT PFRMEXPORT = new PFRMEXPORT();
		        	PFRMEXPORT.FEID = rs.getInt(1);	
		        	PFRMEXPORT.FESEQ = rs.getInt(2);
		        	PFRMEXPORT.FEROW = rs.getInt(3);
		        	PFRMEXPORT.FECOL = rs.getInt(4);
		        	PFRMEXPORTs.add(PFRMEXPORT);
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			    	   
				 		
			try{	
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				//get current date time with Date()
				Date date = new Date();
				   
				fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
				fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));	
				filename = formname + " " + dateFormat.format(date);
				fullpath = fullpath + File.separator + "downloads" + File.separator + filename;	
				
				new File(fullpath).mkdir();
				
				String filepath = "";
				
				XSSFWorkbook wb = null;
				FileInputStream fis = null;
		        XSSFSheet sh = null;                  
		        
		        String templateFilename = retrieveTemplateFromIFS(lib, formid);
		        
		        for(int i = 0; i < PFRMRESPs.size(); i++){		        	
		        	if(i == 0){
		        		String tempTS = PFRMRESPs.get(i).FSSTAMP.replace(".", "");
		        		tempTS = PFRMRESPs.get(i).FSSTAMP.replace(":", "");
		        		filepath = fullpath + File.separator + formname + " " + tempTS + ".xlsx";			        		
		        		filenames.add(filepath);	
		        		
			        	System.out.println("creating first workbook");

			        	if(templateFilename.equals("")){
			        		wb = new XSSFWorkbook(); 
			        		sh = wb.createSheet(formname); 
			        	} else {
			        		fis = new FileInputStream(templateFilename);
				            wb = new XSSFWorkbook(fis);
				        	sh = wb.getSheetAt(0); 
			        	}
			        	
		        	}
		        	
		        	if(i > 0 && !PFRMRESPs.get(i).FSSTAMP.equals(PFRMRESPs.get(i - 1).FSSTAMP)){
				        finalizeWorkbook(filepath, PFRMEXPORTs, sh, wb, PFRMRESPs.get(i - 1).FSSTAMP);
		        		
				        String tempTS = PFRMRESPs.get(i).FSSTAMP.replace(".", "");
				        tempTS = PFRMRESPs.get(i).FSSTAMP.replace(":", "");
		        		filepath = fullpath + File.separator + formname + " " + tempTS + ".xlsx";	
		        		filenames.add(filepath);	
		        		
			        	System.out.println("creating workbook");
			        	
			        	if(templateFilename.equals("")){
			        		wb = new XSSFWorkbook(); 
			        		sh = wb.createSheet(formname); 
			        	} else {
			        		fis.close();  
			        		fis = new FileInputStream(templateFilename);
				            wb = new XSSFWorkbook(fis);
				        	sh = wb.getSheetAt(0); 
			        	} 			        				        	
		        	}
		        	
		        	PFRMEXPORT PFRMEXPORT = PFRMEXPORTs.get(PFRMRESPs.get(i).FESEQ - 1);		        	
		        	XSSFRow row = null;
		        	
		        	System.out.println("sheet name = " + sh.getSheetName());
		        	
		        	if (sh.getRow(PFRMEXPORT.FEROW - 1) == null){
		        		row = sh.createRow(PFRMEXPORT.FEROW - 1);		        		
		        	} else {
		        		row = sh.getRow(PFRMEXPORT.FEROW - 1);
		        		System.out.println("existing row");
		        	}
		        	
		        	System.out.println("row: " + (PFRMEXPORT.FEROW - 1) + " column: " + (PFRMEXPORT.FECOL - 1) + " data: " + PFRMRESPs.get(i).FEDATA);
		        	
		        	XSSFCell cell = null;
		        	if (row.getCell(PFRMEXPORT.FECOL - 1) == null){
		        		cell = row.createCell(PFRMEXPORT.FECOL - 1);
		        	} else {
		        		cell = row.getCell(PFRMEXPORT.FECOL - 1);
		        		System.out.println("existing cell");
		        	}
		        	
			        cell.setCellValue(PFRMRESPs.get(i).FEDATA);
		        }
		        
		        finalizeWorkbook(filepath, PFRMEXPORTs, sh, wb, PFRMRESPs.get(PFRMRESPs.size() - 1).FSSTAMP);
		        if(!templateFilename.equals("")){
		        	fis.close();
		        	File filetodelete = new File(templateFilename);
		        	filetodelete.delete();
	        	}
	        				        
			} catch (Exception e){
				System.out.println(e.getMessage());			
			}
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		connectionHelper.closeConnection(conn);	
  		
  		String directoryZipped = fullpath + ".zip";
  		writeZipFile(directoryZipped, fullpath);
  		  		
  		return "../downloads/" + filename + ".zip";
      } 
       		
     public void finalizeWorkbook(String filepath, ArrayList<PFRMEXPORT> PFRMEXPORTs, XSSFSheet sh, XSSFWorkbook wb, String timestamp) throws IOException{
    	 try {	
    		 
    	 	PFRMEXPORT PFRMEXPORT = PFRMEXPORTs.get(PFRMEXPORTs.size() - 1);
	 		XSSFRow row = null;			        	
	     	if (sh.getRow(PFRMEXPORT.FEROW) == null){
	     		row = sh.createRow(PFRMEXPORT.FEROW - 1);	   
	     	} else {	     		
	     		row = sh.getRow(PFRMEXPORT.FEROW - 1);	   
	     	}
	     	   	
	     	XSSFCell cell = row.createCell(PFRMEXPORT.FECOL - 1);
	        cell.setCellValue(timestamp);
	        
	        FileOutputStream out = new FileOutputStream(filepath);			
	        wb.write(out);
	        out.close();

    	 } catch (FileNotFoundException e) {
    		System.out.println(e.getMessage());		
		}     
     }
     
 	public static void writeZipFile(String zipFile, String srcDir) {		
		try {
			
			// create byte buffer
			byte[] buffer = new byte[1024];

			FileOutputStream fos = new FileOutputStream(zipFile);

			ZipOutputStream zos = new ZipOutputStream(fos);

			File dir = new File(srcDir);

			File[] files = dir.listFiles();

			for (int i = 0; i < files.length; i++) {
				
				System.out.println("Adding file: " + files[i].getName());

				FileInputStream fis = new FileInputStream(files[i]);

				// begin writing a new ZIP entry, positions the stream to the start of the entry data
				zos.putNextEntry(new ZipEntry(files[i].getName()));
				
				int length;

				while ((length = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}

				zos.closeEntry();

				// close the InputStream
				fis.close();
			}

			// close the ZipOutputStream
			zos.close();	
			dir.delete();		
			
		}
		catch (IOException ioe) {
			System.out.println("Error creating zip file" + ioe);
		}
 	}
 	
 	public String retrieveTemplateFromIFS(String lib, String formid){
 		XSSFWorkbook xfwb = null;
 		SXSSFWorkbook wb = null;
 		
 		System.out.println("getting file from IFS");
 		
 		Encrypt encrypt = new Encrypt();
    	lib = encrypt.decryptLibrary(lib);
 		
 		// Create an AS400 object.  This new directory will 
	    // be created on this system. 		
		Context initialContext;
		String dbhost = "";  					  
		String userid = "";  
		String password = "";  
		try {
			initialContext = (Context)new InitialContext().lookup("java:comp/env");		
		    dbhost = (String)initialContext.lookup("dbhost");  					  
			userid = (String)initialContext.lookup("dbuser");  
			password = (String)initialContext.lookup("dbpassword"); 
		} catch (NamingException e) {
			System.out.println(e.getMessage());
		}
		  
	    AS400 sys = new AS400(dbhost, userid, password);
	    
		// /$Library/Forms/$Formid/$thisresponsenumber
	    String directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid + IFSFile.separator + "Export";  
	   
	    IFSFile dir = new IFSFile(sys, directorypath);
	    IFSFile[] directoryListing = null;
		try {
			directoryListing = dir.listFiles();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	    
	    System.out.println(directoryListing.length);
	    
	    String temppath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
	    temppath = temppath.substring(0, temppath.indexOf("WEB-INF"));		        		
	    temppath = temppath + File.separator + "temp" ;

	    for (IFSFile child : directoryListing) {
	        // Create a file object that represents the directory.
	    	String filename = child.getName(); 
	    	System.out.println(filename);
	    	temppath = temppath + File.separator + filename;
	    	try {	  
				IFSFileInputStream source = new IFSFileInputStream(child);
				byte[] buffer = new byte[(int) child.length()];	
				source.read(buffer);
				FileOutputStream fos = new FileOutputStream(temppath);
				fos.write(buffer);
				fos.close();
				return temppath;
			} catch (AS400SecurityException | IOException e) {
				System.out.println("error message " + e.getMessage());
			}	       
	    }
	    
		return "";
     
 	}  
}  
