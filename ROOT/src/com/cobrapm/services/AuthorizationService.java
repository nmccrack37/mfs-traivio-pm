package com.cobrapm.services;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.cobrapm.authentication.AuthenticationHelper;
import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Constants;
import com.cobrapm.authentication.Encrypt;
import com.cobrapm.authentication.ModuleTaskRecord;
import com.cobrapm.authentication.SecurityRecord;
import com.cobrapm.authentication.UserRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("AuthorizationService")
public class AuthorizationService {
	
	ModuleTaskRecord CURRENT_APPLICATION = new ModuleTaskRecord();

	static Properties props;
	static Properties resourceBundle;
	static Connection conn = null;
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private AuthenticationHelper authenticate = new AuthenticationHelper();
//	private Utilities utilities = new Utilities();
	private Constants constants = new Constants();
	private static Logger log = Logger.getLogger(AuthorizationService.class.getName());
	
	@GET
	@Path("/authenticateUser/{userID}/{password}/{isRemember}")
	@Produces(MediaType.APPLICATION_JSON)
	public String authenticateUser(@PathParam("userID")String userID, @PathParam("password") String password, @PathParam("isRemember") String isRemember){
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		CURRENT_APPLICATION.MODULEID = 90000;
		CURRENT_APPLICATION.TASKID = 90000;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		UserRecord userRecord = new UserRecord();
		Encrypt encrypt = new Encrypt();
		
		String json = "";
		String lib = "PMCOMMON";
		org.codehaus.jackson.map.ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		/* 
			This service authenticates using these six steps:
			
			1) Validate the os400 userid and password.
			2) Get user datasource using default datasource.  It must exist.
			3) Test if Strategy is available, or is it locked for EOD, maintenance, etc.
			4) Check if user is found and active, get group id.
			5) Check if user has rights to the application.
			6) Get session ID and token.
		 
		 */ 
		
		// 1. Authenticate user on system
		
	    try
	    	{
	    		    	
		    	resourceBundle = new Properties();
//		    	resourceBundle.load(new FileInputStream("properties/application.properties"));		
		        
		    	String appPath = "";
		        
	        	// get and store the full system path	            
	            appPath = System.getProperty("user.dir");
	    		
	            userRecord.fullFileSystemPath = appPath;  	                     		       		

		        log.info("...Full fs path: " + appPath);
		        
		        log.info("...Application name: Strategy Security");
		        
		        log.info("...Application version: " + constants.APPLICATION_VERSION);
		
		        log.info("...Authenticating user <" + userID + ">");
		        
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			Encrypt PasswordEncryption = new Encrypt();
				String encryptedPassword = PasswordEncryption.PasswordEncryption(password);
				userID = userID.toLowerCase();
		
//		        AS400Authentication as400 = new AS400Authentication();
		
//		        String validationMessage = as400.authenticate(userID, password);
		        
				CallableStatement stmt = null;
			    String query = "CALL SPMUSECR00(?,?)";
			    
			    try {
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			        stmt.setString(1, userID);
			        stmt.setString(2, encryptedPassword);
	
			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	userRecord.userid = rs.getString(1).trim();
			        	userRecord.firstname = rs.getString(2).trim();
			        	userRecord.lastname = rs.getString(3).trim();
			        	userRecord.datasource = rs.getString(6).trim();	
			        }
			        
			        rs.close();
			        stmt.close();
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
		        
		        if(userRecord.datasource == null)
		        	{
			            authorizationRecord.returnMessage = "Incorrect Username or Password.";
			            log.info("...User <" + userID + "> failed authentication.");
			            log.info("...Actual as400 security message");
			            authorizationRecord.isError = true;
				  		json = ow.writeValueAsString(authorizationRecord);
			            return(json);
		        	}
		        else
		        	{
			            userRecord.applicationVersion = constants.APPLICATION_VERSION;
			            
			            stmt = null;
					    query = "CALL SQINITRR05(?,?,?,?)"; 
					    
					    String JOB_c = "";
					    String USER_c = "";
					    String NBR_c = "";
					    try {
					        stmt = conn.prepareCall(query);
					        stmt.registerOutParameter(2, Types.VARCHAR);
					        stmt.registerOutParameter(3, Types.VARCHAR);
					        stmt.registerOutParameter(4, Types.VARCHAR);
					        stmt.setString(1, userRecord.datasource);	  
					        stmt.setString(2, JOB_c);	 
					        stmt.setString(3, USER_c);	 
					        stmt.setString(4, NBR_c);	 
					        stmt.execute();	
					        
					        JOB_c = stmt.getString(2);
					        USER_c = stmt.getString(3);
					        NBR_c = stmt.getString(4);
					        
					        stmt.close();
					    } catch (SQLException e ) {
					    	log.warn(e);
					    } finally {
					        if (stmt != null) { stmt.close(); }
					    }  
					    
			            log.info("...User <" + userID + "> authenticated.");
		        	}
	        
	    	}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
	    
	    // 3. Is Strategy available?
		
		try
			{
			log.info("...Testing if Strategy is available");

	        // get the system our user's datasource from pconection connected to...
	        String datasourceConnectedSystem = "";
	        
	        CallableStatement stmt = null;
		    String query = "CALL STATUSINFO(?)";	
		    
		    try {
			        stmt = conn.prepareCall(query);
			        stmt.registerOutParameter(1, java.sql.Types.CHAR);
			        stmt.execute();	
			        datasourceConnectedSystem = stmt.getString(1);
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
	        
	        userRecord.datasourceConnectedSystem = datasourceConnectedSystem; 
	        
	        log.info("...Datasource connection is connected to system: " + datasourceConnectedSystem);
	        	                    
	        String strategyAvailableYN = "";
	        
	        stmt = null;
		    query = "CALL SPLOCKRR03(?)";	
		    
		    try {
			        stmt = conn.prepareCall(query);
			        stmt.setString(1, strategyAvailableYN);
			        stmt.registerOutParameter(1, java.sql.Types.CHAR);
			        
			        stmt.execute();	
			        
			        strategyAvailableYN = stmt.getString(1);	        
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
	
	        log.info("...strategyAvailable: " + strategyAvailableYN);
	
	        if(!strategyAvailableYN.equals("Y"))
	        {
	            connectionHelper.closeConnection(conn);
	        	authorizationRecord.returnMessage = "EOD is running (Strategy is locked)";
	        	authorizationRecord.isError = true;
	        	json = ow.writeValueAsString(authorizationRecord);
			    return(json);
	        }
	        
	        userRecord.datasource = encrypt.encryptLibrary(userRecord.datasource);

		}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "Authentication failed. An error occured testing if Strategy is available.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
		
	    
	    // 5) Check if user has rights to the application.
	    
	    try{
	
	        log.info("...Retrieve ALL task rights for user " + userID);
				
	        CallableStatement stmt = null;
		    String query = "CALL SPSECURR00(?)";	
		    
		    try {
		        stmt = conn.prepareCall(query);
		        stmt.setString(1, userID);
		        stmt.execute();	
		        
		        ResultSet rs = stmt.getResultSet();
				        
		        
		        SecurityRecord resultTaskRight = new SecurityRecord();
		        
		        while (rs.next()) {		        		 
//			    	SecurityRecord resultTaskRight = new SecurityRecord();
				    switch(rs.getString(1).trim()){
				    	case "ADM" :
				    		resultTaskRight.ADM = rs.getString(2);
				    		break;
				    	case "PROC" :
				    		resultTaskRight.PROC = rs.getString(2);
				    		break;
				    	case "PNP" :
				    		resultTaskRight.PNP = rs.getString(2);
				    		break;
				    	case "PNT" :
				    		resultTaskRight.PNT = rs.getString(2);
				    		break;
				    	case "PPTM" :
				    		resultTaskRight.PPTM = rs.getString(2);
				    		break;
				    	case "PTTM" :
				    		resultTaskRight.PTTM = rs.getString(2);
				    		break;
				    	case "PSI" :
				    		resultTaskRight.PSI = rs.getString(2);
				    		break;
				    	case "PPC" :
				    		resultTaskRight.PPC = rs.getString(2);
				    		break;
				    	case "PEPM" :
				    		resultTaskRight.PEPM = rs.getString(2);
				    		break;
				    	case "PREA" :
				    		resultTaskRight.PREA = rs.getString(2);
				    		break;
				    	case "NOTE" :
				    		resultTaskRight.NOTE = rs.getString(2);
				    		break;
				    	case "NOTM" :
				    		resultTaskRight.NOTM = rs.getString(2);
				    		break;
				    	case "FORM" :
				    		resultTaskRight.FORM = rs.getString(2);
				    		break;
				    	case "FRMM" :
				    		resultTaskRight.FRMM = rs.getString(2);
				    		break;
				    }          
				} 
		        log.info(resultTaskRight.NOTE + ", " + resultTaskRight.FORM);
//		        while (rs.next()) {		        		 
//		        	SecurityRecord resultTaskRight = new SecurityRecord();	
//			            	
//			        resultTaskRight.Module = rs.getString(1); 		// Module ID
//			        resultTaskRight.RightValue = rs.getString(2);			// Task ID  
//			                
//			        log.info("....Module:User Right's " +  resultTaskRight.Module + ":" +  resultTaskRight.RightValue + " ");			           
//		            // append this record to the return result set				
//			        resultTaskRights.add(resultTaskRight);
//			        log.info("....Task record added");            
//			    }   
		        
		        userRecord.taskRights = resultTaskRight;
		        
		        rs.close();
		        
			} catch (SQLException e ) {
			   	log.warn(e);
			} finally {
				if (stmt != null) { stmt.close(); 
			}
		}		    
				
	    }
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "Authentication failed. An error occured getting user rights to the application.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());					
			
			authorizationRecord.isError = true;
		    return(json);
	    }
		
		// 6. Get session ID and token
		
		try
		{
			log.info("...Getting session ID");
	        // generate a token to identify this session
	        log.info("...Generating token.");
	        double myRandom;
	        myRandom = Math.random();
	        int token = (int) Math.round(myRandom * 1000000000);
	        				
			String sessionID = "";
			String loginSuccessfulYN = "";
	       	        
			CallableStatement stmt = null;
		    String query = "CALL SPSESSRU00(?,?,?,?)";	
		    
		    try {
			        stmt = conn.prepareCall(query);
			        stmt.setString(1, userRecord.userid);
			        stmt.setInt(2, token);
			        stmt.registerOutParameter(3, java.sql.Types.INTEGER);
			        stmt.registerOutParameter(4, java.sql.Types.CHAR);
			        
			        stmt.execute();	
			        
			        sessionID = stmt.getString(3);
			        loginSuccessfulYN = stmt.getString(4);		        
				        
			} catch (SQLException e ) {
			   	log.warn(e);
			} finally {
			    if (stmt != null) { stmt.close(); }
			}
	        log.info("...sessionID: "+ sessionID);
	        if(Integer.parseInt(sessionID) > 0){
	            log.info("...SessionID: " + sessionID);
	            log.info("...Creating user record.");
	            
	            userRecord.sessionID = Integer.parseInt(sessionID);
	            userRecord.sessionToken = token;
	            
	            log.info("...User: <" + userRecord.userid + "> session: <" + userRecord.sessionID + ">");
	            
	            log.info("...Retrieving session timeout value property.");
	            String timeout = "120";
	            log.info("...timeout in minutes: " + timeout);
	            userRecord.sessionTimeout = Integer.parseInt(timeout);
	        }
	        else
	        {
	            log.info("...Could not get session ID.");
	            connectionHelper.closeConnection(conn);
	        	authorizationRecord.returnMessage = "Authentication failed.  Could not get session ID.";
	        	authorizationRecord.isError = true;
	        	json = ow.writeValueAsString(authorizationRecord);
			    return(json);
	        }
		}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "Authentication failed. An error occured getting user session ID.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());					
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
		
		try{
  			
			log.info("...retrieving job function");
						
			CallableStatement stmt = null;
		    String query = "CALL SROLEJCR01(?)";	
		    
		    try {
		        stmt = conn.prepareCall(query);
		        stmt.setString(1, userRecord.userid);
		        stmt.execute();	
		        
		        ResultSet rs = stmt.getResultSet();
			
		        while(rs.next()){
		        	userRecord.jobfunction = rs.getString(1);
					userRecord.jobfunctiondescription =  rs.getString(2);
					userRecord.queue =  rs.getString(3);
		        }
	
		    } catch (SQLException e ) {
			   	log.warn(e);
			} finally {
			    if (stmt != null) { stmt.close(); }
			}	
		    
		    if(userRecord.jobfunction.equals("")){
		    	log.info("...no job function record present for user: " + userRecord.userid);
		    	connectionHelper.closeConnection(conn);
	        	authorizationRecord.returnMessage = "Authentication failed.  No job function exists for this user.";
	        	authorizationRecord.isError = true;
			    return(json);
		    }
		    
		    log.info("...should we Remember? " + isRemember);
			if(isRemember.equals("true")){
				stmt = null;
				query = "CALL SPTRACRU00(?,?,?,?)";
		    
				UUID uuid = UUID.randomUUID();
				String randomUUIDString = uuid.toString();
				
				String libd = encrypt.decryptLibrary(userRecord.datasource);
		    
				try {
					stmt = conn.prepareCall(query);
					stmt.setString(1, randomUUIDString);
					stmt.setInt(2, userRecord.sessionToken);
					stmt.setString(3, libd);
					stmt.setString(4, "I");
					stmt.execute();	
	
				} catch (SQLException e ) {
					log.warn(e);
				} finally {
					if (stmt != null) { stmt.close(); }
				}
				userRecord.sessionTrack = randomUUIDString;
			}
     	                      
		    connectionHelper.closeConnection(conn);                   
		}
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "Authentication failed. An error occured getting user session ID.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());				
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
		
		authorizationRecord.userRecord = userRecord;
		try {
			json = ow.writeValueAsString(authorizationRecord);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	    }
	
	@POST
	@Path("/retrieveLogin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String retrieveLogin(String sessionToken){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
//		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		UserRecord userRecord = new UserRecord();
		
		log.info("..mapping JSON string");
		log.info("..."+sessionToken);
		JsonElement jelement = new JsonParser().parse(sessionToken);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String sessionTrack = jobject.get("sessionToken").toString();
	    sessionTrack = sessionTrack.substring(1, sessionTrack.length()-1);
		
		String json = "";
		String lib = "PMCOMMON";
		org.codehaus.jackson.map.ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
// 1. Authenticate user on system
		
	    try
	    	{
	    		    	
		    	resourceBundle = new Properties();
//		    	resourceBundle.load(new FileInputStream("properties/application.properties"));		
		        
		    	String appPath = "";
		        
	        	// get and store the full system path	            
	            appPath = System.getProperty("user.dir");
	    		
	            userRecord.fullFileSystemPath = appPath;  	                     		       		

		        log.info("...Full fs path: " + appPath);
		        
		        log.info("...Application name: Strategy Security");
		        
		        log.info("...Application version: " + constants.APPLICATION_VERSION);
		
//		        log.info("...Authenticating user <" + userID + ">");
		        
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
				CallableStatement stmt = null;
			    String query = "CALL SPMUSECR01(?)";
			    
			    Encrypt encrypt = new Encrypt();
			    
			    try {
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			        stmt.setString(1, sessionTrack);
	
			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	userRecord.sessionID = rs.getInt(1);
			        	userRecord.sessionToken = rs.getInt(2);
			        	userRecord.userid = rs.getString(3).trim();
			        	userRecord.firstname = rs.getString(4).trim();
			        	userRecord.lastname = rs.getString(5).trim();
			        	userRecord.datasource = rs.getString(6).trim();	
			        }
			        userRecord.jobfunctiondescription = userRecord.firstname + " " + userRecord.lastname;
			        rs.close();
			        stmt.close();
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    	String error = "Error";
			    	return error;
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
		        
			    userRecord.sessionTimeout = 120;
			    
		        if(userRecord.datasource == null)
		        	{
//			            authorizationRecord.returnMessage = "Incorrect Username or Password.";
			            log.info("...User failed authentication.");
			            log.info("...Actual as400 security message");
//			            authorizationRecord.isError = true;
				  		json = ow.writeValueAsString(userRecord);
			            return(json);
		        	}
		        userRecord.datasource = encrypt.encryptLibrary(userRecord.datasource);
		        
		        // 5) Check if user has rights to the application.
			    
		        log.info("...Retrieve ALL task rights for user " + userRecord.userid);
						
		        stmt = null;
		        query = "CALL SPSECURR00(?)";	
				    
		        try {
		        	stmt = conn.prepareCall(query);
				    stmt.setString(1, userRecord.userid);
				    stmt.execute();	
				        
				    ResultSet rs = stmt.getResultSet();
						        
				        
				    SecurityRecord resultTaskRight = new SecurityRecord();	
				        
				    while (rs.next()) {		        		 
//				    	SecurityRecord resultTaskRight = new SecurityRecord();
					    switch(rs.getString(1).trim()){
					    	case "ADM" :
					    		resultTaskRight.ADM = rs.getString(2);
					    		break;
					    	case "PROC" :
					    		resultTaskRight.PROC = rs.getString(2);
					    		break;
					    	case "PNP" :
					    		resultTaskRight.PNP = rs.getString(2);
					    		break;
					    	case "PNT" :
					    		resultTaskRight.PNT = rs.getString(2);
					    		break;
					    	case "PPTM" :
					    		resultTaskRight.PPTM = rs.getString(2);
					    		break;
					    	case "PTTM" :
					    		resultTaskRight.PTTM = rs.getString(2);
					    		break;
					    	case "PSI" :
					    		resultTaskRight.PSI = rs.getString(2);
					    		break;
					    	case "PPC" :
					    		resultTaskRight.PPC = rs.getString(2);
					    		break;
					    	case "PEPM" :
					    		resultTaskRight.PEPM = rs.getString(2);
					    		break;
					    	case "PREA" :
					    		resultTaskRight.PREA = rs.getString(2);
					    		break;
					    	case "NOTE" :
					    		resultTaskRight.NOTE = rs.getString(2);
					    		break;
					    	case "NOTM" :
					    		resultTaskRight.NOTM = rs.getString(2);
					    		break;
					    	case "FORM" :
					    		resultTaskRight.FORM = rs.getString(2);
					    		break;
					    	case "FRMM" :
					    		resultTaskRight.FRMM = rs.getString(2);
					    		break;
					    }          
					}   
				    log.info("....Task records added");  
				    userRecord.taskRights = resultTaskRight;
				        
				    rs.close();
				        
		        } catch (SQLException e ) {
					log.warn(e);
				} finally {
					if (stmt != null) { stmt.close(); 
				}
			}
	    }
	    catch(Exception ex)
	    {
//	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
//	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
//			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
//			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
		    return(json);
	    }
	    
//	    authorizationRecord.userRecord = userRecord;
		try {
			json = ow.writeValueAsString(userRecord);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	
	@POST
	@Path("/deleteLogin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteLogin(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		UserRecord userRecord = new UserRecord();
		Encrypt encrypt = new Encrypt();
		
		log.info("..mapping JSON string");
		log.info("..."+ masterString);
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    userRecord.sessionTrack = jobject.get("sessionTrack").toString();
	    userRecord.sessionTrack = userRecord.sessionTrack.substring(1, userRecord.sessionTrack.length()-1);
	    userRecord.sessionToken = jobject.get("sessionToken").getAsInt();
	    userRecord.datasource = jobject.get("datasource").toString();
	    userRecord.datasource = userRecord.datasource.substring(1, userRecord.datasource.length()-1);
	    userRecord.datasource = encrypt.decryptLibrary(userRecord.datasource);
	    
		String json = "";
		String lib = "PMCOMMON";
		
// 1. Authenticate user on system
		
	    try
	    	{
	    		    	
		    	resourceBundle = new Properties();
		        
		    	String appPath = "";
		        
	        	// get and store the full system path	            
	            appPath = System.getProperty("user.dir");
	    		
	            userRecord.fullFileSystemPath = appPath;  	                     		       		

		        log.info("...Full fs path: " + appPath);
		        
		        log.info("...Application name: Strategy Security");
		        
		        log.info("...Application version: " + constants.APPLICATION_VERSION);
		
//		        log.info("...Authenticating user <" + userID + ">");
		        
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
		        
				CallableStatement stmt = null;
			    String query = "CALL SPTRACRU00(?,?,?,?)";
			    
			    try {
					stmt = conn.prepareCall(query);
					stmt.setString(1, userRecord.sessionTrack);
					stmt.setInt(2, userRecord.sessionToken);
					stmt.setString(3, userRecord.datasource);
					stmt.setString(4, "D");
					stmt.execute();	
	
				} catch (SQLException e ) {
					log.warn(e);
				} finally {
					if (stmt != null) { stmt.close(); }
				}
	        
	    	}
	    catch(Exception ex)
	    {
			log.error(ex.getMessage());			
			
			json = "An error occured during the authenticate process.";
		    return(json);
	    }

		return json;
	}

@GET
@Path("/changeUserPassword/{userID}/{oldPass}/{newPass}")
@Produces(MediaType.APPLICATION_JSON)
public String changeUserPassword(@PathParam("userID")String userID, @PathParam("oldPass") String oldPass, @PathParam("newPass") String newPass) throws SQLException{
	CONNECTOR CONNECTOR = new CONNECTOR(); 
	Connection conn = null;
	
	CURRENT_APPLICATION.MODULEID = 90000;
	CURRENT_APPLICATION.TASKID = 90000;
	
	AuthorizationRecord authorizationRecord = new AuthorizationRecord();
	UserRecord userRecord = new UserRecord();
	
	String json = "";
	String lib = "PMCOMMON";
	org.codehaus.jackson.map.ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	Encrypt PasswordEncryption = new Encrypt();
	String encryptedPasswordOld = PasswordEncryption.PasswordEncryption(oldPass);
	String encryptedPasswordNew = PasswordEncryption.PasswordEncryption(newPass);
	userID = userID.toLowerCase();
	
	// 1. Authenticate user on system
	
    try
    	{
    		    	
	    	resourceBundle = new Properties();
//	    	resourceBundle.load(new FileInputStream("properties/application.properties"));		
	        
	    	String appPath = "";
	        
        	// get and store the full system path	            
            appPath = System.getProperty("user.dir");
    		
            userRecord.fullFileSystemPath = appPath;  	                     		       		
	        
	        CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
	        
			CallableStatement stmt = null;
		    String query = "CALL SPMUSECR00(?,?)";
		    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, userID);
		        stmt.setString(2, encryptedPasswordOld);

		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	userRecord.userid = rs.getString(1).trim();
		        	userRecord.firstname = rs.getString(2).trim();
		        	userRecord.lastname = rs.getString(3).trim();
		        	userRecord.datasource = rs.getString(6).trim();	
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
	        
	        if(userRecord.datasource == null)
	        	{
		            authorizationRecord.returnMessage = "An Error Occurred. Please validate password's and try again.";
		            log.info("...User <" + userID + "> failed authentication.");
		            log.info("...Actual as400 security message");
		            authorizationRecord.isError = true;
			  		json = ow.writeValueAsString(authorizationRecord);
		            return(json);
	        	}
	        else
	        	{
		            userRecord.applicationVersion = constants.APPLICATION_VERSION;
		            
		            stmt = null;
				    query = "CALL SQINITRR05(?,?,?,?)"; 
				    
				    String JOB_c = "";
				    String USER_c = "";
				    String NBR_c = "";
				    try {
				        stmt = conn.prepareCall(query);
				        stmt.registerOutParameter(2, Types.VARCHAR);
				        stmt.registerOutParameter(3, Types.VARCHAR);
				        stmt.registerOutParameter(4, Types.VARCHAR);
				        stmt.setString(1, userRecord.datasource);	  
				        stmt.setString(2, JOB_c);	 
				        stmt.setString(3, USER_c);	 
				        stmt.setString(4, NBR_c);	 
				        stmt.execute();	
				        
				        JOB_c = stmt.getString(2);
				        USER_c = stmt.getString(3);
				        NBR_c = stmt.getString(4);
				        
				        stmt.close();
				    } catch (SQLException e ) {
				    	log.warn(e);
				    } finally {
				        if (stmt != null) { stmt.close(); }
				    }  
				    
		            log.info("...User <" + userID + "> authenticated.");
	        	}
        
    	}
    catch(Exception ex)
    {
        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
        authorizationRecord.returnErrorMessage = ex.getMessage();
    	
		log.error(authorizationRecord.returnMessage);
		log.error(ex.getMessage());			
		
		authorizationRecord.isError = true;
		json = "An error occured during the authenticate process.";
	    return(json);
    }
    
	CallableStatement stmt = null;
    String query = "CALL SPMUSERU00(?,?,?)";
    
    try {
        stmt = conn.prepareCall(query);
        stmt.setString(1, userID);
        stmt.setString(2, encryptedPasswordOld);
        stmt.setString(3, encryptedPasswordNew);

        stmt.execute();			        

        stmt.close();
        
    } catch (SQLException e ) {
    	log.warn(e);
    } finally {
        if (stmt != null) { stmt.close(); }
    }
    
	try {
		json = ow.writeValueAsString(authorizationRecord);
	} catch (JsonGenerationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JsonMappingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return json;
    }

	@POST
	@Path("/checkPasswordLink")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String checkPasswordLink(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		
		log.info("..mapping JSON string");
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String token = jobject.get("token").toString();
	    token = token.substring(1, token.length()-1);
	    String email = jobject.get("email").toString();
	    email = email.substring(1, email.length()-1);
	    
	    Encrypt PasswordEncryption = new Encrypt();
	    
	    token = PasswordEncryption.decryptLibrary(token);
	    email = PasswordEncryption.decryptLibrary(email);
	    
	    String json = "";
	    
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection("PMCOMMON");
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP to create Check if the link is still valid");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPPASSCR00(?,?,?)";
			   try {
				  stmt = conn.prepareCall(query);
				  
				  stmt.registerOutParameter(3, Types.VARCHAR);
			      stmt.setString(1, token);
			      stmt.setString(2, email);
			      stmt.setString(3, "");

			      stmt.executeUpdate();			       
			      
			      String errorCheck = stmt.getString(3).trim();
			      log.info("Error Check: " + errorCheck);
			      
			      stmt.close();
			      json = new Gson().toJson(errorCheck);
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			 connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
	
	@POST
	@Path("/updatePassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updatePassword(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		
		log.info("..mapping JSON string");
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String password = jobject.get("password").toString();
	    password = password.substring(1, password.length()-1);
	    String email = jobject.get("email").toString();
	    email = email.substring(1, email.length()-1);
	    
	    Encrypt PasswordEncryption = new Encrypt();
	    
	    password = PasswordEncryption.PasswordEncryption(password);
	    email = PasswordEncryption.decryptLibrary(email);
	    
	    String json = "";
	    
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection("PMCOMMON");
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP to reset User Password");
		        
				CallableStatement stmt = null;
			    String query = "CALL SPPASSRU01(?,?)";
			   try {
				  stmt = conn.prepareCall(query);

			      stmt.setString(1, email);
			      stmt.setString(2, password);

			      stmt.executeUpdate();			       
			      
			      stmt.close();
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			 connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}
}
