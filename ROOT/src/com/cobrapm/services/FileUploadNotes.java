package com.cobrapm.services;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
 
   
@Path("FileUploadNotes")  
public class FileUploadNotes { 
	
    private static final String SUCCESS_RESPONSE = "Successful";
    private static final String FAILED_RESPONSE = "Failed";
	
	private static Logger log = Logger.getLogger(FileUploadNotes.class.getName());
		
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String upload(@Context HttpServletRequest request)
    {
        String responseStatus = SUCCESS_RESPONSE;        
        String candidateName = null;
        
        String FILE_UPLOAD_PATH = "";
        
        String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
		fullpath = fullpath + "uploads" + File.separator + request.getHeader("directory");	
//        String fullpath = "C:\\Workspaces\\Cobra\\Portal (Java)\\NOTES\\WebContent\\WEB-INF";
//        fullpath = fullpath + File.separator + "uploads" + File.separator + request.getHeader("directory");	
		log.info(fullpath);
		
		new File(fullpath).mkdir();
		
		FILE_UPLOAD_PATH = fullpath; 
		
        //checks whether there is a file upload request or not
        if (ServletFileUpload.isMultipartContent(request))
        {
            final FileItemFactory factory = new DiskFileItemFactory();
            final ServletFileUpload fileUpload = new ServletFileUpload(factory);
            try
            {
                /*
                 * parseRequest returns a list of FileItem
                 * but in old (pre-java5) style
                 */
                final List<?> items = fileUpload.parseRequest(request);
                 
                if (items != null)
                {
                    final Iterator<?> iter = items.iterator();
                    while (iter.hasNext())
                    {
                        final FileItem item = (FileItem) iter.next();
                        final String itemName = item.getName();
                        final String fieldName = item.getFieldName(); 
                        final String fieldValue = item.getString();
 
                        if (item.isFormField())
                        {
                            candidateName = fieldValue;
                            log.info("Field Name: " + fieldName + ", Field Value: " + fieldValue);
                            log.info("Candidate Name: " + candidateName);
                        }
                        else
                        {
                                final File savedFile = new File(FILE_UPLOAD_PATH + File.separator
                                         + itemName);
                                log.info("Saving the file: " + savedFile.getName());
                                item.write(savedFile);                                
                        }
 
                    }
                }
            }
            catch (FileUploadException fue)
            {
                responseStatus = FAILED_RESPONSE;
                fue.printStackTrace();                
            }
            catch (Exception e)
            {
                responseStatus = FAILED_RESPONSE;
                e.printStackTrace();
            }
        }        
         
        log.info("Returned Response Status: " + responseStatus);
         
        return responseStatus;
    }
	
	@POST
	@Path("/uploadTemplate")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String uploadTemplate(@Context HttpServletRequest request)
    {
        String responseStatus = SUCCESS_RESPONSE;        
        String candidateName = null;
        
        String FILE_UPLOAD_PATH = "";
        
        String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
		fullpath = fullpath + File.separator + "templates" + File.separator + request.getHeader("directory");	
		
		log.info(fullpath);
		
		new File(fullpath).mkdir();
		
		FILE_UPLOAD_PATH = fullpath; 
		
        //checks whether there is a file upload request or not
        if (ServletFileUpload.isMultipartContent(request))
        {
            final FileItemFactory factory = new DiskFileItemFactory();
            final ServletFileUpload fileUpload = new ServletFileUpload(factory);
            try
            {
                /*
                 * parseRequest returns a list of FileItem
                 * but in old (pre-java5) style
                 */
                final List<?> items = fileUpload.parseRequest(request);
                 
                if (items != null)
                {
                    final Iterator<?> iter = items.iterator();
                    while (iter.hasNext())
                    {
                        final FileItem item = (FileItem) iter.next();
                        final String itemName = item.getName();
                        final String fieldName = item.getFieldName(); 
                        final String fieldValue = item.getString();
 
                        if (item.isFormField())
                        {
                            candidateName = fieldValue;
                            log.info("Field Name: " + fieldName + ", Field Value: " + fieldValue);
                            log.info("Candidate Name: " + candidateName);
                        }
                        else
                        {
                                final File savedFile = new File(FILE_UPLOAD_PATH + File.separator
                                         + itemName);
                                log.info("Saving the file: " + savedFile.getName());
                                item.write(savedFile);                                
                        }
 
                    }
                }
            }
            catch (FileUploadException fue)
            {
                responseStatus = FAILED_RESPONSE;
                fue.printStackTrace();                
            }
            catch (Exception e)
            {
                responseStatus = FAILED_RESPONSE;
                e.printStackTrace();
            }
        }        
         
        log.info("Returned Response Status: " + responseStatus);
         
        return responseStatus;
    }
	
	@GET 
	@Path("/remove/{directory}/{filename}")  
    @Produces(MediaType.APPLICATION_JSON)
	public void removeFile(@PathParam("directory") String directory, @PathParam("filename") String filename) 
    {        
        String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
		fullpath = fullpath + File.separator + "uploads" + File.separator + directory + File.separator + filename;	
		
		log.info(fullpath);
		
		File filetodelete = new File(fullpath);
		
		filetodelete.delete();		
		
    }
}  
