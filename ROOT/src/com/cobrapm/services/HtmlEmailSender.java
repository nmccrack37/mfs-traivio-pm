package com.cobrapm.services;

import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Encrypt;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("HtmlEmailSender")  
public class HtmlEmailSender {
	
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	
	@GET  
    @Path("/sendHtmlEmail/{toAddress}/{fromName}/{toName}/{taskTitle}")  
    @Produces(MediaType.APPLICATION_JSON)
    public String sendHtmlEmail(@PathParam("toAddress") String toAddress, @PathParam("fromName") String fromName, 
    		@PathParam("toName") String toName,  @PathParam("taskTitle") String taskTitle)
   		 throws AddressException, MessagingException { 
		
		Logger log = Logger.getLogger(HtmlEmailSender.class.getName());
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "172.29.20.38");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.auth", "false");
//        properties.put("mail.smtp.starttls.enable", "true");
 
        Session session = Session.getInstance(properties);
 
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        
        log.info("...Sending Email");
        
        String checker = "";
        try {
			msg.setFrom(new InternetAddress("do_not_reply@traiv.io", "Traivio"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject("Outstanding Task: " + taskTitle);
        msg.setSentDate(new Date());
        // set plain text message
        String message = "<html><head><title></title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
        message += "<style type='text/css'>@import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,700,400);</style>";
        message += "<meta name='robots' content='noindex,nofollow'><meta property='og:title' content='Outstanding Task'>";
        message += "</head><body style='margin: 0;padding: 0;min-width: 100%;background-color: #f5f7fa'>";
        message += "<center class='wrapper' style='display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #f5f7fa'>";
        message += "<table class='header centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'><table class='preheader' style='border-collapse: collapse;border-spacing: 0' align='right'>";
        message += "<tbody><tr><td class='emb-logo-padding-box' style='padding: 0;vertical-align: top;padding-bottom: 24px;padding-top: 24px;text-align: right;width: 280px;letter-spacing: 0.01em;line-height: 17px;font-weight: 400;font-size: 11px;color: #b9b9b9'>";
        message += "<div class='spacer' style='font-size: 1px;line-height: 2px;width: 100%'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table style='border-collapse: collapse;border-spacing: 0' align='left'><tbody><tr>";
        message += "<td class='logo emb-logo-padding-box' style='padding: 0;vertical-align: top;mso-line-height-rule: at-least;width: 280px;padding-top: 24px;padding-bottom: 24px'>";
        message += "<div class='logo-left' style='font-weight: 700;font-family: Avenir,sans-serif;color: #555;font-size: 0px !important;line-height: 0 !important' align='left' id='emb-email-header'><img style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 300px' src='https://i1.createsend1.com/ei/i/A8/C87/B49/223903/csfinal/traivio.png' alt='Trav.io Logo' width='200' height='53'></div>";
        message += "</td></tr></tbody></table></td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>​</td></tr></tbody></table>";
        message += "<table class='centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto'><tbody><tr>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td>";
        message += "<td style='padding: 0;vertical-align: top'>";
        message += "<table class='one-col' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #ffffff;table-layout: fixed' emb-background-style=''>";
        message += "<tbody><tr><td class='column' style='padding: 0;vertical-align: top;text-align: left'>";
        message += "<div><div class='column-top' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div></div>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<div style='height:15px'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<h2 style='font-style: normal;font-weight: 700;Margin-bottom: 0;Margin-top: 0;font-size: 24px;line-height: 32px;font-family: &quot;Open Sans&quot;,sans-serif;color: #44a8c7;text-align: center'><span style='color:#fc635e'>";
        message += "We Must Task You A Question</span></h2><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 16px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += toName + ", You have a task waiting for you in Traivio called:<br /> <b>" + taskTitle + "</b>.<br />" + fromName + " has sent you this reminder to get in there and knock it out of the park.</p><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 24px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
//        message += "I'd put a link here, but eh, I'm lazy. You know where to go.</p></td></tr></tbody></table>";
        message += "<div class='column-bottom' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div>";
        message += "</td></tr></tbody></table></td>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>&nbsp;</td></tr></tbody></table></center>";
        message += "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>";
        message += "<script type='text/javascript'>";
        message += "$(document).ready(function () {CS.WebVersion.setup({'LikeActionBase':'/t/i-fb-ttqkut-l-','IsSubscriber':false});});</script>";
        message += "<script type='text/javascript'>$(function(){$('area,a').attr('target', '_blank');});</script>";
        message += "<div id='facebox' style='display:none;'><div class='popup'><div class='content'></div><div id='closeBox'><a href='#' class='close' target='_blank'></a></div></div></div></body></html>";
        
        msg.setContent(message, "text/html");
 
        // sends the e-mail
        Transport.send(msg);
        
        return checker;
    }
	
	@POST  
    @Path("/sendUserEmail")  
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String sendUserEmail(String masterString)
   		 throws AddressException, MessagingException {
		
		
		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String fromName = jobject.get("sender").toString();
 	    fromName = fromName.substring(1, fromName.length()-1);
 	    String toAddress = jobject.get("email").toString();
 	    toAddress = toAddress.substring(1, toAddress.length()-1);
 	    String group = jobject.get("group").toString();
 	    group = group.substring(1, group.length()-1);
 	    String security = jobject.get("security").toString();
 	    security = security.substring(1, security.length()-1);
		
		Logger log = Logger.getLogger(HtmlEmailSender.class.getName());
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "172.29.20.38");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.auth", "false");
//        properties.put("mail.smtp.starttls.enable", "true");
 
        Session session = Session.getInstance(properties);
 
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        
        log.info("...Sending Email");
        
        Encrypt encryptLibrary = new Encrypt();
   	 	String returnStringLib = encryptLibrary.encryptLibrary(lib);
	 	String returnStringGroup = encryptLibrary.encryptLibrary(group);
	 	String returnStringSec = encryptLibrary.encryptLibrary(security);
   	 
        String checker = "";
        try {
			msg.setFrom(new InternetAddress("do_not_reply@traiv.io", "Traivio"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject("Welcome to Traivio!");
        msg.setSentDate(new Date());
        // set plain text message
        String message = "<html><head><title></title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
        message += "<style type='text/css'>@import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,700,400);</style>";
        message += "<meta name='robots' content='noindex,nofollow'><meta property='og:title' content='Welcome to Traivio'>";
        message += "</head><body style='margin: 0;padding: 0;min-width: 100%;background-color: #f5f7fa'>";
        message += "<center class='wrapper' style='display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #f5f7fa'>";
        message += "<table class='header centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'><table class='preheader' style='border-collapse: collapse;border-spacing: 0' align='right'>";
        message += "<tbody><tr><td class='emb-logo-padding-box' style='padding: 0;vertical-align: top;padding-bottom: 24px;padding-top: 24px;text-align: right;width: 280px;letter-spacing: 0.01em;line-height: 17px;font-weight: 400;font-size: 11px;color: #b9b9b9'>";
        message += "<div class='spacer' style='font-size: 1px;line-height: 2px;width: 100%'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table style='border-collapse: collapse;border-spacing: 0' align='left'><tbody><tr>";
        message += "<td class='logo emb-logo-padding-box' style='padding: 0;vertical-align: top;mso-line-height-rule: at-least;width: 280px;padding-top: 24px;padding-bottom: 24px'>";
        message += "<div class='logo-left' style='font-weight: 700;font-family: Avenir,sans-serif;color: #555;font-size: 0px !important;line-height: 0 !important' align='left' id='emb-email-header'><img style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 300px' src='https://i1.createsend1.com/ei/i/A8/C87/B49/223903/csfinal/traivio.png' alt='Trav.io Logo' width='200' height='53'></div>";
        message += "</td></tr></tbody></table></td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>​</td></tr></tbody></table>";
        message += "<table class='centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto'><tbody><tr>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td>";
        message += "<td style='padding: 0;vertical-align: top'>";
        message += "<table class='one-col' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #ffffff;table-layout: fixed' emb-background-style=''>";
        message += "<tbody><tr><td class='column' style='padding: 0;vertical-align: top;text-align: left'>";
        message += "<div><div class='column-top' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div></div>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<div style='height:15px'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<h2 style='font-style: normal;font-weight: 700;Margin-bottom: 0;Margin-top: 0;font-size: 24px;line-height: 32px;font-family: &quot;Open Sans&quot;,sans-serif;color: #44a8c7;text-align: center'><span style='color:#fc635e'>";
        message += "Welcome to Traivio!</span></h2><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 16px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "You've been invited to join an instance of Traivio by " + fromName + "</p><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 24px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "Just click the link below to finish set up and get started in Traivio.</p>";
        message += "<p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 24px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "<a href='http://172.29.20.141:8080/MAIN/#/userSetup?l="+returnStringLib+"&g="+returnStringGroup+"&s="+returnStringSec+"&e="+toAddress+"'>Click Here to continue</a></p>";
        message += "</td></tr></tbody></table>";
        message += "<div class='column-bottom' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div>";
        message += "</td></tr></tbody></table></td>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>&nbsp;</td></tr></tbody></table></center>";
        message += "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>";
        message += "<script type='text/javascript'>";
        message += "$(document).ready(function () {CS.WebVersion.setup({'LikeActionBase':'/t/i-fb-ttqkut-l-','IsSubscriber':false});});</script>";
        message += "<script type='text/javascript'>$(function(){$('area,a').attr('target', '_blank');});</script>";
        message += "<div id='facebox' style='display:none;'><div class='popup'><div class='content'></div><div id='closeBox'><a href='#' class='close' target='_blank'></a></div></div></div></body></html>";
        
        msg.setContent(message, "text/html");
 
        // sends the e-mail
        Transport.send(msg);
        
        return checker;
    }
	
	@POST  
    @Path("/sendPasswordReset")  
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String sendPasswordReset(String masterString)
   		 throws AddressException, MessagingException {
		CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String email = jobject.get("email").toString();
 	    email = email.substring(1, email.length()-1);
		
		Logger log = Logger.getLogger(HtmlEmailSender.class.getName());
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "172.29.20.38");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.auth", "false");
//        properties.put("mail.smtp.starttls.enable", "true");
 
        Session session = Session.getInstance(properties);
        String lib = "PMCOMMON";
        
        try {
			CONNECTOR = connectionHelper.getConnection(lib);
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		conn = CONNECTOR.conn;
		
		CallableStatement stmt = null;
		String query = "CALL SPPASSRU00(?,?)";
	    
	    UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
    
		try {
			stmt = conn.prepareCall(query);
			stmt.setString(1, randomUUIDString);
			stmt.setString(2, email);
			stmt.execute();	

		} catch (SQLException e ) {
			log.warn(e);
		} finally {
			if (stmt != null) { try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} }
		}
		
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        
        log.info("...Sending Email");
        
        Encrypt encryptLibrary = new Encrypt();
   	 	String returnStringID = encryptLibrary.encryptLibrary(randomUUIDString);
   	 	String returnStringEmail = encryptLibrary.encryptLibrary(email);
   	 
        String checker = "";
        try {
			msg.setFrom(new InternetAddress("do_not_reply@traiv.io", "Traivio"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        InternetAddress[] toAddresses = { new InternetAddress(email) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject("Forgotten Password");
        msg.setSentDate(new Date());
        // set plain text message
        String message = "<html><head><title></title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
        message += "<style type='text/css'>@import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,700,400);</style>";
        message += "<meta name='robots' content='noindex,nofollow'><meta property='og:title' content='Traivio Password Reset'>";
        message += "</head><body style='margin: 0;padding: 0;min-width: 100%;background-color: #f5f7fa'>";
        message += "<center class='wrapper' style='display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #f5f7fa'>";
        message += "<table class='header centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'><table class='preheader' style='border-collapse: collapse;border-spacing: 0' align='right'>";
        message += "<tbody><tr><td class='emb-logo-padding-box' style='padding: 0;vertical-align: top;padding-bottom: 24px;padding-top: 24px;text-align: right;width: 280px;letter-spacing: 0.01em;line-height: 17px;font-weight: 400;font-size: 11px;color: #b9b9b9'>";
        message += "<div class='spacer' style='font-size: 1px;line-height: 2px;width: 100%'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table style='border-collapse: collapse;border-spacing: 0' align='left'><tbody><tr>";
        message += "<td class='logo emb-logo-padding-box' style='padding: 0;vertical-align: top;mso-line-height-rule: at-least;width: 280px;padding-top: 24px;padding-bottom: 24px'>";
        message += "<div class='logo-left' style='font-weight: 700;font-family: Avenir,sans-serif;color: #555;font-size: 0px !important;line-height: 0 !important' align='left' id='emb-email-header'><img style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 300px' src='https://i1.createsend1.com/ei/i/A8/C87/B49/223903/csfinal/traivio.png' alt='Trav.io Logo' width='200' height='53'></div>";
        message += "</td></tr></tbody></table></td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>​</td></tr></tbody></table>";
        message += "<table class='centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto'><tbody><tr>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td>";
        message += "<td style='padding: 0;vertical-align: top'>";
        message += "<table class='one-col' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #ffffff;table-layout: fixed' emb-background-style=''>";
        message += "<tbody><tr><td class='column' style='padding: 0;vertical-align: top;text-align: left'>";
        message += "<div><div class='column-top' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div></div>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<div style='height:15px'>&nbsp;</div></td></tr></tbody></table>";
        message += "<table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%'><tbody><tr>";
        message += "<td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word'>";
        message += "<h2 style='font-style: normal;font-weight: 700;Margin-bottom: 0;Margin-top: 0;font-size: 24px;line-height: 32px;font-family: &quot;Open Sans&quot;,sans-serif;color: #44a8c7;text-align: center'><span style='color:#fc635e'>";
        message += "Forgotten Password</span></h2><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 16px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "A Password Reset was requested for this email address. Please use the link below to reset your passsword.</p><p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 24px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "The link will be valid for 15 minutes. If you did not request this reset, please ignore this email.</p>";
        message += "<p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 24px;font-size: 15px;line-height: 24px;font-family: &quot;Open Sans&quot;,sans-serif;color: #60666d;text-align: center'>";
        message += "<a href='http://mfs.traiv.io/#/passwordReset?e="+returnStringEmail+"&t="+returnStringID+"'>Click Here to continue</a></p>";
        message += "</td></tr></tbody></table>";
        message += "<div class='column-bottom' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all'>&nbsp;</div>";
        message += "</td></tr></tbody></table></td>";
        message += "<td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #dddee1;width: 1px'>​</td></tr></tbody></table>";
        message += "<table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #dddee1;Margin-left: auto;Margin-right: auto' width='602'>";
        message += "<tbody><tr><td style='padding: 0;vertical-align: top'>&nbsp;</td></tr></tbody></table></center>";
        message += "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>";
        message += "<script type='text/javascript'>";
        message += "$(document).ready(function () {CS.WebVersion.setup({'LikeActionBase':'/t/i-fb-ttqkut-l-','IsSubscriber':false});});</script>";
        message += "<script type='text/javascript'>$(function(){$('area,a').attr('target', '_blank');});</script>";
        message += "<div id='facebox' style='display:none;'><div class='popup'><div class='content'></div><div id='closeBox'><a href='#' class='close' target='_blank'></a></div></div></div></body></html>";
        
        msg.setContent(message, "text/html");
 
        // sends the e-mail
        Transport.send(msg);
        
        return checker;
    }
}