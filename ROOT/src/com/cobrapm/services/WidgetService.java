package com.cobrapm.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.doa.UserDisplayRecord;
import com.cobrapm.doa.WidgetMasterRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
   
@Path("WidgetService")  
public class WidgetService { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(WidgetService.class.getName());
		
     @GET  
     @Path("/getWidget/{lib}/{user}/{sessionID}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String getWidget(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	WidgetMasterRecord WidgetMstrTmp = new WidgetMasterRecord();
    	ArrayList<WidgetMasterRecord> WidgetMasterRecords = new ArrayList<WidgetMasterRecord>();
//    	Encrypt encrypt = new Encrypt();
    	
    	String json = "";
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
		    	
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPTUSECR02(?)";					
				
		    log.info("Getting Widget Properties");			
		    				    			    
		    try {
   	
		        stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, user);	               		        
			    stmt.execute();
			        
			    ResultSet rs = stmt.getResultSet();
			    while (rs.next()) {
			    	WidgetMstrTmp = new WidgetMasterRecord();
			    	WidgetMstrTmp.SeqNum = rs.getInt(1);
			    	WidgetMstrTmp.Title = rs.getString(2).trim();
			    	String tmptype = rs.getString(3);
			    	WidgetMstrTmp.Assigned = rs.getString(4);
			    	WidgetMstrTmp.Role = rs.getString(5);
			    	WidgetMstrTmp.ProcessType = rs.getString(6);
			    	WidgetMstrTmp.Cat = rs.getString(7);
			    	WidgetMstrTmp.Cat1 = rs.getString(8);
			    	WidgetMstrTmp.Cat2 = rs.getString(9);
			    	WidgetMstrTmp.Cat3 = rs.getString(10);
			    	WidgetMstrTmp.Color = "#" + rs.getString(11);
			    	String checker = rs.getString(12);
			    	try{
			    		WidgetMstrTmp.status = Integer.parseInt(checker);
			    	}catch (NumberFormatException nfe){}	
			    	if(tmptype.equals("T")){
			    		WidgetMstrTmp.PorT = "Task";
			    	} else {
			    		WidgetMstrTmp.PorT = "Process";
			    	}
			    	WidgetMstrTmp.ShowNotStart = rs.getString(13);
			    	WidgetMstrTmp.Order = rs.getInt(14);
			    	WidgetMasterRecords.add(WidgetMstrTmp);
			    }
			    rs.close();
		        stmt.close();
		        
		        log.info("Retrived User Widgets");
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return json;
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
		    
		    log.info("...Got Widgets, now load PPTSESSION to get Stats");
		    
		    stmt = null;
		    query = "CALL SPTSESRD00(?,?)";				
		    for(WidgetMasterRecord WMRec: WidgetMasterRecords){				    			    
		    try {
		    	
		        stmt = conn.prepareCall(query);
		        stmt.setBigDecimal(1, sessionID);
		        stmt.setInt(2, WMRec.SeqNum);
		        stmt.execute();			        
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
		    }
		    log.info("Deleted old data");
		    		
		    for(WidgetMasterRecord WMRec: WidgetMasterRecords){
		    	String jobType = "";
		    	String holder = WMRec.Role;
		    	if(WMRec.Role.trim().equals("Use Org Chart")){
		    		jobType = "D";
		    		WMRec.Role = "";
		    	} else {
		    		jobType = "J";
		    	}
		    	if(WMRec.status == 0 || WMRec.status == 2){
		    		if(WMRec.PorT.equals("Task")){
		    			stmt = null;
		    			query = "CALL SPPMTCR10(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		    			try {
		    				java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
				    	
		    				log.info(sessionID);
				    	
		    				stmt = conn.prepareCall(query);
		    				stmt.setBigDecimal(1, sessionID);
		    				stmt.setInt(2, WMRec.SeqNum);
		    				stmt.setString(3, WMRec.Role.trim());
		    				stmt.setString(4, "");
		    				stmt.setString(5, "");
		    				stmt.setString(6, WMRec.ProcessType);
		    				stmt.setString(7, WMRec.Cat1);
		    				stmt.setString(8, "");
		    				stmt.setString(9, "");
		    				stmt.setString(10, "");
		    				stmt.setString(11, WMRec.Assigned.trim());
		    				stmt.setString(12, "");		        
		    				stmt.setString(13, "N");
		    				stmt.setString(14, "");
		    				stmt.setString(15, "0");
		    				stmt.setString(16, jobType);
		    				stmt.setDate(17, timeNow);
		    				stmt.setString(18, "");
		    				stmt.setString(19, "");
		    				stmt.setInt(20, 0);		        
		    				stmt.execute();			        
		    				stmt.close();
		    				
		    				stmt = null;
		    				stmt = conn.prepareCall(query);
		    				stmt.setBigDecimal(1, sessionID);
		    				stmt.setInt(2, WMRec.SeqNum);
		    				stmt.setString(3, WMRec.Role.trim());
		    				stmt.setString(4, "");
		    				stmt.setString(5, "");
		    				stmt.setString(6, WMRec.ProcessType);
		    				stmt.setString(7, WMRec.Cat1);
		    				stmt.setString(8, "");
		    				stmt.setString(9, "");
		    				stmt.setString(10, "");
		    				stmt.setString(11, WMRec.Assigned.trim());
		    				stmt.setString(12, "");		        
		    				stmt.setString(13, "N");
		    				stmt.setString(14, "");
		    				stmt.setString(15, "5");
		    				stmt.setString(16, jobType);
		    				stmt.setDate(17, timeNow);
		    				stmt.setString(18, "");
		    				stmt.setString(19, "");
		    				stmt.setInt(20, 0);		        
		    				stmt.execute();			        
		    				stmt.close();
				        
		    			} catch (SQLException e ) {
		    				log.warn(e);
		    			} finally {
		    				if (stmt != null) { stmt.close(); }
		    			}	
		    		} else{
		    			stmt = null;
		    			query = "CALL SPPMPCR07(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";	
		    			try {
		    				java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    	
		    				log.info(sessionID);
   	
		    				stmt = conn.prepareCall(query);
		    				stmt.setBigDecimal(1, sessionID);
		    				stmt.setInt(2, WMRec.SeqNum);
		    				stmt.setString(3, WMRec.Role.trim());
		    				stmt.setString(4, WMRec.ProcessType);
		    				stmt.setString(5, "");
		    				stmt.setString(6, WMRec.Cat1);
		    				stmt.setString(7, "");
		    				stmt.setString(8, "");
		    				stmt.setString(9, "");
		    				stmt.setString(10, WMRec.Assigned.trim());
		    				stmt.setString(11, "");		        
		    				stmt.setString(12, "N");
		    				stmt.setString(13, "");
		    				stmt.setString(14, "0");
		    				stmt.setString(15, jobType);
		    				stmt.setDate(16, timeNow);
		    				stmt.setString(17, "");
		    				stmt.setString(18, "");
		    				stmt.setInt(19, 0);		        
		    				stmt.execute();			        
		    				stmt.close();
		    				
		    				stmt = null;
		    				stmt = conn.prepareCall(query);
		    				stmt.setBigDecimal(1, sessionID);
		    				stmt.setInt(2, WMRec.SeqNum);
		    				stmt.setString(3, WMRec.Role.trim());
		    				stmt.setString(4, WMRec.ProcessType);
		    				stmt.setString(5, "");
		    				stmt.setString(6, WMRec.Cat1);
		    				stmt.setString(7, "");
		    				stmt.setString(8, "");
		    				stmt.setString(9, "");
		    				stmt.setString(10, WMRec.Assigned.trim());
		    				stmt.setString(11, "");		        
		    				stmt.setString(12, "N");
		    				stmt.setString(13, "");
		    				stmt.setString(14, "5");
		    				stmt.setString(15, jobType);
		    				stmt.setDate(16, timeNow);
		    				stmt.setString(17, "");
		    				stmt.setString(18, "");
		    				stmt.setInt(19, 0);		        
		    				stmt.execute();			        
		    				stmt.close();
				        
		    			} catch (SQLException e ) {
		    				log.warn(e);
		    			} finally {
		    				if (stmt != null) { stmt.close(); }
		    			}
		    		}
		    	}
		    	
		    	if(WMRec.status == 1 || WMRec.status == 2){
			    	if(WMRec.PorT.equals("Task")){
			    	
			    		stmt = null;
			    		query = "CALL SPPMTCR10(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
			    	
			    		try {
			    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
			    	
			    			log.info(sessionID);
	   	
			    			stmt = conn.prepareCall(query);
			    			stmt.setBigDecimal(1, sessionID);
			    			stmt.setInt(2, WMRec.SeqNum);
			    			stmt.setString(3, WMRec.Role.trim());
			    			stmt.setString(4, "");
			    			stmt.setString(5, "");
			    			stmt.setString(6, WMRec.ProcessType);
			    			stmt.setString(7, WMRec.Cat1);
			    			stmt.setString(8, "");
			    			stmt.setString(9, "");
			    			stmt.setString(10, "");
			    			stmt.setString(11, WMRec.Assigned.trim());
			    			stmt.setString(12, "");		        
			    			stmt.setString(13, "Y");
			    			stmt.setString(14, "");
			    			stmt.setString(15, "1");
			    			stmt.setString(16, jobType);
			    			stmt.setDate(17, timeNow);
			    			stmt.setString(18, "");
			    			stmt.setString(19, "");
			    			stmt.setInt(20, 0);		        
			    			stmt.execute();			        
			    			stmt.close();
			    			
			    		} catch (SQLException e ) {
			    			log.warn(e);
			    		} finally {
			    			if (stmt != null) { stmt.close(); }
			    		}
			    	
			    	} else {
			    	
			    		stmt = null;
			    		query = "CALL SPPMPCR07(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
			    	
			    		try {
			    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
			    	
			    			log.info(sessionID);
	   	
			    			stmt = conn.prepareCall(query);
			    			stmt.setBigDecimal(1, sessionID);
			    			stmt.setInt(2, WMRec.SeqNum);
			    			stmt.setString(3, WMRec.Role.trim());
			    			stmt.setString(4, WMRec.ProcessType);
			    			stmt.setString(5, "");
			    			stmt.setString(6, WMRec.Cat1);
			    			stmt.setString(7, "");
			    			stmt.setString(8, "");
			    			stmt.setString(9, "");
			    			stmt.setString(10, WMRec.Assigned.trim());
			    			stmt.setString(11, "");		        
			    			stmt.setString(12, "Y");
			    			stmt.setString(13, "");
			    			stmt.setString(14, "1");
			    			stmt.setString(15, jobType);
			    			stmt.setDate(16, timeNow);
			    			stmt.setString(17, "");
			    			stmt.setString(18, "");
			    			stmt.setInt(19, 0);		        
			    			stmt.execute();			        
			    			stmt.close();
			        
			    		} catch (SQLException e ) {
			    			log.warn(e);
			    		} finally {
			    			if (stmt != null) { stmt.close(); }
			    		}
			    	
			    	}
				}
		    	WMRec.Role = holder;
		    	if(WMRec.ShowNotStart != null){
		    		if(WMRec.ShowNotStart.equals("N")){
		    			stmt = null;
		    			query = "CALL SPPTSDR00(?,?)";				
		    	
		    			try {
		    				stmt = conn.prepareCall(query);
		    				stmt.setBigDecimal(1, sessionID);
		    				stmt.setInt(2, WMRec.SeqNum);        
		    				stmt.execute();			        
		    				stmt.close();
		        
		    			} catch (SQLException e ) {
		    				log.warn(e);
		    			} finally {
		    				if (stmt != null) { stmt.close(); }
		    			}
		    		}
		    	}
		    }
		    	
		    log.info("...PPTSESSION built, now calling metrics");
		    
		    stmt = null;
		    query = "CALL SPTUSECR03(?,?)";	
		    stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    
		    for(WidgetMasterRecord WMRec: WidgetMasterRecords){
		    	stmt.setBigDecimal(1, sessionID);
		    	stmt.setInt(2, WMRec.SeqNum);
			    stmt.execute();
			    
			    ResultSet rs = stmt.getResultSet();
			    while (rs.next()) {
			    	WMRec.Count = rs.getInt(1);
			    	WMRec.CountOverdue = rs.getInt(2);
			    	WMRec.CountDueToday = rs.getInt(3);
			    	log.info("...Count: " + WMRec.Count);
			    }
			    rs.close();
		    }
		    stmt.close();
			
			stmt = null;
		    query = "CALL SPUDSPCR00(?,?)";	
		    stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    
		    for(WidgetMasterRecord WMRec: WidgetMasterRecords){
		    	stmt.setInt(1, WMRec.SeqNum);
		    	stmt.setString(2, user);
			    stmt.execute();
			    
			    ResultSet rs = stmt.getResultSet();
			    while (rs.next()) {
			    	UserDisplayRecord userDisplayRec = new UserDisplayRecord();
			    	userDisplayRec.DSSEQ = rs.getInt(1);
			    	userDisplayRec.DSORDER = rs.getInt(2);
			    	userDisplayRec.DSTYPE = rs.getString(3).trim();
			    	userDisplayRec.DSSEQNUM = rs.getInt(4);
			    	userDisplayRec.NAMID = rs.getString(5).trim();
			    	userDisplayRec.WIDTH = rs.getString(6);
			    	if(userDisplayRec.WIDTH == null){
			    		userDisplayRec.WIDTH = "10";
			    	} else {
			    		userDisplayRec.WIDTH = userDisplayRec.WIDTH.trim();
			    	}
			    	userDisplayRec.sort = userDisplayRec.NAMID.replaceAll("\\s+", "");
			    	userDisplayRec.sortfunc = userDisplayRec.NAMID.replaceAll("\\s+", "");
			    	if(userDisplayRec.sort.equals("StartDate")){
			    		userDisplayRec.sortfunc = "getters.startDateSetter";
			    	}
			    	if(userDisplayRec.sort.equals("EndDate")){
			    		userDisplayRec.sortfunc = "getters.endDateSetter";
			    	}
			    	if(userDisplayRec.NAMID.equals("Process Name")){
			    		userDisplayRec.colstyle = "text-align: left !important; cursor:pointer; color:black;";
			    	}
			    	WMRec.columns.add(userDisplayRec);
			    }
			    rs.close();
		    }
		    stmt.close();
		    
		    connectionHelper.closeConnection(conn);
		    
		    json = new Gson().toJson(WidgetMasterRecords);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return json;
      }
     
     @POST
  	@Path("/updateWidgetOrder")
  	@Consumes(MediaType.APPLICATION_JSON)
  	@Produces(MediaType.APPLICATION_JSON)
  	public String updateWidgetOrder(String masterString){
  	CONNECTOR CONNECTOR = new CONNECTOR(); 
  		Connection conn = null;
  		
  		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
  		ArrayList<WidgetMasterRecord> masterRecords = new ArrayList<WidgetMasterRecord>();
//  		Encrypt encrypt = new Encrypt();
  		
  		log.info("..mapping JSON string");
//  		log.info("..." + masterString);
  		
  		JsonElement jelement = new JsonParser().parse(masterString);
  	    JsonObject  jobject = jelement.getAsJsonObject();
  	    String lib = jobject.get("lib").toString();
  	    lib = lib.substring(1, lib.length()-1);
  	    String userid = jobject.get("userid").toString();
  	    userid = userid.substring(1, userid.length()-1);
  	    String widgets_tmp = jobject.get("widgets").toString();
  		
      	ObjectMapper mapper = new ObjectMapper();
      	try {
      		 masterRecords = mapper.readValue(widgets_tmp, new TypeReference<ArrayList<WidgetMasterRecord>>(){});
  		} catch (JsonParseException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (JsonMappingException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		} catch (IOException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
  		
  		String json = "";
  		
  	    try
  	    	{	    		    	
  		        CONNECTOR = connectionHelper.getConnection(lib);
  	  			conn = CONNECTOR.conn;
  		        
  	  			log.info("..calling SP: SPORDERR00/SPORDERU00 to update Widgets");
  	  			
  	  			CallableStatement stmt = null;
			    String query = "CALL SPORDERR00(?)";
			   try {
				  stmt = conn.prepareCall(query);

			      stmt.setString(1, userid);
			      stmt.executeUpdate();			        
			      stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
  		        
  				stmt = null;
  			    query = "CALL SPORDERU00(?,?,?)";
  			   try {
  				  stmt = conn.prepareCall(query);
  				  
  				  for(WidgetMasterRecord masterRecord: masterRecords){

  			        stmt.setString(1, userid);
  			        stmt.setInt(2, masterRecord.SeqNum);
  			        stmt.setInt(3, masterRecord.Order);

  			        stmt.executeUpdate();			        
  				  }	
  			        stmt.close();
  			        
  			        
  			    } catch (SQLException e ) {
  			    	log.warn(e);
  			    } finally {
  			        if (stmt != null) { stmt.close(); }
  			    }
  			    
  			 connectionHelper.closeConnection(conn);
  	    	}
  	    
  	    catch(Exception ex)
  	    {
  	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
  	        authorizationRecord.returnErrorMessage = ex.getMessage();
  	    	
  			log.error(authorizationRecord.returnMessage);
  			log.error(ex.getMessage());			
  			
  			authorizationRecord.isError = true;
  			json = "An error occured during the authenticate process.";
  			connectionHelper.closeConnection(conn);
  		    return(json);
  	    }
  	    return(json);
  	}
     
     @POST
   	@Path("/updateWidgetColumnOrder")
   	@Consumes(MediaType.APPLICATION_JSON)
   	@Produces(MediaType.APPLICATION_JSON)
   	public String updateWidgetColumnOrder(String masterString){
    	 CONNECTOR CONNECTOR = new CONNECTOR(); 
   		Connection conn = null;
   		
   		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
   		ArrayList<UserDisplayRecord> masterRecords = new ArrayList<UserDisplayRecord>();
//   		Encrypt encrypt = new Encrypt();
   		
   		log.info("..mapping JSON string");
//   		log.info("..." + masterString);
   		
   		JsonElement jelement = new JsonParser().parse(masterString);
   	    JsonObject  jobject = jelement.getAsJsonObject();
   	    String lib = jobject.get("lib").toString();
   	    lib = lib.substring(1, lib.length()-1);
   	    String userid = jobject.get("userid").toString();
   	    userid = userid.substring(1, userid.length()-1);
   	    String columns_tmp = jobject.get("columns").toString();
   		
       	ObjectMapper mapper = new ObjectMapper();
       	try {
       		 masterRecords = mapper.readValue(columns_tmp, new TypeReference<ArrayList<UserDisplayRecord>>(){});
   		} catch (JsonParseException e1) {
   			// TODO Auto-generated catch block
   			e1.printStackTrace();
   		} catch (JsonMappingException e1) {
   			// TODO Auto-generated catch block
   			e1.printStackTrace();
   		} catch (IOException e1) {
   			// TODO Auto-generated catch block
   			e1.printStackTrace();
   		}
   		
   		String json = "";
   		
   	    try
   	    	{	    		    	
   		        CONNECTOR = connectionHelper.getConnection(lib);
   	  			conn = CONNECTOR.conn;
   		        
   	  			log.info("..calling SP: SPUDSPRR00/SPUDSPRU00 to update Widgets");
   	  			
   	  			CallableStatement stmt = null;
			    String query = "CALL SPUDSPRR00(?,?)";
				try {
					stmt = conn.prepareCall(query);
				   stmt.setString(1, userid);
				   stmt.setInt(2, masterRecords.get(0).DSSEQ);
				   stmt.executeUpdate();			        
				   stmt.close();
		        
		        
			 	} catch (SQLException e ) {
			 		log.warn(e);
			 	} finally {
			 		if (stmt != null) { stmt.close(); }
			 	}
 				
 				for(UserDisplayRecord masterRec: masterRecords){
   		        
 					 stmt = null;
 					 query = "CALL SPUDSPRU00(?,?,?,?,?)";
 					 try {
 						 stmt = conn.prepareCall(query);

 						 stmt.setString(1, userid);
 						 stmt.setInt(2, masterRec.DSSEQ);
 						 stmt.setInt(3, masterRec.DSORDER);
 						 stmt.setString(4, masterRec.DSTYPE);
 						 stmt.setInt(5, masterRec.DSSEQNUM);
 					
 						 stmt.executeUpdate();			        	
 						 stmt.close();
   			                
 					 } catch (SQLException e ) {
 						 log.warn(e);
 					 } finally {
 						 if (stmt != null) { stmt.close(); }
 					 }
   			    
 				 }
 				connectionHelper.closeConnection(conn);
   	    	}
   	    catch(Exception ex)
   	    {
   	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
   	        authorizationRecord.returnErrorMessage = ex.getMessage();
   	    	
   			log.error(authorizationRecord.returnMessage);
   			log.error(ex.getMessage());			
   			
   			authorizationRecord.isError = true;
   			json = "An error occured during the authenticate process.";
   			connectionHelper.closeConnection(conn);
   		    return(json);
   	    }
   	    return(json);
   	}
     
     @GET  
     @Path("/modifyWidget/{lib}/{user}/{sessionID}/{widgetIn}")  
     @Produces(MediaType.APPLICATION_JSON)
     public Response modifyWidget(@PathParam("lib") String lib, @PathParam("user") String user, 
    		 @PathParam("sessionID") BigDecimal sessionID, @PathParam("widgetIn") String widgetIn) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	log.info("...JSON: " + widgetIn);
    	
    	WidgetMasterRecord WMRec = new WidgetMasterRecord();
//    	Encrypt encrypt = new Encrypt();
    	
    	String widgetDecode = "";
		try {
			widgetDecode = URLDecoder.decode(widgetIn, "UTF-8");
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			WMRec = mapper.readValue(widgetDecode, new TypeReference<WidgetMasterRecord>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
		    	
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPTUSERU03(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";					
				
		    log.info("Updating Widget");	
		    String SQLCODEN = "";
		    				    			    
		    try {
		    	String stringOut = Integer.toString(WMRec.status);
		        stmt = conn.prepareCall(query);
		        stmt.registerOutParameter(2, Types.INTEGER);
		        stmt.registerOutParameter(16, Types.VARCHAR);
		        stmt.setString(1, user);
		        stmt.setInt(2, WMRec.SeqNum);
		        stmt.setString(3, WMRec.Title);
		        stmt.setString(4, WMRec.PorT);
		        stmt.setString(5, WMRec.Assigned);
		        stmt.setString(6, WMRec.Role);
		        stmt.setString(7, WMRec.ProcessType);
		        stmt.setString(8, WMRec.Cat);
		        stmt.setString(9, WMRec.Cat1);
		        stmt.setString(10, WMRec.Cat2);
		        stmt.setString(11, WMRec.Cat3);
		        stmt.setString(12, WMRec.Color);
		        stmt.setString(13, stringOut);
		        stmt.setString(14, WMRec.ShowNotStart);
		        stmt.setString(15, WMRec.Mode);
		        
			    stmt.executeUpdate();
			    
			    if(WMRec.SeqNum == 0){
		        	WMRec.SeqNum = stmt.getInt(2);
		        }
			    
		        stmt.close();
		        
		        log.info("Updated User Widgets");
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
		    
		    stmt = null;
	    	query = "CALL SPUDSPRR00(?,?)";							    			    
	    	try {
	    		stmt = conn.prepareCall(query);
	    		stmt.setString(1, user);
	    		stmt.setInt(2, WMRec.SeqNum);
	        
	    		stmt.executeUpdate();
		        
	    		stmt.close();
	        
	    	} catch (SQLException e ) {
	    		log.warn(e);
	    	} finally {
	    		if (stmt != null) { stmt.close(); }
	    	}
	    	
		    for(UserDisplayRecord masterRec: WMRec.columns){
		    	
		    	if(masterRec.DSSEQ == 0){
		    		masterRec.DSSEQ = WMRec.SeqNum;
		    	}
		    	stmt = null;
		    	query = "CALL SPUDSPRU00(?,?,?,?,?)";			
		    				    			    
		    	try {
		    		stmt = conn.prepareCall(query);
		    		stmt.setString(1, user);
					stmt.setInt(2, masterRec.DSSEQ);
					stmt.setInt(3, masterRec.DSORDER);
					stmt.setString(4, masterRec.DSTYPE);
					stmt.setInt(5, masterRec.DSSEQNUM);
		        
		    		stmt.executeUpdate();
			        
		    		stmt.close();
		        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    
			connectionHelper.closeConnection(conn);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return Response.status(200).entity("success").build(); 
      }   	
}  
