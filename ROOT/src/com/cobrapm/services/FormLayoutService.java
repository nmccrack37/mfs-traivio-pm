package com.cobrapm.services;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.ModuleTaskRecord;
import com.cobrapm.doa.PRELDESC;
import com.cobrapm.forms.FormDetailLayoutRecord;
import com.cobrapm.forms.FormLayoutRecord;
import com.cobrapm.forms.PFRMPROP;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("FormLayoutService")
public class FormLayoutService {
	
	ModuleTaskRecord CURRENT_APPLICATION = new ModuleTaskRecord();

	static Properties props;
	static Properties resourceBundle;
	static Connection conn = null;
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(NotificationService.class.getName());
	
	@POST
	@Path("/retrieveFormData")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String retrieveFormData(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<PFRMPROP> propertyRecord = new ArrayList<PFRMPROP>();
		ArrayList<FormLayoutRecord> layoutRecord = new ArrayList<FormLayoutRecord>();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    String userid = jobject.get("userid").toString();
	    userid = userid.substring(1, userid.length()-1);
	    int formid = jobject.get("formid").getAsInt();
		
		String json = "";
		String json2 = "";
		String jsonAll = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SFRMPRCR00 to get Form Properties");
		        
				CallableStatement stmt = null;
			    String query = "CALL SFRMPRCR00(?)";
			    log.info(formid);

			    try {
			    	stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			        stmt.setInt(1, formid);

			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while(rs.next()){
			        	PFRMPROP tmp = new PFRMPROP();
			        	tmp.FPID = rs.getInt(1);
 						tmp.FPSEQ = rs.getInt(2);
 						tmp.FPNAME = rs.getString(3).trim();
 						tmp.FPTYPE = rs.getString(4).trim();
 						tmp.FPREQ = rs.getString(5).trim();
 						propertyRecord.add(tmp);
 					}
			        
			        rs.close();
					
			        stmt.close();
			        
			        
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			    
			    json = new Gson().toJson(propertyRecord);
			    log.info("..calling SP: SFLSECCR00 to get Form Sections");
		        
			    stmt = null;
			    query = "CALL SFLSECCR00(?)";
 				try {
 					stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
 					stmt.setInt(1, formid);		
 					stmt.execute();
 						
 					ResultSet rs = stmt.getResultSet();
 					while(rs.next()){
 						FormLayoutRecord tmpRecord = new FormLayoutRecord();
 						tmpRecord.SectionSeq =  rs.getInt(1);
 						tmpRecord.Columns = rs.getInt(2);
 						layoutRecord.add(tmpRecord);
 					}
 					stmt.close();
 			        
 				} catch (Exception e ) {
 					log.info(e);
 				} finally {
 					if (stmt != null) { stmt.close(); }
 				}
 				
 				stmt = null;
			    query = "CALL SFLSECCR01(?,?)";
 				for(FormLayoutRecord tmpRecord: layoutRecord){
 					try {
 	 					stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
 	 					stmt.setInt(1, formid);		
 	 					stmt.setInt(2, tmpRecord.SectionSeq);
 	 					stmt.execute();
 	 						
 	 					ResultSet rs = stmt.getResultSet();
 	 					while(rs.next()){
 	 						FormDetailLayoutRecord tmpRecord2 = new FormDetailLayoutRecord();
 	 						tmpRecord2.ColumnSeq =  rs.getInt(1);
 	 						tmpRecord2.LayoutSeq = rs.getInt(2);
 	 						tmpRecord2.PropSeq = rs.getInt(3);
 	 						tmpRecord.formLayoutDetails.add(tmpRecord2);
 	 					}
 	 					stmt.close();
 	 			        
 	 				} catch (Exception e ) {
 	 					log.info(e);
 	 				} finally {
 	 					if (stmt != null) { stmt.close(); }
 	 				}
 					
 				}

				 connectionHelper.closeConnection(conn);
				 json2 = new Gson().toJson(layoutRecord);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    jsonAll = "["+json+","+json2+"]";
	    return(jsonAll);
	}
	
	@POST
	@Path("/updateFormData")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateFormData(String masterString){
	CONNECTOR CONNECTOR = new CONNECTOR(); 
		Connection conn = null;
		
		AuthorizationRecord authorizationRecord = new AuthorizationRecord();
		ArrayList<FormLayoutRecord> layoutRecord = new ArrayList<FormLayoutRecord>();
		
		log.info("..mapping JSON string");
		log.info("..." + masterString);
		
		JsonElement jelement = new JsonParser().parse(masterString);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    String lib = jobject.get("lib").toString();
	    lib = lib.substring(1, lib.length()-1);
	    String userid = jobject.get("userid").toString();
	    userid = userid.substring(1, userid.length()-1);
	    int formid = jobject.get("formid").getAsInt();
	    String layout_tmp = jobject.get("layout").toString();
	    
	    ObjectMapper mapper = new ObjectMapper();
    	try {
    		layoutRecord = mapper.readValue(layout_tmp, new TypeReference<ArrayList<FormLayoutRecord>>(){});
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String json = "";
		
	    try
	    	{	    		    	
		        CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
		        
	  			log.info("..calling SP: SFLSECDR00 to delete Form Layout");
		        
				CallableStatement stmt = null;
			    String query = "CALL SFLSECDR00(?)";
			    
			    try {
		    		stmt = conn.prepareCall(query);
		    		stmt.setInt(1, formid);
		    		stmt.executeUpdate();
						
		    		stmt.close();
			        
		    	} catch (Exception e ) {
		    		log.info(e);
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
			    
			    stmt = null;
				query = "CALL SFLSECRU00(?,?,?)";
			    
			    log.info("..calling SP: SFLSECRU00 to update Form Sections");
			    
			    for(FormLayoutRecord tmpRecord : layoutRecord){
			    	try {
			    		stmt = conn.prepareCall(query);
			    		stmt.setInt(1, formid);		
			    		stmt.setInt(2, tmpRecord.SectionSeq);
			    		stmt.setInt(3, tmpRecord.Columns);
			    		stmt.executeUpdate();
 						
			    		stmt.close();
 			        
			    	} catch (Exception e ) {
			    		log.info(e);
			    	} finally {
			    		if (stmt != null) { stmt.close(); }
			    	}
			    }
			    
			    for(FormLayoutRecord tmpRecord : layoutRecord){
 					stmt = null;
 					query = "CALL SFLSECRU01(?,?,?,?,?)";
 					for(FormDetailLayoutRecord tmpRecord2: tmpRecord.formLayoutDetails){
 						try {
 							stmt = conn.prepareCall(query);
 							stmt.setInt(1, formid);		
 							stmt.setInt(2, tmpRecord.SectionSeq);
 							stmt.setInt(3, tmpRecord2.ColumnSeq);
 							stmt.setInt(4, tmpRecord2.LayoutSeq);
 							stmt.setInt(5, tmpRecord2.PropSeq);
 							stmt.executeUpdate();

 							stmt.close();
 	 			        
 						} catch (Exception e ) {
 							log.info(e);
 						} finally {
 							if (stmt != null) { stmt.close(); }
 						}
 					}
 				}

				 connectionHelper.closeConnection(conn);
	    	}
	    
	    catch(Exception ex)
	    {
	        authorizationRecord.returnMessage = "An error occured during the authenticate process.";
	        authorizationRecord.returnErrorMessage = ex.getMessage();
	    	
			log.error(authorizationRecord.returnMessage);
			log.error(ex.getMessage());			
			
			authorizationRecord.isError = true;
			json = "An error occured during the authenticate process.";
			connectionHelper.closeConnection(conn);
		    return(json);
	    }
	    return(json);
	}

}
