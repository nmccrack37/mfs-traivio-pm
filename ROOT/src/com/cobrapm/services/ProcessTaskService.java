package com.cobrapm.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.doa.DefinedFieldsRecord;
import com.cobrapm.doa.GroupRecord;
import com.cobrapm.doa.JobFunctionRetrievalRecord;
import com.cobrapm.doa.PINFORecord;
import com.cobrapm.doa.PRELDESC;
import com.cobrapm.doa.ProcessCreateRecord;
import com.cobrapm.doa.ProcessDisplayRecord;
import com.cobrapm.doa.ProcessTypeRecord;
import com.cobrapm.doa.TaskTypeRecord;
import com.cobrapm.doa.UDFRecord;
import com.cobrapm.doa.UserDisplayRecord;
import com.cobrapm.doa.WidgetMasterRecord;
import com.cobrapm.process.UDFData;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
   
@Path("ProcessTaskService")  
public class ProcessTaskService { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private static Logger log = Logger.getLogger(ProcessTaskService.class.getName());
	
	 @POST
	 @Path("/getProcessTasks")
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public String getProcessTasks(String masterString){
	    	 
		CONNECTOR CONNECTOR = new CONNECTOR(); 
	   	Connection conn = null;
	   	
	   	ProcessDisplayRecord ProcessDisplayRecord = new ProcessDisplayRecord();
    	ArrayList<ProcessDisplayRecord> ProcessDisplayRecords = new ArrayList<ProcessDisplayRecord>();
	   	WidgetMasterRecord WMRec = new WidgetMasterRecord();
	   		
	   	log.info("..mapping JSON string");
	   		
	   	JsonElement jelement = new JsonParser().parse(masterString);
	   	JsonObject  jobject = jelement.getAsJsonObject();
	   	String lib = jobject.get("lib").toString();
	   	lib = lib.substring(1, lib.length()-1);
	   	String userid = jobject.get("userid").toString();
	   	userid = userid.substring(1, userid.length()-1);
	   	int sessionID = jobject.get("sessionID").getAsInt();
	   	String widget_tmp = jobject.get("widget").toString();
	   		
	    ObjectMapper mapper = new ObjectMapper();
	    try {
	    	WMRec = mapper.readValue(widget_tmp, new TypeReference<WidgetMasterRecord>(){});
	   	} catch (JsonParseException e1) {
	   		// TODO Auto-generated catch block
	   		e1.printStackTrace();
	   	} catch (JsonMappingException e1) {
	   		// TODO Auto-generated catch block
	   		e1.printStackTrace();
	   	} catch (IOException e1) {
	   		// TODO Auto-generated catch block
	   		e1.printStackTrace();
	   	}
	    
    	String json = "";
    	
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPTSESRD00(?,?)";				
		    				    			    
		    try {
		    	
		        stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, sessionID);
		        stmt.setInt(2, WMRec.SeqNum);
		        stmt.execute();			        
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
				
		    log.info("Deleted old data");
		    
		    String jobType = "";
	    	String holder = WMRec.Role;
	    	if(WMRec.Role.trim().equals("Use Org Chart")){
	    		jobType = "D";
	    		WMRec.Role = "";
	    	} else {
	    		jobType = "J";
	    	}
	    	
		    if(WMRec.status == 0 || WMRec.status == 2){
		    	if(WMRec.PorT.equals("Task")){
		    	
		    		stmt = null;
		    		query = "CALL SPPMTCR10(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
		    	
		    		try {
		    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    	
		    			log.info(sessionID);
   	
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, "");
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.ProcessType);
		    			stmt.setString(7, WMRec.Cat1);
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, "");
		    			stmt.setString(11, WMRec.Assigned.trim());
		    			stmt.setString(12, "");		        
		    			stmt.setString(13, "N");
		    			stmt.setString(14, "");
		    			stmt.setString(15, "0");
		    			stmt.setString(16, jobType);
		    			stmt.setDate(17, timeNow);
		    			stmt.setString(18, "");
		    			stmt.setString(19, "");
		    			stmt.setInt(20, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		    			
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, "");
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.ProcessType);
		    			stmt.setString(7, WMRec.Cat1);
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, "");
		    			stmt.setString(11, WMRec.Assigned.trim());
		    			stmt.setString(12, "");		        
		    			stmt.setString(13, "N");
		    			stmt.setString(14, "");
		    			stmt.setString(15, "5");
		    			stmt.setString(16, jobType);
		    			stmt.setDate(17, timeNow);
		    			stmt.setString(18, "");
		    			stmt.setString(19, "");
		    			stmt.setInt(20, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		    			
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	
		    	} else {
		    	
		    		stmt = null;
		    		query = "CALL SPPMPCR07(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
		    	
		    		try {
		    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    	
		    			log.info(sessionID);
   	
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, WMRec.ProcessType);
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.Cat1);
		    			stmt.setString(7, "");
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, WMRec.Assigned.trim());
		    			stmt.setString(11, "");		        
		    			stmt.setString(12, "N");
		    			stmt.setString(13, "");
		    			stmt.setString(14, "0");
		    			stmt.setString(15, jobType);
		    			stmt.setDate(16, timeNow);
		    			stmt.setString(17, "");
		    			stmt.setString(18, "");
		    			stmt.setInt(19, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		    			
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, WMRec.ProcessType);
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.Cat1);
		    			stmt.setString(7, "");
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, WMRec.Assigned.trim());
		    			stmt.setString(11, "");		        
		    			stmt.setString(12, "");
		    			stmt.setString(13, "");
		    			stmt.setString(14, "5");
		    			stmt.setString(15, jobType);
		    			stmt.setDate(16, timeNow);
		    			stmt.setString(17, "");
		    			stmt.setString(18, "");
		    			stmt.setInt(19, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		        
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	
		    	}
			}
		    
		    if(WMRec.status == 1 || WMRec.status == 2){
		    	if(WMRec.PorT.equals("Task")){
		    	
		    		stmt = null;
		    		query = "CALL SPPMTCR10(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
		    	
		    		try {
		    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    	
		    			log.info(sessionID);
   	
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, "");
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.ProcessType);
		    			stmt.setString(7, WMRec.Cat1);
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, "");
		    			stmt.setString(11, WMRec.Assigned.trim());
		    			stmt.setString(12, "");		        
		    			stmt.setString(13, "Y");
		    			stmt.setString(14, "");
		    			stmt.setString(15, "1");
		    			stmt.setString(16, jobType);
		    			stmt.setDate(17, timeNow);
		    			stmt.setString(18, "");
		    			stmt.setString(19, "");
		    			stmt.setInt(20, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		    			
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	
		    	} else {
		    	
		    		stmt = null;
		    		query = "CALL SPPMPCR07(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
		    	
		    		try {
		    			java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    	
		    			log.info(sessionID);
   	
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, sessionID);
		    			stmt.setInt(2, WMRec.SeqNum);
		    			stmt.setString(3, WMRec.Role.trim());
		    			stmt.setString(4, WMRec.ProcessType);
		    			stmt.setString(5, "");
		    			stmt.setString(6, WMRec.Cat1);
		    			stmt.setString(7, "");
		    			stmt.setString(8, "");
		    			stmt.setString(9, "");
		    			stmt.setString(10, WMRec.Assigned.trim());
		    			stmt.setString(11, "");		        
		    			stmt.setString(12, "Y");
		    			stmt.setString(13, "");
		    			stmt.setString(14, "1");
		    			stmt.setString(15, jobType);
		    			stmt.setDate(16, timeNow);
		    			stmt.setString(17, "");
		    			stmt.setString(18, "");
		    			stmt.setInt(19, 0);		        
		    			stmt.execute();			        
		    			stmt.close();
		        
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	
		    	}
		    	WMRec.Role = holder;	
			}
		    
		    if(WMRec.ShowNotStart.equals("N")){
	    		stmt = null;
	    		query = "CALL SPPTSDR00(?,?)";				
	    	
	    		try {
	
	    			stmt = conn.prepareCall(query);
	    			stmt.setInt(1, sessionID);
	    			stmt.setInt(2, WMRec.SeqNum);        
	    			stmt.execute();			        
	    			stmt.close();
	        
	    		} catch (SQLException e ) {
	    			log.warn(e);
	    		} finally {
	    			if (stmt != null) { stmt.close(); }
	    		}
	    	}
		    
		    log.info("Populating new data");
		    
		    stmt = null;
		    query = "CALL SPTSESCR02(?,?)";				
		    int rownumber = 0;				    			    
		    try {
   	
		        stmt = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, sessionID);
		        stmt.setInt(2, WMRec.SeqNum);		               		        
		        stmt.execute();			
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	ProcessDisplayRecord = new ProcessDisplayRecord();
		        	ProcessDisplayRecord.TKNO = rs.getInt("T4BNO");
		        	ProcessDisplayRecord.TSTSKNO = rs.getInt("T4TSKNO");
		        	ProcessDisplayRecord.TSTSKSNO = rs.getInt("T4TTSKSNO");
		        	ProcessDisplayRecord.TaskName = rs.getString("T4TTYPED");
		        	ProcessDisplayRecord.ProcessName = rs.getString("T4PTYPSD");
		        	ProcessDisplayRecord.Status = setStatus(rs.getString("T4BSTAT"), rs.getDate("T4BACTST"), rs.getDate("T4BDUEDT"), rs.getDate("T4BACTCMP"), rs.getInt("T4BDAYSDUE"), rs.getInt("T4BOTDAYS"));
		        	ProcessDisplayRecord.StartDate = rs.getString("T4BSTARTE");
		        	ProcessDisplayRecord.EndDate = rs.getString("T4BDUEDT");
		        	ProcessDisplayRecord.Days = rs.getInt("T4BDAYSDUE");
		        	ProcessDisplayRecord.Assigned = rs.getString("T4BFULLJD");
		        	ProcessDisplayRecord.Group = rs.getString("T4QUEUE");
		        	ProcessDisplayRecord.RelatedTo = rs.getString("T4BDISPLY");
		        	ProcessDisplayRecord.FormID = rs.getString("T4TTASKID");
		        	ProcessDisplayRecord.SubProc = rs.getString("T4PQUEUED");
		        	ProcessDisplayRecord.ProcessType =  rs.getString("T4BTMPNM");
		        	ProcessDisplayRecord.ProcessSubType = rs.getString("T4PSUBTYFD");
		        	ProcessDisplayRecord.Frequency = rs.getString("T4PFREQFD");
		        	ProcessDisplayRecord.Category = rs.getString("T4BCATD");
		        	ProcessDisplayRecord.SubCategory = rs.getString("T4BSUBCATD");
		        	ProcessDisplayRecord.SubCategory2 = rs.getString("T4BSUBCA2D");
		        	ProcessDisplayRecord.SubCategory3 = rs.getString("T4BSUBCA3D");
		        	ProcessDisplayRecord.Severity = rs.getString("T4SEVERITY");
		        	ProcessDisplayRecord.Amount = rs.getDouble("T4AMOUNT");
		        	ProcessDisplayRecord.Approved = rs.getString("T4APPROVED");
		        	ProcessDisplayRecord.CaseNumber = rs.getString("T4CASE");
		        	ProcessDisplayRecord.JobNumber = rs.getDouble("T4AMOUNT");
		        	ProcessDisplayRecord.HardDeadline = rs.getString("T4APPROVED");
		        	ProcessDisplayRecord.Type = rs.getString("T4CASE");
		        	ProcessDisplayRecord.Estimate = rs.getInt("T4ESTHRS");
		        	
		        	if(ProcessDisplayRecord.Assigned != null){
		        		ProcessDisplayRecord.Assigned =  ProcessDisplayRecord.Assigned.trim();
		        	}
		        	if(ProcessDisplayRecord.Group != null){
		        		if(ProcessDisplayRecord.Assigned == null){
		        			ProcessDisplayRecord.Assigned = ProcessDisplayRecord.Group.trim();
		        		}
		        	}
		        	if(ProcessDisplayRecord.SubProc != null){
		        		ProcessDisplayRecord.SubProc =  ProcessDisplayRecord.SubProc.trim();
		        	}    	
		        	if(ProcessDisplayRecord.ProcessName != null){
		        		ProcessDisplayRecord.ProcessName =  ProcessDisplayRecord.ProcessName.trim();
		        	}
		        	if(ProcessDisplayRecord.TaskName != null){
		        		ProcessDisplayRecord.TaskName =  ProcessDisplayRecord.TaskName.trim();
		        	}
		        	if(ProcessDisplayRecord.RelatedTo != null){
		        		ProcessDisplayRecord.RelatedTo =  ProcessDisplayRecord.RelatedTo.trim();
		        	}
		        	if(ProcessDisplayRecord.HardDeadline != null){
		        		if(ProcessDisplayRecord.HardDeadline.equals("N")){
		        			ProcessDisplayRecord.HardDeadline = "No";
		        		} else {
		        			ProcessDisplayRecord.HardDeadline = "Yes";
		        		}
		        	}
		        	ProcessDisplayRecord.rownumber = rownumber;
		        	ProcessDisplayRecords.add(ProcessDisplayRecord);
		        	rownumber += 1;
		        }
		        
		        rs.close();
		        stmt.close();
		        
		        log.info("Built result set with " + ProcessDisplayRecords.size() + " records.");
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }	
		    
		    log.info("..calling SP: SPRELCR00 to retrieve all Note ID's and Descriptions");
	        
			stmt = null;
		    query = "CALL SPRELCR00(?,?,?)";
		    
		    for(ProcessDisplayRecord processDisplayRecord: ProcessDisplayRecords){
		    	try {
		    		stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    		if(processDisplayRecord.TKNO == 0 || processDisplayRecord.FormID != null){
		    			stmt.setInt(1, processDisplayRecord.TKNO);
		    			stmt.setInt(2, processDisplayRecord.TSTSKNO);
		    			stmt.setInt(3, processDisplayRecord.TSTSKSNO);
		    		} else {
		    			stmt.setInt(1, processDisplayRecord.TKNO);
		    			stmt.setInt(2, 0);
		    			stmt.setInt(3, 0);
		    		}
		    		stmt.execute();			        
		        
		    		ResultSet rs = stmt.getResultSet();
		    		String relText = "";
		    		while (rs.next()) {
		    			String checker = rs.getString(4);
		    			if(checker.equals("T")){
		    				relText += rs.getString(7) + ", ";
		    			} else {
		    				processDisplayRecord.Response = rs.getString(6);
		    			}
		    		}
		    		if(relText.length() > 0){
		    			relText = relText.substring(0, (relText.length()-2));
		    		}
		    		if(relText.equals("null")){
		    			processDisplayRecord.RelatedTo = "";
		    		} else{
		    			processDisplayRecord.RelatedTo = relText;
		    		}
		    		rs.close();
		    		stmt.close();
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    
		    stmt = null;
		    query = "CALL SNOTE2CR03(?,?,?)";
		    
		    for(ProcessDisplayRecord processDisplayRecord: ProcessDisplayRecords){
		    	try {
		    		stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    		if(processDisplayRecord.TKNO == 0){
		    			stmt.setInt(1, processDisplayRecord.TKNO);
		    			stmt.setInt(2, processDisplayRecord.TSTSKNO);
		    			stmt.setInt(3, processDisplayRecord.TSTSKSNO);
		    		} else {
		    			stmt.setInt(1, processDisplayRecord.TKNO);
		    			stmt.setInt(2, 0);
		    			stmt.setInt(3, 0);
		    		}
		    		stmt.execute();			        
		        
		    		ResultSet rs = stmt.getResultSet();
		    		String relText = "";
		    		while (rs.next()) {
		    			processDisplayRecord.NoteCount = rs.getInt(1);
		    		}
		    		rs.close();
		    		stmt.close();
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    
			connectionHelper.closeConnection(conn);
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(ProcessDisplayRecords);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return json;
      }   	
     
     @GET  
     @Path("/setStatus/{lib}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/{ProcessDisplayRecords}/{status}/{subproc}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String setStatus(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout, @PathParam("ProcessDisplayRecords") String ProcessDisplayRecordString,
    		 @PathParam("status") String status, @PathParam("subproc") boolean subproc) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	try
		{	
  			
    		String displayDecode = "";
    		try {
    			displayDecode = URLDecoder.decode(ProcessDisplayRecordString, "UTF-8");
    		} catch (UnsupportedEncodingException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
    		
    		ObjectMapper mapper = new ObjectMapper();
    		ArrayList<ProcessDisplayRecord> ProcessDisplayRecords = mapper.readValue(displayDecode, new TypeReference<ArrayList<ProcessDisplayRecord>>(){});
//    		ProcessDisplayRecord PDrec = new ProcessDisplayRecord();
    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "";				
		    
		    String statusNum = "";
		    switch(status){
		    	case "S": 
		    		statusNum = "0";
		    		break;
		    	case "C": 
		    		statusNum = "1";
		    		break;
		    	case "I": 
		    		statusNum = "3";
		    		break;
		    	case "H": 
		    		statusNum = "5";
		    		break;
 		    }
		    
		    for(ProcessDisplayRecord PDrec: ProcessDisplayRecords){
		    	try {
		    		query = "CALL SPMTRU09(?,?,?,?,?,?,?,?)";
		    		java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());		
		    		int MODULEID = 0;
		    		int TASKID = 0;
		    	
		    		stmt = conn.prepareCall(query);
		    		stmt.setInt(1, PDrec.TKNO);
		    		stmt.setInt(2, PDrec.TSTSKNO);
		    		stmt.setInt(3, PDrec.TSTSKSNO);
		    		stmt.setString(4, statusNum);
		    		stmt.setDate(5, timeNow);
		    		stmt.setString(6, status);
		    		stmt.setInt(7, MODULEID);
		    		stmt.setInt(8, TASKID);
		    		stmt.executeUpdate();	        
		    		stmt.close();
		    		
		    		if(subproc && PDrec.SubProc != null){
		    			query = "CALL SPPMTRU02(?,?,?,?)";
		    			stmt = conn.prepareCall(query);
		    			stmt.setInt(1, PDrec.TKNO);
			    		stmt.setInt(2, PDrec.TSTSKNO);
			    		stmt.setInt(3, PDrec.TSTSKSNO);
			    		stmt.setString(4, PDrec.SubProc);
			    		stmt.executeUpdate();	        
			    		stmt.close();
		    		}
		        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    	
		    log.info("..started task records");		    
		    
			connectionHelper.closeConnection(conn);	
			
	
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		
  		return "Success";
     }   
     
     @GET  
     @Path("/getJobFunctions/{lib}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/")  
     @Produces(MediaType.APPLICATION_JSON)
     public String getJobFunctions(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	JobFunctionRetrievalRecord JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
    	ArrayList<JobFunctionRetrievalRecord> JobFunctionRetrievalRecords = new ArrayList<JobFunctionRetrievalRecord>();
//    	Encrypt encrypt = new Encrypt();
    	
    	String json = "";
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	try
		{	
  			    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPRLJFCR00()";				
		    				    			    
		    try {
		    	
	    		stmt = conn.prepareCall(query);			        
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
		        	
		        	JobFunctionRetrievalRecord.KAJOBROLE = rs.getString("KAJOBROLE");	
		        	JobFunctionRetrievalRecord.KAJOBDESC = rs.getString("KAJOBDESC");
		        	JobFunctionRetrievalRecord.KAMANGR = rs.getString("KAMANGR");
		        	JobFunctionRetrievalRecord.GRPNAME = rs.getString("GRPNAME");
		        	
		        	JobFunctionRetrievalRecord.DisplayText = JobFunctionRetrievalRecord.KAJOBDESC;	        	
		        	
		        	JobFunctionRetrievalRecords.add(JobFunctionRetrievalRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved job function records");
		    
			connectionHelper.closeConnection(conn);	
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(JobFunctionRetrievalRecords);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		
  		return json;
     }
     
     @GET  
     @Path("/reassignProcessTask/{lib}/{user}/{ProcessDisplayRecords}/{jobfunction}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String reassignProcessTask(@PathParam("lib") String lib, @PathParam("user") String user,
    		 @PathParam("ProcessDisplayRecords") String ProcessDisplayRecordString, @PathParam("jobfunction") String jobfunction) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	try
		{	
  			
    		String displayDecode = "";
    		try {
    			displayDecode = URLDecoder.decode(ProcessDisplayRecordString, "UTF-8");
    		} catch (UnsupportedEncodingException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
    		
    		String jobfunctionDecode = "";
    		try {
    			jobfunctionDecode = URLDecoder.decode(jobfunction, "UTF-8");
    		} catch (UnsupportedEncodingException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
    		
    		ObjectMapper mapper = new ObjectMapper();
    		ArrayList<ProcessDisplayRecord> ProcessDisplayRecords = mapper.readValue(displayDecode, new TypeReference<ArrayList<ProcessDisplayRecord>>(){});
//    		ProcessDisplayRecord PDrec = new ProcessDisplayRecord();
    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "";				
		    
		    for(ProcessDisplayRecord PDrec: ProcessDisplayRecords){
		    	if(PDrec.TSTSKNO == 0){
		    		try {
		    			query = "CALL SPMPRU01(?,?,?,?,?,?,?)";	
		    			int MODULEID = 0;
		    			int TASKID = 0;
		    			String Proc = "P";
		    			
		    			stmt = conn.prepareCall(query);
		    			stmt.setString(1,Proc);
		    			stmt.setInt(2, PDrec.TKNO);
		    			stmt.setInt(3, PDrec.TSTSKNO);
		    			stmt.setInt(4, PDrec.TSTSKSNO);
		    			stmt.setString(5, jobfunctionDecode);
		    			stmt.setInt(6, MODULEID);
		    			stmt.setInt(7, TASKID);
		    			stmt.executeUpdate();	        
		    			stmt.close();	    			        
		        
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    			return "Error";
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	} else {
		    		try {
		    			query = "CALL SPMPRU01(?,?,?,?,?,?,?)";	
		    			int MODULEID = 0;
		    			int TASKID = 0;
		    			String Proc = "T";
		    			
		    			stmt = conn.prepareCall(query);
		    			stmt.setString(1,Proc);
		    			stmt.setInt(2, PDrec.TKNO);
		    			stmt.setInt(3, PDrec.TSTSKNO);
		    			stmt.setInt(4, PDrec.TSTSKSNO);
		    			stmt.setString(5, jobfunctionDecode);
		    			stmt.setInt(6, MODULEID);
		    			stmt.setInt(7, TASKID);
		    			stmt.executeUpdate();	        
		    			stmt.close();		    			        
		        
		    		} catch (SQLException e ) {
		    			log.warn(e);
		    			return "Error";
		    		} finally {
		    			if (stmt != null) { stmt.close(); }
		    		}
		    	}
		    }
		    	
		    log.info("..started task records");		    
		    
			connectionHelper.closeConnection(conn);	
			
	
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		
  		return "Success";
     }
     
     public String setStatus(String TKSTAT, java.util.Date T4BACTST, java.util.Date TKESTCMP, java.util.Date T4BACTCMP, int T4BWNDAYS, int T4BOTDAYS){
    	 
    	String FUTURE_STATUS = "4";
    	String HOLD_STATUS = "5";
        java.util.Date CURRENTDATE = new java.util.Date();  
    	 
    	if (TKSTAT.equals(FUTURE_STATUS)){					
 			return("Future");
    	}
    	if (TKSTAT.equals(HOLD_STATUS)){
    		return("Paused");
    	}
 		if (T4BACTST == null){
 			return("Not Started");
 		}
 		if (TKESTCMP == null){
 			return("On Time");
 		}
 		if (T4BACTCMP == null){
 			if (TKSTAT.equals(FUTURE_STATUS)){
 				return("Future");
 			} else {
 				int od = 0;
				try {						
					DateTime dt1 = new DateTime(CURRENTDATE);
					DateTime dt2 = new DateTime(TKESTCMP); 						
					od = Days.daysBetween(dt1, dt2).getDays();	
				} catch (Exception e){
					od = 999;
				}
 				if (CURRENTDATE.after(TKESTCMP)){
 					if (T4BWNDAYS < 0){
 						return("Behind Schedule");
 					} else {
 						if (od <= T4BOTDAYS && od > 0){
 							return("Near Due");
 						} else {
 							return("On Time");
 						}
 					}
 				} else {
 					return("On Time");
 				}
 			}
 		} else {
 			int od  = 0;
 			try {
 				DateTime dt1 = new DateTime(T4BACTCMP);
				DateTime dt2 = new DateTime(TKESTCMP); 						
				od = Days.daysBetween(dt1, dt2).getDays();	
 			} catch (Exception e){
 				od = 999;
 			}
 			if (od > T4BWNDAYS){
 				return("Completed Behind Schedule");
 			} else {
 				if (od > T4BOTDAYS) {
 					return("Completed In Danger");
 				} else {
 					return("Completed On Time");
 				}
 			}
 		}
     }
     
     @GET  
     @Path("/getEditWidget/{lib}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/{widgetSeq}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String getEditWidget(@PathParam("lib") String lib, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout,
    		 @PathParam("widgetSeq") String widgetSeq) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	JobFunctionRetrievalRecord JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
    	ArrayList<JobFunctionRetrievalRecord> JobFunctionRetrievalRecords = new ArrayList<JobFunctionRetrievalRecord>();
    	ProcessTypeRecord ProcessTypeRecord = new ProcessTypeRecord();
    	ArrayList<ProcessTypeRecord> ProcessTypeRecords = new ArrayList<ProcessTypeRecord>();
    	PRELDESC PRELDESC = new PRELDESC();
    	ArrayList<PRELDESC> PRELDESCS = new ArrayList<PRELDESC>();
    	TaskTypeRecord TaskTypeRecord = new TaskTypeRecord();
    	ArrayList<TaskTypeRecord> TaskTypeRecords = new ArrayList<TaskTypeRecord>();
    	ArrayList<GroupRecord> grpRecords = new ArrayList<GroupRecord>();
    	ArrayList<PINFORecord> pinfoRecords = new ArrayList<PINFORecord>();
    	ArrayList<UserDisplayRecord> dispRecords = new ArrayList<UserDisplayRecord>();
    	ArrayList<DefinedFieldsRecord> defFields = new ArrayList<DefinedFieldsRecord>();
    	ArrayList<UDFRecord> userdefFields = new ArrayList<UDFRecord>();
    	ArrayList<PINFORecord> frequencies = new ArrayList<PINFORecord>();
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	String json1 = "";
    	String json2 = "";
    	String json3 = "";
    	String json4 = "";
    	String json5 = "";
    	String json6 = "";
    	String json7 = "";
    	String json8 = "";
    	String json9 = "";
    	String json10 = "";
    	String jsonAll = "";
    	
    	try
		{	
  			    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SPRLJFCR00()";				
		    				    			    
		    try {
		    	JobFunctionRetrievalRecords.add(new JobFunctionRetrievalRecord());
	    		stmt = conn.prepareCall(query);			        
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	JobFunctionRetrievalRecord = new JobFunctionRetrievalRecord();
		        	
		        	JobFunctionRetrievalRecord.KAJOBROLE = rs.getString("KAJOBROLE");	
		        	JobFunctionRetrievalRecord.KAJOBDESC = rs.getString("KAJOBDESC");
		        	JobFunctionRetrievalRecord.KAMANGR = rs.getString("KAMANGR");
		        	JobFunctionRetrievalRecord.GRPNAME = rs.getString("GRPNAME");
		        	
		        	JobFunctionRetrievalRecord.DisplayText = JobFunctionRetrievalRecord.KAJOBDESC;	        	
		        	
		        	JobFunctionRetrievalRecords.add(JobFunctionRetrievalRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved job function records");
		    
	  		json1 = new Gson().toJson(JobFunctionRetrievalRecords);
	  		
	  		log.info("..calling stored procedure");
			
			stmt = null;
		    query = "CALL SPRLGPCR00()";				
		    				    			    
		    try {
		    	GroupRecord grpRecordTmp = new GroupRecord();
		    	grpRecords.add(new GroupRecord());
		    	grpRecordTmp.GRPNAME = "Use Org Chart";
		    	grpRecords.add(grpRecordTmp);
	    		stmt = conn.prepareCall(query);			        
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	GroupRecord grpRecord = new GroupRecord();
		        	
		        	grpRecord.GRPNAME = rs.getString(1).trim();	        	
		        	
		        	grpRecords.add(grpRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved group records");
		    
	  		json2 = new Gson().toJson(grpRecords);
	  		
	  		log.info("..calling stored procedure");
			
			stmt = null;
		    query = "CALL SPMPTCR02(?,?,?)";				
		    				    			    
		    try {
		    	ProcessTypeRecords.add(new ProcessTypeRecord());
	    		stmt = conn.prepareCall(query);
	    		stmt.setString(1, "");
		    	stmt.setString(2, "N");
		    	stmt.setString(3, "B");
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	ProcessTypeRecord = new ProcessTypeRecord();
		        	
		        	ProcessTypeRecord.TKTYPE = rs.getString("TKTYPE");	
		        	ProcessTypeRecord.TKTMPNM = rs.getString("TKTMPNM");      	
		        	
		        	ProcessTypeRecords.add(ProcessTypeRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved process type records");
			
	  		json3 = new Gson().toJson(ProcessTypeRecords);
	  		
	  		log.info("..calling stored procedure");
			
			stmt = null;
		    query = "CALL SPINFOCR04(?,?)";				
		    				    			    
		    try {
		    	pinfoRecords.add(new PINFORecord());
	    		stmt = conn.prepareCall(query);	
	    		stmt.setString(1, "7D");
	    		stmt.setString(2, "");
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	pinfoRecords.add(pinfoRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
			
		    log.info("..retrieved category records");
		    
	  		json4 = new Gson().toJson(pinfoRecords);
	  		
	  		log.info("..calling SP: SPRINFCR01");
		        
	  		stmt = null;
			query = "CALL SPRINFCR01()";
			    
			try {
				stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			    stmt.execute();			        
			        
			    ResultSet rs = stmt.getResultSet();
			    while (rs.next()) {
			        	PRELDESC = new PRELDESC();
			        	PRELDESC.RECID = rs.getInt(1);
			        	PRELDESC.PRDID = rs.getInt(2);
			        	PRELDESC.PRDESC = rs.getString(3).trim();
			        	PRELDESCS.add(PRELDESC);
			    }
			        
			    rs.close();
			    stmt.close();
			    
			    } catch (SQLException e ) {
			    	log.warn(e);
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
			
		    log.info("..Retrieved " + PRELDESCS.size() + " Tags Records");
	        
	        json5 = new Gson().toJson(PRELDESCS);
	        
	        log.info("..calling stored procedure");
			
			stmt = null;
		    query = "CALL SPMTTYCR00(?,?)";				
		    				    			    
		    try {
		    	TaskTypeRecords.add(new TaskTypeRecord());
	    		stmt = conn.prepareCall(query);
	    		stmt.setString(1, "");
	    		stmt.setString(2, "N");
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	TaskTypeRecord = new TaskTypeRecord();
		        	
		        	TaskTypeRecord.TSTYPE = rs.getString("TSTYPE");	
		        	TaskTypeRecord.TSTYPED = rs.getString("TSTYPED");
		        	TaskTypeRecord.TSDYCMP = rs.getInt("TSDYCMP");
		        	
		        	TaskTypeRecords.add(TaskTypeRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }			
		
		    log.info("..retrieved " + TaskTypeRecords.size() + " task type records");
			
	  		json6 = new Gson().toJson(TaskTypeRecords);
	  		
	  		log.info("..getting specific columns per widget OR all columns");
			
	  		if(widgetSeq != "0"){
	  			stmt = null;
	  			query = "CALL SPUDSPCR00(?,?)";				
		    				    			    
	  			try {
	  				dispRecords.add(new UserDisplayRecord());
	  				stmt = conn.prepareCall(query);
	  				stmt.setInt(1, Integer.parseInt(widgetSeq));
	  				stmt.setString(2, user);
	  				stmt.execute();	            			        
	        
	  				ResultSet rs = stmt.getResultSet();
	  				while (rs.next()) {
	  					UserDisplayRecord dispRecord = new UserDisplayRecord();
		        	
	  					dispRecord.DSSEQ = rs.getInt("DSSEQ");	
	  					dispRecord.DSORDER = rs.getInt("DSORDER");
	  					dispRecord.DSTYPE = rs.getString("DSTYPE");
	  					dispRecord.DSSEQNUM = rs.getInt("DSSEQNUM");
	  					dispRecord.NAMID = rs.getString("NAMID");
	  					dispRecord.WIDTH = rs.getString("WIDTH");
		        	
	  					dispRecords.add(dispRecord);
	  				}
		        
	  				rs.close();
	  				stmt.close();
			        
	  			} catch (SQLException e ) {
	  				log.warn(e);
	  				return "Error";
	  			} finally {
	  				if (stmt != null) { stmt.close(); }
	  			}
	  			
	  			log.info("..retrieved " + dispRecords.size() + " user display records");
	  			
	  	  		json7 = new Gson().toJson(dispRecords);
	  		}
	  		
	  		stmt = null;
  			query = "CALL SPDFFLCR00()";				
	    				    			    
  			try {
//s  				dispRecords.add(new UserDisplayRecord());
  				stmt = conn.prepareCall(query);
  				stmt.execute();	            			        
        
  				ResultSet rs = stmt.getResultSet();
  				while (rs.next()) {
  					DefinedFieldsRecord DefFieldRec = new DefinedFieldsRecord();
	        	
  					DefFieldRec.DFSEQ = rs.getInt("DFSEQ");	
  					DefFieldRec.DFNAMID = rs.getString("DFNAMID");
  					DefFieldRec.DFDISP = rs.getString("DFDISP");
  					DefFieldRec.DFREQ = rs.getString("DFREQ");
  					DefFieldRec.DFWIDTH = rs.getString("DFWIDTH");
  					log.info(DefFieldRec.DFNAMID);
  					defFields.add(DefFieldRec);
  				}
	        
  				rs.close();
  				stmt.close();
		        
  			} catch (SQLException e ) {
  				log.warn(e);
  				return "Error";
  			} finally {
  				if (stmt != null) { stmt.close(); }
  			}
  			
  			log.info("..retrieved " + defFields.size() + " process defined records");
  			
  	  		json8 = new Gson().toJson(defFields);
  	  		
  	  		//TO DO: Get user defined ones
  	  		stmt = null;
			query = "CALL SPUDFCR00()";				
    				    			    
			try {
//s  				dispRecords.add(new UserDisplayRecord());
				stmt = conn.prepareCall(query);
				stmt.execute();	            			        
    
				ResultSet rs = stmt.getResultSet();
				while (rs.next()) {
					UDFRecord UDFieldRec = new UDFRecord();
        	
					UDFieldRec.USEQ = rs.getInt("USEQ");	
					UDFieldRec.UNAMID = rs.getString("UNAMID").trim();
					UDFieldRec.UTYPE = rs.getString("UTYPE").trim();
					UDFieldRec.UDISP = rs.getString("UDISP");
					UDFieldRec.UREQ = rs.getString("UREQ");
					UDFieldRec.UWIDTH = rs.getString("UWIDTH");
        	
					userdefFields.add(UDFieldRec);
				}
        
				rs.close();
				stmt.close();
	        
			} catch (SQLException e ) {
				log.warn(e);
				return "Error";
			} finally {
				if (stmt != null) { stmt.close(); }
			}
			
			log.info("..retrieved " + userdefFields.size() + " user defined records");
			
	  		json9 = new Gson().toJson(userdefFields);
	  		
	  		stmt = null;
		    query = "CALL SPINFOCR04(?,?)";				
		    				    			    
		    try {
		    	pinfoRecords.add(new PINFORecord());
	    		stmt = conn.prepareCall(query);	
	    		stmt.setString(1, "T3");
	    		stmt.setString(2, "");
		        stmt.execute();	            			        
	        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PINFORecord pinfoRecord = new PINFORecord();
		        	
		        	pinfoRecord.SIFTID = rs.getString("SIFTID").trim();
		        	pinfoRecord.SIFCD = rs.getString("SIFCD").trim();
		        	pinfoRecord.SIFSD = rs.getString("SIFSD").trim();
		        	pinfoRecord.SIFFUL = rs.getString("SIFFUL").trim();
		        	
		        	frequencies.add(pinfoRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    	return "Error";
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }

		    json10 = new Gson().toJson(frequencies);
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}
    	
    	connectionHelper.closeConnection(conn);	
  		
    	jsonAll = "["+json1+","+json2+","+json3+","+json4+","+json5+","+json6+"," +json7+","+json8+","+json9+","+json10+"]";

  		return jsonAll;
     }
     
     @POST
	 @Path("/createProcessTask")
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public String createProcessTask(String masterString){
	    	 
		CONNECTOR CONNECTOR = new CONNECTOR(); 
	   	Connection conn = null;
	   	
	   	ProcessCreateRecord ProcessCreateRecord = new ProcessCreateRecord();
	   	ArrayList<UDFData> UDFs = new ArrayList<UDFData>();
    	ArrayList<PRELDESC> PRELDESCs = new ArrayList<PRELDESC>();
	   		
	   	log.info("..mapping JSON string");
	   		
	   	JsonElement jelement = new JsonParser().parse(masterString);
	   	JsonObject  jobject = jelement.getAsJsonObject();
	   	String lib = jobject.get("lib").toString();
	   	lib = lib.substring(1, lib.length()-1);
	   	String user = jobject.get("userid").toString();
	   	user = user.substring(1, user.length()-1);
	   	String createNewString = jobject.get("process").toString();
	   	String relatedStrings = jobject.get("tags").toString();
	   	String udfStrings = jobject.get("udfs").toString();

    	
    	int offset = DateTimeZone.forID("US/Eastern").getOffset(new DateTime());
    	
    	String json = "";
    	
    	try{	
    		
    		ObjectMapper mapper = new ObjectMapper();
    		
    		ProcessCreateRecord = mapper.readValue(createNewString, new TypeReference<ProcessCreateRecord>(){});
    		
    		try {
    			PRELDESCs = mapper.readValue(relatedStrings, new TypeReference<ArrayList<PRELDESC>>(){});
    		} catch (JsonParseException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		} catch (JsonMappingException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		} catch (IOException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    		
    		try {
    			UDFs = mapper.readValue(udfStrings, new TypeReference<ArrayList<UDFData>>(){});
    		} catch (JsonParseException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		} catch (JsonMappingException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		} catch (IOException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    		
    		try {
    			ProcessCreateRecord.Title = URLDecoder.decode(ProcessCreateRecord.Title, "UTF-8");
    			log.info(ProcessCreateRecord.Title);
//    			masterRecord.noteText = masterRecord.noteText.substring(1, masterRecord.noteText.length()-1);
    		} catch (UnsupportedEncodingException e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}
    		
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			int TKNO_out = 0;
			int TSTKNO_out = 0;
			int TSTSKNO_out = 0;
			
			CallableStatement stmt = null;
		    String query = "";				
		    log.info(ProcessCreateRecord.PorT);
		    java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		    if(ProcessCreateRecord.PorT.equals("P")){
		    	log.info("...Process");
		    	try {
		    		query = "CALL SPPMPRU02(?,?,?,?,?,?,?,?,?,?,?,?)";
		    		DateTime dtS = ProcessCreateRecord.Start.minusMillis(offset);
		    		java.sql.Date StartD = new java.sql.Date(dtS.getMillis());
		    		int MODULEID = 0;
		    		int TASKID = 0;
		    	
		    		stmt = conn.prepareCall(query);
		    		stmt.registerOutParameter(11, Types.VARCHAR);
		    		stmt.registerOutParameter(12, Types.INTEGER);
		    		stmt.setString(1, ProcessCreateRecord.Type);
		    		stmt.setString(2, ProcessCreateRecord.Title);
		    		stmt.setDate(3, timeNow);
		    		stmt.setDate(4, StartD);
		    		stmt.setString(5, ProcessCreateRecord.Assigned);
		    		stmt.setString(6,"Y");
		    		stmt.setString(7,"");
		    		stmt.setString(8,"");
		    		stmt.setInt(9, MODULEID);
		    		stmt.setInt(10, TASKID);
		    		stmt.executeUpdate();   
		    		
		    		TKNO_out = stmt.getInt(12);
		    		stmt.close();	    			        
		        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    } else {
		    	log.info("...Task");
		    	try {
		    		query = "CALL SPPMTRU01(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		    		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		    		DateTime checker = formatter.parseDateTime("01/01/1900");
		    		String checkerComp = formatter.print(checker);
		    		DateTime dtS = ProcessCreateRecord.Start.minusMillis(offset);
		    		java.sql.Date StartD = new java.sql.Date(dtS.getMillis());
		    		java.sql.Date StartE =  null;
		    		java.sql.Date StartR = null;
		    		if(ProcessCreateRecord.Due != null){
		    			if(formatter.print(ProcessCreateRecord.Due).equals(checkerComp)){
		    				// Do Nothing
		    			} else{
		    				DateTime dtE = ProcessCreateRecord.Due.minusMillis(offset);
		    				StartE = new java.sql.Date(dtE.getMillis());
		    			}
		    		}
		    		if(ProcessCreateRecord.Remind != null){
		    			if(formatter.print(ProcessCreateRecord.Remind).equals(checkerComp)){
		    				// Do Nothing
		    			} else{
		    				DateTime dtR = ProcessCreateRecord.Remind.minusMillis(offset);
		    				StartR = new java.sql.Date(dtR.getMillis());
		    			}
		    		}
		    		int MODULEID = 0;
		    		int TASKID = 0;
		    		Timestamp sysupd = new Timestamp(timeNow.getTime());
		    		String syscrtby = user;
		    		String pgmcrtby = "SPPMTRU01";
		    	
		    		stmt = conn.prepareCall(query);
		    		stmt.registerOutParameter(12, Types.VARCHAR);
		    		stmt.registerOutParameter(13, Types.INTEGER);
		    		stmt.registerOutParameter(14, Types.INTEGER);
		    		stmt.registerOutParameter(15, Types.INTEGER);
		    		stmt.setString(1, ProcessCreateRecord.Type);
		    		stmt.setString(2, ProcessCreateRecord.Title);
		    		stmt.setDate(3, timeNow);
		    		stmt.setDate(4, StartD);
		    		stmt.setDate(5, StartE);
		    		stmt.setDate(6, StartR);
		    		stmt.setString(7, ProcessCreateRecord.Assigned);
		    		stmt.setString(8, "Y");
		    		stmt.setTimestamp(9, sysupd);
		    		stmt.setString(10, syscrtby);
		    		stmt.setString(11, pgmcrtby);
		    		stmt.executeUpdate();
		    		
		    		TKNO_out = stmt.getInt(13);
		    		TSTKNO_out = stmt.getInt(14);
		    		TSTSKNO_out = stmt.getInt(15);
		    		stmt.close();	    			        
		        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
		    }
		    	
		    log.info("..created Process Or Task");
		    
		    String relTo = "T";
		    for(PRELDESC PRELDESC: PRELDESCs) {
		    	try {
		    		query = "CALL SPRELRU00(?,?,?,?,?,?,?)";
	    	
		    		stmt = conn.prepareCall(query);
		    		stmt.registerOutParameter(7, Types.VARCHAR);
		    		stmt.setInt(1, TKNO_out);
		    		stmt.setInt(2, TSTKNO_out);
		    		stmt.setInt(3, TSTSKNO_out);
		    		stmt.setString(4, relTo);
		    		stmt.setInt(5, PRELDESC.RECID);
		    		stmt.setInt(6, PRELDESC.PRDID);
		    		stmt.executeUpdate();
		    		stmt.close();	    			        
	        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
			}
		    
		    for(UDFData UDF: UDFs) {
		    	try {
		    		query = "CALL SPUDFDRU00(?,?,?,?,?,?)";
	    	
		    		stmt = conn.prepareCall(query);
		    		stmt.setInt(1, TKNO_out);
		    		stmt.setInt(2, TSTKNO_out);
		    		stmt.setInt(3, TSTSKNO_out);
		    		stmt.setInt(4, UDF.DSEQ);
		    		stmt.setString(5, UDF.DATA);
		    		stmt.setString(6, "I");
		    		stmt.executeUpdate();
		    		stmt.close();	    			        
	        
		    	} catch (SQLException e ) {
		    		log.warn(e);
		    		return "Error";
		    	} finally {
		    		if (stmt != null) { stmt.close(); }
		    	}
			}
			connectionHelper.closeConnection(conn);	
			int[] listOut = {TKNO_out,TSTKNO_out,TSTSKNO_out};
			json = new Gson().toJson(listOut);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
		}  		  		
  		return json;
     } 
}  
