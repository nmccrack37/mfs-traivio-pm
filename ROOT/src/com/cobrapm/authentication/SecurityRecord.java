package com.cobrapm.authentication;

public class SecurityRecord {
	public String ADM;
	public String PROC;
	public String PNP;
	public String PNT;
	public String PPTM;
	public String PTTM;
	public String PSI;
	public String PPC;
	public String PEPM;
	public String PREA;
	public String NOTE;
	public String NOTM;
	public String FORM;
	public String FRMM;
	
	
	public SecurityRecord(String ADMin, 
		    String PROCin,
		    String PNPin,
		    String PNTin,
		    String PPTMin,
		    String PTTMin,
		    String PSIin,
		    String PPCin,
		    String PEPMin,
		    String PREAin,
		    String NOTEin,
		    String NOTMin,
		    String FORMin,
		    String FRMMin){
		ADM = ADMin;
		PROC = PROCin;
		PNP = PNPin;
		PNT = PNTin;
		PPTM = PPTMin;
		PTTM = PTTMin;
		PSI = PSIin;
		PPC = PPCin;
		PEPM = PEPMin;
		PREA = PREAin;
		NOTE = NOTEin;
		NOTM = NOTMin;
		FORM = FORMin;
		FRMM = FRMMin;
		}

	public SecurityRecord() {
			ADM = "";
			PROC = "";
			PNP = "";
			PNT = "";
			PPTM = "";
			PTTM = "";
			PSI = "";
			PPC = "";
			PEPM = "";
			PREA = "";
			NOTE = "";
			NOTM = "";
			FORM = "";
			FRMM = "";
	}
}