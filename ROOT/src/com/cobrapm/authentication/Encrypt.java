package com.cobrapm.authentication;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;

import com.cobrapm.services.AuthorizationService;



public class Encrypt {
	private static Logger log = Logger.getLogger(AuthorizationService.class.getName());
	
	public String PasswordEncryption(String plaintext) {
		String key = null;
		try {
			MessageDigest m = MessageDigest.getInstance("SHA-256");
			m.reset();
			m.update(plaintext.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			key = bigInt.toString(16);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return key;
	}
	
	public String encryptLibrary(String lib) {
		String key = "B3llvu3506101403";
		String output = "";
		
		Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(lib.getBytes());
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.newStringUtf8(Base64.encodeBase64URLSafe(encrypted)));
			output = sb.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return output;
		
	}
	
	public String decryptLibrary(String libEncrypted) {
		String key = "B3llvu3506101403";
		String libDecrypted = "";
		
		Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			byte[] decrypted = Base64.decodeBase64(libEncrypted);
			libDecrypted = new String(cipher.doFinal(decrypted));
//			output = sb.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return libDecrypted;
		
	}
}
