package com.cobrapm.authentication;


public class Constants {
	public String APPLICATION_VERSION = "173.21.5 2-April-2014";
	
  	public String RESOURCE_BUNDLE_NAME = "application";
  	public String DEFAULT_SYSTEM_NAME = "default.system.name";
  	public String SESSION_TIMEOUT = "portal.session.timeout";
  	public String CUSTOMER_ID = "customer.id";
  	public String TP_FLAG = "tp.flag";
	public String MULTILIBRARY_SEARCH = "multilibrary.search.flag";
	
	public String PA_CONTEXT_ROOT = "pa.contextroot";
	public String BUDGET_CONTEXT_ROOT = "budget.contextroot";
	public String MT_CONTEXT_ROOT = "mt.contextroot";
	public String FS_CONTEXT_ROOT = "fs.contextroot";	
	
	public String CONNECTION_POOL_USER 	= "connection.pool.user.";
	public String DATA_LIBRARIES = "data.libraries";
	public String J2C_ALIAS 			= "j2c.alias.";
	
		
	public String TEMPLATE_NAME = "template.name";
	public String TEMPLATE_LOAN_DETAIL_NAME = "templateLoanDetail.name";
	public String TEMPLATE_NAME_DETAIL_NAME = "templateNameDetail.name";
	public String TEMPLATE_TOP_EXPOSURES_NAME = "templateTopExposures.name";
	public String TEMPLATE_MCA_DETAIL_NAME = "templateMCADetail.name";
	public String TEMPLATE_ALERT_MESSAGES_NAME = "templateAlertMessage.name"; 
	public String TEMPLATE_ANALYZE_NAME = "templateAnalyze.name";

	public String NOTES_CONTEXT_ROOT = "notes.contextroot";
	
	public String STATEMENTS_CONTEXT_ROOT = "statements.contextroot";
}
