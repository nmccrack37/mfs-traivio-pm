package com.cobrapm.authentication;

import java.sql.Connection;


public class CONNECTOR {
	public Connection conn;
	public String JOB;
	public String USER;
    public String NBR;	
	
	public CONNECTOR(Connection connin,
					String JOBin,
					String USERin,
					String NBRin){
		conn = connin;
		JOB = JOBin;
		USER = USERin;
		NBR = NBRin;
	}

	public CONNECTOR() {
		conn = null;
		JOB = "";
		USER = "";
		NBR = "";
	}
}
