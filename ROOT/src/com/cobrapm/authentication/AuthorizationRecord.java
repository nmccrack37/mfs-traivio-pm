package com.cobrapm.authentication;


public class AuthorizationRecord {
	public boolean isError;
	public UserRecord userRecord;
	public String returnMessage;
	public String returnErrorMessage;
}