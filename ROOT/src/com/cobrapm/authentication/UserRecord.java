package com.cobrapm.authentication;

import java.util.ArrayList;

public class UserRecord {
	public String userid;
	public String datasource;
	public String defaultConnectedSystem;
	public String datasourceConnectedSystem;
	public String groupID;
	public double sessionID;
	public int sessionToken;
	public double sessionTimeout;
	public String applicationVersion;
	public SecurityRecord taskRights;
	public String fullFileSystemPath;			// file system path to app, i.e., c:/../../../StrategyWS/
	public String appURL; 						// URL, i.e., http://localhost:8080/StrategyWS/
	public String appContextPath;				// full context path, /StrategyWS (used to substring document.location to get appURL)
	public String jobfunction;
	public String jobfunctiondescription;
	public String firstname;
	public String lastname;
	public String queue;
	public String sessionTrack;
}