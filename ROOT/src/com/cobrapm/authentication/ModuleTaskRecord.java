package com.cobrapm.authentication;

public class ModuleTaskRecord {
	public int MODULEID;
	public int TASKID;
	public int PARENTID;
	public String TASKNAME;
	public int DEFRIGHTS;
	public int USERRIGHTS;
	public int VALIDMASK;
	public String LOGINID;
	public int OLDUSERRIGHTS;
}
