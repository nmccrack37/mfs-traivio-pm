package com.cobrapm.authentication;

import java.beans.PropertyVetoException;
import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;


public class AS400Authentication {
	static AS400 as400Object;
	static AS400 fileAccessObject;

	public static void setSystemName(String systemName) {
		as400Object = new AS400(systemName);
	}

	public static String authenticate(String username, String password) {
		boolean returnValue = false;
		String returnMessage = null;
		try {
			returnValue = as400Object.authenticate(username, password);		
			
		} catch (AS400SecurityException ex1) {
			returnMessage = ex1.getLocalizedMessage();
			
		} catch (IOException ex2) {
			returnMessage = ex2.getLocalizedMessage();
		}
		if (returnValue) {
			returnMessage = "OK";
		}
		return returnMessage;
	}

	public static String getUserId() {
		return as400Object.getUserId();
	}
	
	public static void accessFileSystem(String systemName, String username, String password){
		fileAccessObject = new AS400(systemName, username, password);
		try {
			fileAccessObject.connectService(AS400.FILE);
		} catch (AS400SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public static String changePassword(String username, String oldpassword, String newpassword){
        String returnMessage = null;
        try {
              as400Object.setUserId(username);
              as400Object.changePassword(oldpassword, newpassword);
        } catch (AS400SecurityException ex1) {
              returnMessage = ex1.getLocalizedMessage();
        } catch (IOException ex2) {
              returnMessage = ex2.getLocalizedMessage();
        } catch (PropertyVetoException ex3) {
              returnMessage = ex3.getLocalizedMessage();
        }
        if(returnMessage != null && returnMessage.length()> 0){
              return(returnMessage);
        }
        return "SUCCESS";
  }

}