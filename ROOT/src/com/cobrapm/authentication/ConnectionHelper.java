package com.cobrapm.authentication;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class ConnectionHelper {
	
	static Properties props;
	static Connection conn = null;
	private static Logger log = Logger.getLogger(ConnectionHelper.class.getName());
	public static final String DATASOURCE_CONTEXT = "java:comp/env/PMCOMMON";
	 
	public CONNECTOR getConnection(String lib) throws SQLException, ClassNotFoundException{
		try
		{		
			CONNECTOR CONNECTOR = new CONNECTOR();
			String datasource2 = "PMCOMMON";
			
			if(lib != "PMCOMMON"){
				Encrypt encrypt = new Encrypt();
				lib = encrypt.decryptLibrary(lib);
			}
				
			log.info("...Getting connection to " + DATASOURCE_CONTEXT);
			
		    Connection result = null;
		    try {
		      Context initialContext = new InitialContext();
		      DataSource datasource = (DataSource)initialContext.lookup(DATASOURCE_CONTEXT);
		      if (datasource != null) {		    	
		        result = datasource.getConnection();
		      }
		      else {
		    	  log.info("Failed to lookup datasource.");
		      }
		    }
		    catch ( NamingException ex ) {
		    	log.info("Cannot get connection: " + ex);
		    }
		    catch(SQLException ex){
		    	log.info("Cannot get connection: " + ex);
		    }
		    
		    log.info("...Got connection"); 
		    
		    if(lib == null){
		    	lib = datasource2;
			}
		        
		    conn = result;
		    
		    CallableStatement stmt = null;
		    String query = "CALL SQINITRR05(?,?,?,?)"; 
		   
		    try {
		        stmt = conn.prepareCall(query);
		        stmt.registerOutParameter(2, Types.VARCHAR);
		        stmt.registerOutParameter(3, Types.VARCHAR);
		        stmt.registerOutParameter(4, Types.VARCHAR);
		        stmt.setString(1, lib);	  
		        stmt.setString(2, CONNECTOR.JOB);	 
		        stmt.setString(3, CONNECTOR.NBR);	 
		        stmt.setString(4, CONNECTOR.USER);	 
		        stmt.execute();	
		        
		        CONNECTOR.JOB = stmt.getString(2);
		        CONNECTOR.NBR = stmt.getString(3);
		        CONNECTOR.USER = stmt.getString(4);
		        
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }   
		    
		    log.info("...JOB(" + CONNECTOR.NBR + "/" + CONNECTOR.USER + "/" + CONNECTOR.JOB +
                    ") connected");
		    
		    CONNECTOR.conn = conn;
			return CONNECTOR;
		}
		catch(Exception ex)
		{
			log.warn(ex);
			return(null);
		}
		
	}
	
	public boolean closeConnection(Connection conn){
		try
		{			
			log.info("...Closing Connection");	
			conn.close();
			log.info("...Connection Closed");
			return(true);
		}
		catch(Exception ex)
		{
			return(false);
		}
		
	}
}
