package com.cobrapm.notes;

import java.util.ArrayList;

import org.codehaus.jackson.type.TypeReference;


public class DeleteNoteRecord {
	public String lib;
	public String userid;
	public int noteID;
	
	public DeleteNoteRecord(	String libin,
								String useridin,
								int noteIDin){
		
		lib = libin;
		userid = useridin;
		noteID = noteIDin;
	}

	public DeleteNoteRecord() {
		lib = "";
		userid = "";
		noteID = 0;
	}
}
