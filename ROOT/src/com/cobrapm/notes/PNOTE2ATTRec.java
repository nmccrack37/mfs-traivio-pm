package com.cobrapm.notes;



public class PNOTE2ATTRec {
	
	public int NANOTEID;
	public int NAID;
	public String NAPATH;
	public String NAFILENAME;
	public String NAFILENAUN;
	public String NAIMAGEPATH;
	
	public PNOTE2ATTRec(int NANOTEIDin, 
				    int NAIDin,
				    String NAPATHin,
				    String NAFILENAMEin,
				    String NAFILENAUNin){
		NANOTEID = NANOTEIDin;
		NAID = NAIDin;
		NAPATH = NAPATHin;
		NAFILENAME = NAFILENAMEin;
		NAFILENAUN = NAFILENAUNin;
	}

	public PNOTE2ATTRec() {
		NANOTEID = 0;
		NAID = 0;
		NAPATH = "";
		NAFILENAME = "";
		NAFILENAUN = "";
	}
}
