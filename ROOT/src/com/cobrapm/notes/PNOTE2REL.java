package com.cobrapm.notes;


public class PNOTE2REL {
	
	public int N5ID;
	public int RECID;
	public int PRDID;
	public String DESC;
	
	public PNOTE2REL(int N5IDin,
					int RECIDin, 
				    int PRDIDin,
				    String DESCin){
		N5ID = N5IDin;
		RECID = RECIDin;
		PRDID = PRDIDin;
		DESC = DESCin;
	}

	public PNOTE2REL() {
		N5ID = 0;
		RECID = 0;
		PRDID = 0;
		DESC = "";
	}
}
