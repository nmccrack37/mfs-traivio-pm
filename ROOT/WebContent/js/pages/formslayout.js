angular.module('cobraApp').controller('FormLayoutCtrl',
function ($scope, $filter, $sessionStorage, $state, $rootScope, $http, $uibModal, $parse) {
	
	$scope.props = [];
	$scope.storedProps = [];
	$scope.layouts = [];
	$scope.colnum = 0;
	$scope.usedProps = [];
	
	var modalInstanceFL = null;
	
	$scope.init = function(){
		modalInstanceFL = null;
		$http({ 
			url:"./rest/FormLayoutService/retrieveFormData",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "formid":$sessionStorage.formid},
			headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.props = response.data[0];
				$scope.storedProps = angular.copy($scope.props);
				$scope.layouts = response.data[1];
				for(var i = 0; i < $scope.layouts.length; i++){
					for(var k = 0; k < $scope.layouts[i].formLayoutDetails.length; k++){
						var tmp1 = $scope.layouts[i].SectionSeq.toString();
						var tmp2 = ($scope.layouts[i].formLayoutDetails[k].ColumnSeq-1).toString();
						var concat = tmp1+"usedProps"+tmp2;
						var j = $scope.props.length;
						while(j--){
							if($scope.layouts[i].formLayoutDetails[k].PropSeq == $scope.props[j].FPSEQ){
								if($scope[concat] === undefined){
									$scope[concat] = [$scope.props[j]];
								} else {
									$scope[concat].push($scope.props[j]);
								}
								$scope.props.splice(j,1);
							}
						}
					}
				}
			});
	};
	
	$scope.reset = function(){
		for(var i = 0; i < $scope.layouts.length; i++){
			for(var k = 0; k < $scope.layouts[i].formLayoutDetails.length; k++){
				var tmp1 = $scope.layouts[i].SectionSeq.toString();
				var tmp2 = ($scope.layouts[i].formLayoutDetails[k].ColumnSeq-1).toString();
				var concat = tmp1+"usedProps"+tmp2;
				if($scope[concat] !== undefined){
					$scope[concat] = [];
				}
			}
		}
		$scope.layouts = [];
		$scope.props = $scope.storedProps;
	}
	
	$scope.addSection = function(){
		modalInstanceFL = $uibModal.open({
			  animation: true,
		      templateUrl: './pages/modals/columnsModal.html',
		      controller: 'ColumnModalCtrl',
		      resolve: {}
		    });
		modalInstanceFL.result.then(function(answer){
			  $scope.colnum = answer;
			  $scope.newLayout = {
					  SectionSeq: $scope.layouts.length+1,
					  Columns:answer,
					  formLayoutDetails: []
			  };
			  $scope.layouts.push($scope.newLayout);
		  });
	};
	
	$scope.getColNum =  function(number){
		return new Array(number);
	};
	
	$scope.getLayouts = function(SeqNum, colNum){
		var concat = SeqNum+"usedProps"+colNum;
		if($scope[concat] === undefined || $scope[concat].length == 0){
			$scope[concat] = [{FPID: 0,
				FPSEQ: 0,
				FPNAME: " ",
				FPTYPE: "",
				FPREQ: "N"}];
		}
		return $scope[concat];
	};
	
	$scope.submit = function(){
		for(var i = 0; i < $scope.layouts.length; i++){
			$scope.tmpLayoutArray = [];
			for(var k = 0; k < $scope.layouts[i].Columns; k++){
				var intCollection = $scope[$scope.layouts[i].SectionSeq+"usedProps"+k];
				for(var j = 0; j < intCollection.length; j++){
//					var found = false;
					if(intCollection[j].FPSEQ != 0){
//						if($scope.layouts[i].formLayoutDetails.length > 0){
//							for(var l = 0; l < $scope.layouts[i].formLayoutDetails.length; l++){
//								if($scope.layouts[i].formLayoutDetails[l].ColumnSeq == k+1 &&
//										$scope.layouts[i].formLayoutDetails[l].LayoutSeq == j+1){
//										found = true;
//										$scope.layouts[i].formLayoutDetails[l].PropSeq = intCollection[j].FPSEQ;
//										break;
//									}
//								}
//							}
//						if(!found){
							$scope.layoutDetail = {
								ColumnSeq:k+1,
								LayoutSeq:j+1,
								PropSeq:intCollection[j].FPSEQ
							}
							$scope.tmpLayoutArray.push($scope.layoutDetail);
//							$scope.layouts[i].formLayoutDetails.push($scope.layoutDetail);
					
//						}
					}
				}
			}
			$scope.layouts[i].formLayoutDetails = $scope.tmpLayoutArray;
		}
		$http({ 
			url:"./rest/FormLayoutService/updateFormData",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "formid":$sessionStorage.formid, "layout":$scope.layouts},
			headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$rootScope.isFormSubmit = false;
				$sessionStorage.formid = 0;
				$sessionStorage.$save();
				alert("Thank You. Form Layout Submitted Sucessfully.");
				$state.go('forms');
			});
	};
	
	$scope.init();
}).controller('ColumnModalCtrl', function ($scope, $filter, $modalInstance) {
	
	$scope.colnum = 0;
	
	$scope.setColumn = function(num){
		$scope.colnum = num;
		$("#column"+num).attr("src","./images/formslayout/column"+num+"B.png");
		for(var i = 1; i < 4; i++){
			if(i != num){
				$("#column"+i).attr("src","./images/formslayout/column"+i+".png");
			}
		}
	}
	
	$scope.ok = function(){
		$modalInstance.close($scope.colnum);
	}
	
	$scope.cancel = function(){
		$modalInstance.dismiss('cancel');
	}
	
	
});