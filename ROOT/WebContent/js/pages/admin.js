
angular.module('cobraApp').controller('AdminCtrl',
function ($scope, $filter, $location, $sessionStorage, $rootScope, $http, $state) {
	
	$scope.groups = {
			GRPNAME: "",
			GRPMEMBERs: [],
			selected: false,
			origName: ""
	};
	
	$scope.users = {
			KAJOBROLE: "",
			KAJOBDESC: "",
			GRPNAME: "",
			taskRights: "",
			selected: false,
			isAdmin : false,
			isProcess : false,
			isNotes : false,
			isForms : false
	};
	
	$scope.group = {};
	$scope.userDescs = [];
	$scope.fields = [];
	$scope.ufields = [];
	
	$scope.security = [{SECOPT: "0", SECNAME: "Read-Only"},
	                   {SECOPT: "1", SECNAME: "Read/Write"},
	                   {SECOPT: "2", SECNAME: "Administrator"}];
	
	$scope.securitySelect = {};
	$scope.groupSelect = {};
	
	$scope.user = "";
	$scope.fname = "";
	$scope.lname = "";
	
	$scope.isEditing = false;
	
	$scope.init = function () {
		$scope.userDescs = [];
		$.getJSON( "./rest/AdminService/loadGroups/" + $sessionStorage.datasource, function( data ) {
			$.each( data, function( key, val ) {
				if(key == 0){
					$scope.groups = val;
				} else {
					$scope.users = val;
					
					for(var i = 0; i < $scope.users.length; i++){
						if($scope.users[i].taskRights.ADM == "3"){
							$scope.users[i].isAdmin = true;
						} else {
							$scope.users[i].isAdmin = false;
						}
						if($scope.users[i].taskRights.PROC == "3"){
							$scope.users[i].isProcess = true;
						} else {
							$scope.users[i].isProcess = false;
						}
						if($scope.users[i].taskRights.NOTE == "3"){
							$scope.users[i].isNotes = true;
						} else {
							$scope.users[i].isNotes = false;
						}
						if($scope.users[i].taskRights.FORM == "3"){
							$scope.users[i].isForms = true;
						} else {
							$scope.users[i].isForms = false;
						}
					}
				}
			});

			for(var i = 0; i < $scope.users.length; i++){
				$scope.userDescs.push($scope.users[i].KAJOBDESC);
			}
			$scope.$apply();
    	});
		$scope.isEditing = false;
		setTimeout(function(){
			$scope.loadRequired();
		}, 500);
	};
	
	$scope.loadRequired = function(){
		$http({ 
    		url:"./rest/AdminService/getRequiredFields",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( response ) {
    		$scope.fields = response.data[0];
    		$scope.ufields = response.data[1];
    		for(var i = 0; i < $scope.fields.length; i++){
    			if($scope.fields[i].DFREQ == "Y"){
    				$scope.fields[i].checked = true;
    			} else {
    				$scope.fields[i].checked = false;
    			}
    		}
    		for(var i = 0; i < $scope.ufields.length; i++){
    			if($scope.ufields[i].UREQ == "Y"){
    				$scope.ufields[i].checked = true;
    			} else {
    				$scope.ufields[i].checked = false;
    			}
    		}
    	});
	};
	
	$scope.showSecurity = function(user) {
		$scope.isEditing = true;
		if(user.selected){
			$scope.user = user;
		}
	};
	
	$scope.showGroupMembers = function(grp){
		if(grp.selected){
			$("#groupsTags").tagit("removeAll");
			$scope.group = grp;
		
			$scope.isEditing = true;
			
			setTimeout(function(){
				$("#groupsTags").tagit({
					availableTags: $scope.userDescs,
					onlyAvailableTags: true
				});
				for(var i = 0; i < $scope.group.GRPMEMBERs.length; i++){
					$("#groupsTags").tagit("createTag", $scope.group.GRPMEMBERs[i]);
				}
			},300);
		}
	};
	
	$scope.addGroups = function() {
		$("#groupsTags").tagit("removeAll");
		$scope.group = {GRPNAME: "",
				GRPMEMBERs: [],
				selected: false,
				origName: ""};
		$scope.isEditing = true;
		
		setTimeout(function(){
			$("#groupsTags").tagit({
				availableTags: $scope.userDescs,
				onlyAvailableTags: true
			});
		},300);
	};
	
	$scope.submitSecurity = function() {
		for(var i = 0; i < $scope.users.length; i++){
			if($scope.users[i].isAdmin == true){
				$scope.users[i].taskRights.ADM = "3";
			} else {
				$scope.users[i].taskRights.ADM = "0";
			}
			if($scope.users[i].isProcess == true){
				$scope.users[i].taskRights.PROC = "3";
			} else {
				$scope.users[i].taskRights.PROC = "0";
			}
			if($scope.users[i].isNotes == true){
				$scope.users[i].taskRights.NOTE = "3";
			} else {
				$scope.users[i].taskRights.NOTE = "0";
			}
			if($scope.users[i].isForms == true){
				$scope.users[i].taskRights.FORM = "3";
			} else {
				$scope.users[i].taskRights.FORM = "0";
			}
		}
		
		var JSONResponse = JSON.stringify($scope.users);
		
		$http({ 
    		url:"./rest/AdminService/updateSecurity",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "users":$scope.users},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		$scope.isEditing = false;
    		$state.go('administration.security');
    		$scope.init();
    	});
	};
	
	$scope.submitGroups = function(){
		$scope.group.GRPMEMBERs = [];
		var currentTags = $("#groupsTags").tagit("assignedTags");
		for(var i = 0; i < currentTags.length; i++){
			for(var k = 0; k < $scope.users.length; k ++){
				if(currentTags[i] == $scope.users[k].KAJOBDESC){
					$scope.group.GRPMEMBERs.push($scope.users[k].KAJOBROLE);
				}
			}
		}
		
		$http({ 
    		url:"./rest/AdminService/updateGroups",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "group":$scope.group},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		$scope.isEditing = false;
    		$state.go('administration.groups');
    		$scope.init();
    	});
		
	};
	
	$scope.submitNewUser = function(){
		var group = "";
		if($scope.groupSelect.value == undefined){
			group = "";
		} else {
			group = $scope.groupSelect.value.GRPNAME;
		}
		$http({ 
    		url:"./rest/HtmlEmailSender/sendUserEmail",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "sender":$sessionStorage.jobfunctiondescription, "email":$scope.email, 
    				"group":group, "security":$scope.securitySelect.value.SECOPT},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		$scope.isEditing = false;
    	});
	};
	
	
	$scope.notifyMe = function() {
		// Let's check if the browser supports notifications
		if (!("Notification" in window)) {
			alert("This browser does not support desktop notification");
		} else if (Notification.permission === "granted") {
			// If it's okay let's create a notification
			var title = "User Added";
			var options = {
				      body: "User Added Successfully",
				      icon: "./images/justTheO.png"
				  }
		    var notification = new Notification(title, options);
		} else if (Notification.permission !== 'denied') {
			Notification.requestPermission(function (permission) {
				// If the user accepts, let's create a notification
				if (permission === "granted") {
					var notification = new Notification("Hi there!");
				}
			});
		}
	};
	
	$scope.submitRequired = function(){
		for(var i = 0; i < $scope.fields.length; i++){
			if($scope.fields[i].checked){
				$scope.fields[i].DFREQ = "Y";
			} else {
				$scope.fields[i].DFREQ = "N";
			}
		}
		
		for(var i = 0; i < $scope.ufields.length; i++){
			if($scope.ufields[i].checked){
				$scope.ufields[i].UREQ = "Y";
			} else {
				$scope.ufields[i].UREQ = "N";
			}
		}

		$http({ 
    		url:"./rest/AdminService/updateRequired",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "fields":$scope.fields, "ufields":$scope.ufields},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		$scope.init();
    	});
	};
	
	$scope.init();
    
});

angular.module('cobraApp').filter('unique', function () {

	return function (items, filterOn) {
	if (filterOn === false) {
		return items;
	}

	if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
	  var hashCheck = {}, newItems = [];

	  var extractValueToCompare = function (item) {
	    if (angular.isObject(item) && angular.isString(filterOn)) {
	      return item[filterOn];
	    } else {
	      return item;
	    }
	  };

	  angular.forEach(items, function (item) {
	    var valueToCheck, isDuplicate = false;

	    for (var i = 0; i < newItems.length; i++) {
	      if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
	        isDuplicate = true;
	        break;
	      }
	    }
	    if (!isDuplicate) {
	      newItems.push(item);
	    }

	  });
	  items = newItems;
	}
	return items;
	};
});