angular.module('cobraApp').controller('eFormCtrl', ['$scope', '$filter', '$location', '$sessionStorage', '$window', 'blockUI','$state','$rootScope','$http',
   function ($scope, $filter, $location, $sessionStorage, $window, blockUI,$state, $rootScope, $http) {
   	$scope.context = {
   			uniqueToken : "",	
   			documentFolder : "",	  
   			formid : ""
   	};
     
   	$scope.PFRMMSTR = {};
     
     $scope.PFRMPROP = 
             { FPID : "",
   			FPSEQ : 0,
   			FPNAME : "",
   			FPTYPE : "",
   			FPREQ : "", 	
   			FOVALUES : [] };
     
     $scope.selectedForm = {};
     
     $scope.PFRMRESP = {
   			FEID : 0,
   			FERID : 0,
   			FESEQ : 0,
   			FEDATA : "",
   			FSSTAMP : ""
   	};
     
     $scope.shouldShowNew = false;
     $scope.formIsCompleted = false;
     $scope.externalCheck = {
    	isAvailableExternally : false  
     };
     $scope.isEditForm = false;

     $scope.PFRMRESPs = [];
     
     $scope.FormElements = [];	 
     $scope.currentid = 0;
     $scope.errorLabel = "";
     $scope.externalLink = "";
     $scope.formID = 0;
     $scope.select = {};
     $scope.userValues = {name : "", email : ""};
     
     $scope.formCollection = [];
     $scope.lib = "";
     
     $scope.init = function () {
    	 $rootScope.isExtForm = true;
    	 $scope.formID = $location.search().id;
    	 $scope.lib = $location.search().l;
    	 $.getJSON( "./rest/ValidateUser/validateForm/" + $scope.lib + "/" + $scope.formID + "/" +
    			 $location.search().h, function( data ) {
	    			$scope.PFRMMSTR = data.PFRMMSTR;
   		  			$scope.selectedForm = $scope.PFRMMSTR;
   		  			$scope.loadFormResponses();
  		});
    	$scope.context.uniqueToken = maketoken();
    	$scope.context.documentFolder = $scope.lib + "_" + $scope.formid + "_" + $scope.context.uniqueToken;
     };
     
     $scope.getColNum =  function(number){
 		return new Array(number);
 	};
     
     $scope.loadFormResponses = function(){
   	  for(var i = 0; i < $scope.PFRMRESPs.length; i++){
   		  $('#'+$scope.PFRMRESPs[i].FESEQ).val($scope.PFRMRESPs[i].FEDATA);
   	  }
   	  $scope.$apply();
     };
     
     $scope.changeCheck = function(SEQ){
    	for(var i = 0; i < $scope.PFRMMSTR.PFRMPROP.length; i++){
    		if($scope.PFRMMSTR.PFRMPROP[i].FPSEQ == SEQ){
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == undefined){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("Y");
    				break;
    			}
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == "Y"){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES = [];
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("N");
    				break;
    			}
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == "N"){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES = [];
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("Y");
    				break;
    			}
    		}
    	}
     };
     
     $scope.changeRadio =  function(seq, value){
    	 var found = false;
    	 for(var i = 0; i < $scope.radioValues.length; i++){
    		 if($scope.radioValues[i].seq == seq){
    			 found = true;
    			 $scope.radioValues[i].value = value;
    		 }
    	 }
    	 if(!found){
    		 $scope.radioValues.push({seq : seq, value : value});
    	 }
     };
     
     $scope.submitForm = function() {
   		$("#message").text("");
   		var FormResponses = [];	
   		var valid = "true";

   		for(var mainkey in $scope.PFRMMSTR.PFRMPROP){
   			if($scope.PFRMMSTR.PFRMPROP.hasOwnProperty(mainkey)){
   				if($scope.PFRMMSTR.PFRMPROP[mainkey].FPTYPE != "FILE"){
   					var DataValue = "";
   					try{
   						document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ).parentNode.className = "form-group";
   						if($scope.PFRMMSTR.PFRMPROP[mainkey].FPREQ == "Y" && 
   						document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ).value == ""){
   							document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ).parentNode.className = "form-group has-error";
   							valid = "false";
   						} 
   						DataValue = document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ).value;
   						if(DataValue == "?"){
   							DataValue = "";
   						}
   						if($scope.PFRMMSTR.PFRMPROP[mainkey].FPTYPE == "CHECKBOX"){
   							if($scope.PFRMMSTR.PFRMPROP[mainkey].FOVALUES[0] == undefined ||
   									$scope.PFRMMSTR.PFRMPROP[mainkey].FOVALUES[0] == "N"){
   								DataValue = "N";
   							} else {
   								DataValue = "Y";
   							}
   						}
   					} catch(e){
   						for(var i = 0; i < $scope.radioValues.length; i++){
							if($scope.radioValues[i].seq == $scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ){
								DataValue = $scope.radioValues[i].value;
							}
						}
//   						DataValue = getRadioValue($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ);
   						document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ + "radiogroup").style.border = "0px solid #fff";
   						if($scope.PFRMMSTR.PFRMPROP[mainkey].FPREQ == "Y" && DataValue == ""){
   							document.getElementById($scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ + "radiogroup").style.border = "1px solid #a94442";
   							valid = "false";
   						} 
   					}
   					var FormResponse = { FEID: $scope.PFRMMSTR.PFRMPROP[mainkey].FPID, FESEQ: $scope.PFRMMSTR.PFRMPROP[mainkey].FPSEQ, FEDATA: DataValue };
   					FormResponses.push(FormResponse);
   				}
   			}
   		}
   		
   		if(valid == "true"){
   			var JSONResponse = JSON.stringify(FormResponses);
   			$.ajax({
   			    type: "GET",
   			    cache: false,		    
   			    url: "./rest/SaveFormResponse/saveResponseExt/" + $scope.lib + "/" + $scope.formID + "/" + $scope.context.documentFolder + "/" + 
   			    $scope.userValues.name + "/" + $scope.userValues.email + "/" + JSONResponse,	    
   			    complete: function(msg) {	
   			    	if(msg.status == 200){ 
   			    	 	try{
   			    	 		if(!$rootScope.isFormsModal){
   			    	 			$("#contentarea").html( "<p>Thank you, form successfully submitted.</p>" );
   			    	 		} else {
   			    	 			$rootScope.isFormSubmit = false;
   			    	 			$state.go('dashboard');
   			    	 		}
   			    		} catch(err){
   			    			console.log(err);
   			    		}    
   				    } else {
   				    	console.log(msg.responseText);
   				    }
   			    }
   			});
   		} else {
   			$("#message").text("Forms contains errors, please correct them before proceding");
   		}	
   	};
   	
   	$scope.cancelForm = function(){
    	window.close();
   	};
   	
    $scope.init();
     	
    $scope.addPopulatedElement = function(){
     		for (var n = 0; n < $scope.FormElements.length; n++) {	  
     			var FormElement = $scope.FormElements[n];
     			
     			$scope.addElement();
     			document.getElementById(n + "FPNAME").value = FormElement.FPNAME;
     		    document.getElementById(n + "FPTYPE").value = FormElement.FPTYPE;
     		    
     		    var FPREQelement = document.getElementById(n + "FPREQ"); 
     		    if(FormElement.FPREQ == "Y"){
     		    	FPREQelement.checked = true;
     		    }	
     		      	
     		    if(FormElement.FOVALUES.length > 0){
     		    	var parentElement = document.getElementById(n + "FOVALUE0");
     		    	element = parentElement.childNodes[0];
     		    	$(element).prop('disabled', false);
     		    	$(element).val(FormElement.FOVALUES[0]);
     		    }  	    
     		  	for (var i = 1; i < FormElement.FOVALUES.length; i++) {
     		  		var rowid = n + i;
     				$('#' + n + "FOVALUE").append("<p id = '" + n + i + "'><input class='form-control' style='display:inline;width:90%;' value = '" + FormElement.FOVALUES[i] + "'></input>" +
     			                 "<span style = 'margin-left:10px;' class='fa fa-minus-circle' ng-click='removeRow(rowid)'></span></p>");  	
     		    }
     		}
     	};
     	
     	$scope.handleValues = function(e){
     		for (var n = 0; n < $scope.formCollection.length; n++) {
     			if(e == $scope.formCollection[n].FPID){
     				$scope.PFRMPROP = $scope.formCollection[n];
     			}
     		}
     		if($scope.PFRMPROP.FPTYPE == "COMBO" || $scope.PFRMPROP.FPTYPE == "RADIO"){
     			var element = document.getElementById(e + "FOVALUE");
     			var elementCounter = 0;  		    
     			var OptionElements = [];
     			while(element != null){
     				elementCounter = elementCounter + 1;
     				try{
     					var parentElement = document.getElementById(e + "FOVALUE" + elementCounter);
     					element = parentElement.childNodes[0];
     					$(element).prop('disabled', false);
     					element = parentElement.childNodes[1];    
     					element.addEventListener('click', addFormOption, false);
     				} catch (err) {
     					element = null;
     				}
     			}
     		} else {
     			$scope.addOptions(e);
     		}
     	};

     	$scope.resetForm = function(){
     			$scope.currentid = 0;
     			$('#tablebody').empty();   		
     	        $scope.addElement();                                             
     	};

     	$scope.configureExportPresentation = function(){
     		$('#configureExportTableBody').empty();  
     		for (var i = 0; i < $scope.currentid; i++) {	
     		    var FPNAME = document.getElementById(i + "FPNAME").value;
     			$('#exportDataTable').append('<tr id = "Export' + i + '">' +                             
     			                               '<td><p>' + FPNAME + '</p></td>' +
     			                               '<td><input id = "' + i + 'ROW" class="form-control"></input></td>' +
     			                               '<td><input id = "' + i + 'COLUMN" class="form-control"></input></td>' +                              
     			                             '</tr>');
     		}
     	};

   }]);

function maketoken() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	  for( var i=0; i < 10; i++ ) {
		  text += possible.charAt(Math.floor(Math.random() * possible.length));
	  }
	  return text;
};