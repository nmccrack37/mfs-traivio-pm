
angular.module('cobraApp').controller('AccountCtrl', 
function ($scope, $filter, $location, $sessionStorage, $window, blockUI, $state, $rootScope, $cookies, $http, $uibModal, $state) {
	
	$scope.oldPass = "";
	$scope.newPass = "";
	$scope.passCheck = "";
	$scope.errorLabel = "";
	$scope.checkel = "";
	$scope.name = $sessionStorage.jobfunctiondescription;
	$scope.fname = "";
	$scope.lname = "";
	$scope.email = $sessionStorage.userid;
	$scope.isSubmitted = false;
	$scope.notifications = [];
	
	$scope.init = function () {
		$scope.isSubmitted = false;
		var splitter = $sessionStorage.jobfunctiondescription.split(" ");
		$scope.fname = splitter[0];
		$scope.lname = splitter[1];
		if($scope.notifications.length == 0){
			$http({ 
				url:"./rest/AdminService/getNotifications",
				method: "POST",
				data: {"datasource":$sessionStorage.datasource, "userid":$sessionStorage.userid},
				headers : {'Content-Type' : 'application/json'}}) 
				.then(function( response ) {
					$scope.notifications = response.data;
				});
		}
		
	};
	
	$scope.submitPassChange = function() {
		if($scope.newPass != $scope.passCheck){
			$scope.errorLabel = "Passwords do not match.";
		} else {
			$.getJSON( "./rest/AuthorizationService/changeUserPassword/" + $sessionStorage.userid + "/" + $scope.oldPass + "/" + $scope.newPass, function( data ) {	
				$.each( data, function( key, val ) {
				    switch(key){
				    	case "isError":
				    		error = val;
				    		break;
				    	case "returnMessage":
				    		if(error == true){
				    			$scope.errorLabel = val;
				    			$scope.$apply();
				    		}
				    		break;	
			    	}  	
			  	});
				if(error == false){
					$scope.checkel = $cookies.get('AUTH');
					try{
			    		$cookies.remove('AUTH');
					} catch (err) {}
					$scope.isRemember = false;
					$rootScope.isLogin = true;
					$sessionStorage.$reset();
		    		$state.go('login');
				}
			});
		}
	};
	
	$scope.submitProfileChange = function(isValid){
		
		$scope.name = $scope.fname + " " + $scope.lname;
		
		if(isValid){
		
			$http({ 
				url:"./rest/AdminService/updateUserProf",
				method: "POST",
				data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "fname":$scope.fname, "lname":$scope.lname, "email":$scope.email},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( data ) {
				$sessionStorage.userid = $scope.email;
				$sessionStorage.jobfunctiondescription = $scope.name
				$sessionStorage.$save();
			});
		}
	};
	
	$scope.submitNotifications = function(){
		for(var i = 0; i < $scope.notifications.length; i++){
			if(!$scope.notifications[i].optionRecord.InAppYN && !$scope.notifications[i].optionRecord.EmailYN){
				$scope.notifications[i].optionRecord.NotifyYN = false;
			} else {
				$scope.notifications[i].optionRecord.NotifyYN = true;
			}
		}
		$http({ 
			url:"./rest/AdminService/updateNotifications",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "notifications":$scope.notifications},
			headers : {'Content-Type' : 'application/json'}}) 
		.then(function( data ) {
			alert("Update Successful");
		});
	}
	
	$scope.init();
    
});