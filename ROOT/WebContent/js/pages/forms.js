angular.module('cobraApp').controller('FormCtrl',
function ($scope, $filter, $location, $sessionStorage, $window, blockUI,$state, $rootScope, $http, $compile) {
	$scope.context = {
			uniqueToken : "",	
			documentFolder : "",	  
			formid : ""
	};
  
  $scope.PFRMPROP = 
          { FPID : "",
			FPSEQ : 0,
			FPNAME : "",
			FPTYPE : "",
			FPREQ : "", 	
			FOVALUES : [] };
  
  $scope.selectedForm = {	
			FRID : 0,
			FRNAME : "",
			FREXTNL : "",
			FREXTHSH : "",
			PFRMPROPs : []
  };
  
  $scope.PFRMRESP = {
			FEID : 0,
			FERID : 0,
			FESEQ : 0,
			FEDATA : "",
			FSSTAMP : ""
	};
  
  $scope.userValues = {name: "", email : ""};
  
  $scope.shouldShowNew = true;
  $scope.formIsCompleted = false;
  $scope.externalCheck = {
	isAvailableExternally : false  
  };
  $scope.isEditForm = false;
  $scope.currentlyInNew = false;
//  $scope.isCollapsed = false;
  
  $scope.PFRMMSTRs = [];
  $scope.PFRMRESPs = [];
  
  $scope.FormElements = [];	 
  $scope.currentid = 0;
  $scope.errorLabel = "";
  $scope.formName = {
		  text : ""
  };  
  $scope.externalLink = "";
  $scope.formID = 0;
  $scope.libEncrypted = "";
  
  $scope.formCollection = [];
  $scope.radioValues = [];
  
  $scope.processtasks = {
		  PORT: "",
		  TKTYPE: "",
		  TKTMPNM: ""
  };
  $scope.PorT = {};
  
  $scope.encryptLibrary = function(){
		$.ajax({
		    type: "GET",
		    cache: false,
		    url: "./rest/ValidateUser/encryptLib/" + $sessionStorage.datasource,	    
		    complete: function(data) {	
		    	$.each( data, function( key, val ) {
		    		$scope.libEncrypted = data.responseText;
		    	});
		    }
		});
	};
  
  $scope.init = function () {
	  
	  if($sessionStorage.formid != undefined && $sessionStorage.formid != 0){
	 		if($sessionStorage.formresp != undefined && $sessionStorage.formresp != 0){
	 			$scope.formIsCompleted = true;
	 		}
	 		if(!$rootScope.isFormsModal){
	 			document.getElementById("formsContent").setAttribute("id", "formsSubmitContent");
	 		}
	 		$scope.loadForm();
	  } else {
		  $scope.formIsCompleted = false;
		  $http({ 
				url:"./rest/RetrieveForm/accessAllForms",
				method: "POST",
				data: {"lib":$sessionStorage.datasource, "user":$sessionStorage.userid},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.PFRMMSTRs = response.data;
		  });
	 	}
 	$scope.context.uniqueToken = maketoken();
 	$scope.context.documentFolder = $sessionStorage.datasrource + "_" + $sessionStorage.formid + "_" + $scope.context.uniqueToken;
	$scope.encryptLibrary();
  };

  
  $scope.loadForm = function(){
	  $scope.shouldShowNew = false;
	  if(!$rootScope.isFormsModal){
		  $state.go('forms.formmain');
	  }
	  $.getJSON( "./rest/RetrieveFormResponses/accessResponses/" + $sessionStorage.datasource + "/" + $sessionStorage.formid + "/" +
		$sessionStorage.formresp + "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + 
		$sessionStorage.sessionTimeout, function( data ) {
		  		$scope.PFRMMSTR = data.PFRMMSTR;
		  		$scope.testArray = [];
		  		$.each(data.PFRMMSTR.PFRMPROP, function(index, prop) {
		  			$scope.testArray.push(prop);
		  		});
		  		$scope.PFRMMSTR.PFRMPROP = $scope.testArray;
				$scope.selectedForm = $scope.PFRMMSTR;

				$.each(data.PFRMRESP, function (key, val){
			  		$scope.PFRMRESPs.push(val); 
				});
				$rootScope.isFormSubmit = true;
				if($scope.formIsCompleted){
					$scope.userValues.name = $scope.PFRMRESPs[0].FSNAME;
					$scope.userValues.email = $scope.PFRMRESPs[0].FSEMAIL;
				}
				$scope.$apply();
				$scope.loadFormResponses();
	  });
  };
  
  $scope.loadFormLocal = function(id) {
	  for(var i = 0; i < $scope.PFRMMSTRs.length; i++){
		  if($scope.PFRMMSTRs[i].FRID == id){
			  $scope.selectedForm = $scope.PFRMMSTRs[i];
		  }
	  }
  };
  
  $scope.loadFormResponses = function(){
	  for(var i = 0; i < $scope.PFRMRESPs.length; i++){
		  $('#'+$scope.PFRMRESPs[i].FESEQ).val($scope.PFRMRESPs[i].FEDATA);
	  }
  };
  
  $scope.newForm = function(){
	  if(!$scope.isEditForm && !$scope.currentlyInNew){
		$scope.currentlyInNew = true;
	  	$state.go('forms.formsbuilder');
	    $scope.addOptions(1);
		if($sessionStorage.formid != undefined && $sessionStorage.formid != 0){
			$.getJSON( "./rest/RetrieveForm/accessForm/" + $sessionStorage.datasource + "/" + $sessionStorage.formid + "/" + $sessionStorage.useris + 
					"/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout,  function( data ) {
				  $.each( data, function( key, val ) {
				    switch(key){
				    	case "ERROR":
				    		$scope.errorLabel = val;
				    		break;
				    	case "FRNAME":
				    		$scope.formName.text = val;		    		  
				    		break;
				    	case "PFRMPROP":			    		
				    	    for (var i = 0; i < val.length; i++) {
				    	    	var FPID;
				    			var FPSEQ;
				    			var FPNAME;
				    			var FPTYPE;
				    			var FPREQ; 	
				    			var FOVALUES = [];
				    			
					    		$.each( val[i], function( propkey, propval ) {			    			
					    					
					    			switch(propkey){
					    				case "FPID":
					    					FPID = propval;
					    					break;
					    				case "FPSEQ":
					    					FPSEQ = propval;
					    					break;
					    				case "FPNAME":
					    					FPNAME = propval;
					    					break;
					    				case "FPTYPE":
					    					FPTYPE = propval;
					    					break;
					    				case "FPREQ":
					    					FPREQ = propval;
					    					break;
					    				case "FOVALUES":
					    					FOVALUES = propval;
					    					break;
					    			 }		    			 
					    		});
					    		var FormElement = { FPID: FPID, FPSEQ: FPSEQ, FPNAME: FPNAME, FPTYPE: FPTYPE, FPREQ: FPREQ, FOVALUES: FOVALUES};
					    		$scope.FormElements.push(FormElement);
					    	}		    					    				
				    		break;
				    }	
				  });
				  $scope.addPopulatedElement(); 	  
				});	
		} else {	
			$.getJSON( "./rest/ValidateUser/validate/" + $sessionStorage.datasource + "/" + $sessionStorage.userid + 
					"/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout,  function( data ) {
				  $.each( data, function( key, val ) {
				    switch(key){
				    	case "ERROR":
				    		$scope.errorLabel = val;
				    		break;
				   	}
				});
			});	  
			$scope.addElement();
		}
		$scope.loadProcesses();
	  }
		
	};
	
	$scope.loadProcesses = function(){
		$.getJSON( "./rest/RetrieveForm/getProcessTasks/" + $sessionStorage.datasource, function( data ) {
			$scope.processtasks = data;
			$scope.$apply();
		});
	};
	
	$scope.submitFormCheck = function(){
		if($sessionStorage.formid != 0 && $sessionStorage.formid != undefined || !$scope.shouldShowNew){
			$scope.submitForm();
		} else{
			$scope.submitNewForm();
		}
	};
  
  $scope.submitForm = function() {
		$("#message").text("");
		var FormResponses = [];	
		var valid = "true";
		
//		for(var key in $scope.PFRMMSTRs){
//			if($scope.PFRMMSTRs.hasOwnProperty(key)){
		for(var mainkey in $scope.selectedForm.PFRMPROP){
			if($scope.selectedForm.PFRMPROP.hasOwnProperty(mainkey)){
				if($scope.selectedForm.PFRMPROP[mainkey].FPTYPE != "FILE"){
					var DataValue = "";
					try{
						document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ).parentNode.className = "form-group";
						if($scope.selectedForm.PFRMPROP[mainkey].FPREQ == "Y" && 
								document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ).value == ""){
							document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ).parentNode.className = "form-group has-error";
							valid = "false";
						} 
						DataValue = document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ).value;
						if(DataValue == "?"){
   							DataValue = "";
   						}
						if($scope.selectedForm.PFRMPROP[mainkey].FPTYPE == "CHECKBOX"){
		   					if($scope.selectedForm.PFRMPROP[mainkey].FOVALUES[0] == undefined ||
		   							$scope.selectedForm.PFRMPROP[mainkey].FOVALUES[0] == "N"){
		   						DataValue = "N";
		   					} else {
		   						DataValue = "Y";
		   					}
		   				}
					} catch(e){
						for(var i = 0; i < $scope.radioValues.length; i++){
							if($scope.radioValues[i].seq == $scope.selectedForm.PFRMPROP[mainkey].FPSEQ){
								DataValue = $scope.radioValues[i].value;
							}
						}
//						DataValue = getRadioValue($scope.selectedForm.PFRMPROPs[mainkey].FPSEQ);
						document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ + "radiogroup").style.border = "0px solid #fff";
						if($scope.selectedForm.PFRMPROP[mainkey].FPREQ == "Y" && DataValue == ""){
							document.getElementById($scope.selectedForm.PFRMPROP[mainkey].FPSEQ + "radiogroup").style.border = "1px solid #a94442";
							valid = "false";
						} 
					}
					var FormResponse = { FEID: $scope.selectedForm.PFRMPROP[mainkey].FPID, FESEQ: $scope.selectedForm.PFRMPROP[mainkey].FPSEQ, FEDATA: DataValue };
					FormResponses.push(FormResponse);
				}
			}
//				}
//			}
  		}	
		
		if(valid == "true"){
			var JSONResponse = JSON.stringify(FormResponses);
			$.ajax({
			    type: "GET",
			    cache: false,		    
			    url: "./rest/SaveFormResponse/saveResponse/" + $sessionStorage.datasource + "/" + $sessionStorage.formid + "/" + 
			    	$sessionStorage.userid + "/" + $sessionStorage.jobfunctiondescription + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + 
			    	$sessionStorage.sessionTimeout + "/" + $scope.context.documentFolder + "/" + $sessionStorage.KEYN1 + "/" 
			    	+ $sessionStorage.KEYN2 + "/" + $sessionStorage.KEYN3 + "/" + JSONResponse,	    
			    complete: function(msg) {	
			    	if(msg.status == 200){ 
			    	 	try{
			    			$("#contentarea").html( "<p>Thank you, form successfully submitted.</p>" );  			
			    		} catch(err){
			    			console.log(err);
			    		}    
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
			});
			setTimeout(function(){
				$rootScope.isFormSubmit = false;
				document.getElementById("formsSubmitContent").setAttribute("id", "formsContent");
			}, 5000);
		} else {
			$("#message").text("Forms contains errors, please correct them before proceding");
		}	
	};
	
	$scope.editForm = function(id){
	  	$scope.formID = id;
	  	for(var i = 0; i < $scope.PFRMMSTRs.length; i++){
			  if($scope.PFRMMSTRs[i].FRID == id){
				  $scope.selectedForm = $scope.PFRMMSTRs[i];
			  }
		}
	  	
	  	$scope.formName.text = $scope.selectedForm.FRNAME;
	  	$scope.formCollection = $scope.selectedForm.PFRMPROP;
	  	$scope.isEditForm = true;
	  	if($scope.selectedForm.FREXTNL == "Y"){
	  		$scope.externalCheck.isAvailableExternally = true;
	  	} else {
	  		$scope.externalCheck.isAvailableExternally = false;
	  	}

	  	$scope.currentid = $scope.formCollection[$scope.formCollection.length-1].FPSEQ+1;
	  	$scope.externalLink = "http://mfs.traiv.io/#/eforms?id="+id+"&h="+$scope.selectedForm.FREXTHSH+"&l="+$scope.libEncrypted;
	  	for(var i = 0; i < $scope.formCollection.length; i++){
	  		if($scope.formCollection[i].FPSEQ == $scope.selectedForm.FRPORTLBL){
	  			$scope.PorT.sdTitle = $scope.formCollection[i];
	  		}
	  	}
	  	
	  	$.getJSON( "./rest/RetrieveForm/getProcessTasks/" + $sessionStorage.datasource, function( data ) {
			$scope.processtasks = data;
			for(var i = 0; i < $scope.processtasks.length; i++){
				if($scope.processtasks[i].PORT == $scope.selectedForm.FRPORT && $scope.processtasks[i].TKTYPE == $scope.selectedForm.FRPORTID){
					$scope.PorT.values = $scope.processtasks[i];
				}
			}
			$scope.$apply();
		});
	};
	
	$scope.submitFormEdit = function() {
		var External = "N";
		var FormElements = [];
		if($scope.externalCheck.isAvailableExternally){
			External = "Y";
		};
		 
		for (var i = 0; i < $scope.formCollection.length; i++) {
		    
		    if($scope.formCollection[i].FPTYPE == ""){
		    	$scope.formCollection[i].FPTYPE = "TEXTFIELD";
		    }
		    
		    if($scope.formCollection[i].FPREQ == true){$scope.formCollection[i].FPREQ = "Y"}else{$scope.formCollection[i].FPREQ = "N"};	    
		    
		    var element = document.getElementById($scope.formCollection[i].FPSEQ + "FOVALUE");
		    var elementCounter = 0;
		    
		    var OptionElements = [];
		    while(element != null){
		    	elementCounter = elementCounter + 1;
		    	element = document.getElementById($scope.formCollection[i].FPSEQ + "FOVALUE" + elementCounter);
		    	if(element != null){
		    		var OptionElement = element.childNodes[0].value;
		    		if(OptionElement != ""){
		    			OptionElements.push(OptionElement);
		    		}
		    	} 	
		    }	    
		    $scope.formCollection[i].FOVALUES = OptionElements;
			var FormElement = { FPID: $scope.formCollection[i].FPID, FPSEQ: $scope.formCollection[i].FPSEQ, FPNAME: $scope.formCollection[i].FPNAME, FPTYPE: $scope.formCollection[i].FPTYPE, FPREQ: $scope.formCollection[i].FPREQ, FOVALUES: OptionElements };
			FormElements.push(FormElement);
		}
				
//		var JSONResponse = JSON.stringify(FormElements);

		if($scope.formName.text != ""){
			var PorT = "";
			var PorTID = "";
			var PorTLBL = 0;
			
			try {
				PorT = $scope.PorT.values.PORT;
				PorTID = $scope.PorT.values.TKTYPE;
				PorTLBL = $scope.PorT.sdTitle.FPSEQ;
			} catch(exception){
				
			}
			$http({ 
	    		url:"./rest/SaveForm/save",
	    		method: "POST",
	    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "formID":$scope.formID, "FormName":$scope.formName.text,
	    				"External":External, "ExternalHash":$scope.selectedForm.FREXTHSH, "PorT":PorT, "PorTID":PorTID, "PorTLBL":PorTLBL, 
	    				"PAssign":$scope.selectedForm.FRPASSIGN, "responses":FormElements},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( data ) {
	    		try{
	    	 		$scope.formName.text = "";
	      			$scope.currentid = 1;
	      			$scope.formCollection = []; 
	    			$("#formcontents").html( "<p style='margin-left: 35%;'>Form successfully created.</p>" ); 
	    			$('#buttondiv').empty();
	    			$scope.currentlyInNew = false;
	    			$scope.isEditForm = false;
		    			setTimeout(function(){
		    				$state.go('forms');
		    			}, 3000);
	    		} catch(err){
	    			console.log(err);
	    		} 
	    	});
		}
			
	};
	
	$scope.cancelForm = function(){
		$rootScope.isFormSubmit = false;
		if(!$rootScope.isFormsModal){
			document.getElementById("formsSubmitContent").setAttribute("id", "formsContent");
			window.close();
		} else {
			$state.go('dashboard');
		}
	};
	
	$scope.loadFormLayout = function(){
		$sessionStorage.formid = $scope.selectedForm.FRID;
		$sessionStorage.$save();
		$state.go('formlayout');
	};
	
  	$scope.init();
  	
  	$scope.addPopulatedElement = function(){
  		for (var n = 0; n < $scope.FormElements.length; n++) {	  
  			var FormElement = $scope.FormElements[n];
  			
  			$scope.addElement();
  			document.getElementById(n + "FPNAME").value = FormElement.FPNAME;
  		    document.getElementById(n + "FPTYPE").value = FormElement.FPTYPE;
  		    
  		    var FPREQelement = document.getElementById(n + "FPREQ"); 
  		    if(FormElement.FPREQ == "Y"){
  		    	FPREQelement.checked = true;
  		    }	
  		      	
  		    if(FormElement.FOVALUES.length > 0){
  		    	var parentElement = document.getElementById(n + "FOVALUE0");
  		    	element = parentElement.childNodes[0];
  		    	$(element).prop('disabled', false);
  		    	$(element).val(FormElement.FOVALUES[0]);
  		    }  	    
  		  	for (var i = 1; i < FormElement.FOVALUES.length; i++) {
  				$('#' + n + "FOVALUE").append("<p id = '" + n + i + "'><input class='form-control' style='display:inline;width:90%;' value = '" + FormElement.FOVALUES[i] + "'></input>" +
  			                 "<span style = 'margin-left:10px;' class='fa fa-minus-circle' ng-click='removeRow($event)'></span></p>");  	
  		    }
  		}
  	};
  	
  	$scope.addElement = function(){
  		$scope.PFRMPROP = {
  				FPID:  $scope.currentid,
				FPSEQ: 0,
				FPNAME: "",
				FPTYPE: "",
				FPREQ: false,
				FOVALUES: []
		};
  		$scope.formCollection.push($scope.PFRMPROP);
//  	    addOptions($scope.currentid);  
  	  $scope.currentid += 1;                     
  	};
  	
  	$scope.addElementEdit = function(){
  		$scope.PFRMPROP = {
  				FPID:  $scope.formID,
				FPSEQ: $scope.currentid,
				FPNAME: "",
				FPTYPE: "",
				FPREQ: false,
				FOVALUES: []
		};
  		$scope.formCollection.push($scope.PFRMPROP);
//  	    addOptions($scope.currentid);  
  	  $scope.currentid += 1;    
  	};
  	
  	$scope.addOptions = function(row){
  		var element = document.getElementById(row + "FOVALUE");	    
  		$(element).html("<p id = '" + row + "FOVALUE1'><input class='form-control' style='display:inline;width:90%;' disabled></input>" +
  	                        "<span style = 'margin-left:10px;' class='fa fa-plus-circle'></span></p>"); 
  	};
  	
  	$scope.removeRow = function(rowid){
  		var row = document.getElementById(rowid.target.parentNode.id);
  		row.parentNode.removeChild(row);
  	};
  	
  	$scope.removeElement = function(row){
  		var index = $scope.formCollection.indexOf(row);
  		$scope.formCollection.splice(index, 1);
  	};
  	
  	function addFormOption(e){
  		var parentid = e.target.parentNode.parentNode.id;
  		var nextIndex = 0;
  	    var elementExists = document.getElementById(parentid);
  		while(elementExists != null){
  			nextIndex = nextIndex + 1;
  			elementExists = document.getElementById(parentid + nextIndex);
  		}
  		var rowid = parentid + nextIndex;
  		
  		var compileTxt = "<p id = '" + parentid + nextIndex + "'><input class='form-control' style='display:inline;width:90%;'></input>" +
          "<span style = 'margin-left:10px;' class='fa fa-minus-circle' ng-click='removeRow($event)'></span></p>";
  		var compileDone = $compile(compileTxt)($scope);

  		$('#' + parentid).append(compileDone);
  	};
  	
  	$scope.handleValues = function(e){
  		for (var n = 0; n < $scope.formCollection.length; n++) {
  			if(e == $scope.formCollection[n].FPID){
  				$scope.PFRMPROP = $scope.formCollection[n];
  			}
  		}
  		if($scope.PFRMPROP.FPTYPE == "COMBO" || $scope.PFRMPROP.FPTYPE == "RADIO"){
  			var element = document.getElementById(e + "FOVALUE");
  			var elementCounter = 0;  		    
  			var OptionElements = [];
  			while(element != null){
  				elementCounter = elementCounter + 1;
  				try{
  					var parentElement = document.getElementById(e + "FOVALUE" + elementCounter);
  					element = parentElement.childNodes[0];
  					$(element).prop('disabled', false);
  					element = parentElement.childNodes[1];    
  					element.addEventListener('click', addFormOption, false);
  				} catch (err) {
  					element = null;
  				}
  			}
  		} else {
  			$scope.addOptions(e);
  		}
  	};
  	
  	$scope.handleValuesEdit = function(e){
  		for (var n = 0; n < $scope.formCollection.length; n++) {
  			if(e == $scope.formCollection[n].FPSEQ){
  				$scope.PFRMPROP = $scope.formCollection[n];
  			}
  		}
  		if($scope.PFRMPROP.FPTYPE == "COMBO" || $scope.PFRMPROP.FPTYPE == "RADIO"){
  			var element = document.getElementById(e + "FOVALUE");
  			var elementCounter = 0;  		    
  			var OptionElements = [];
  			while(element != null){
  				elementCounter = elementCounter + 1;
  				try{
  					var parentElement = document.getElementById(e + "FOVALUE" + elementCounter);
  					element = parentElement.childNodes[0];
  					$(element).prop('disabled', false);
  					element = parentElement.childNodes[1];    
  					element.addEventListener('click', addFormOption, false);
  				} catch (err) {
  					element = null;
  				}
  			}
  		} else {
  			$scope.addOptions(e);
  		}
  	};
  	
  	$scope.addFormOptionEdit = function(e){
  		var parentid = e.target.parentNode.parentNode.id;
  		var nextIndex = 0;
  	    var elementExists = document.getElementById(parentid);
  		while(elementExists != null){
  			nextIndex = nextIndex + 1;
  			elementExists = document.getElementById(parentid + nextIndex);
  		}
  		var rowid = parentid + nextIndex;
  		
  		var compileTxt = "<p id = '" + parentid + nextIndex + "'><input class='form-control' style='display:inline;width:90%;'></input>" +
          "<span style = 'margin-left:10px;' class='fa fa-minus-circle' ng-click='removeRow($event)'></span></p>";
  		var compileDone = $compile(compileTxt)($scope);

  		$('#' + parentid).append(compileDone);
  	};
  	
  	$scope.submitNewForm = function(){
  		
  		var External = "N";
  		var FormElements = [];
  		if($scope.externalCheck.isAvailableExternally){
  			External = "Y";
  		};
  		 
  		for (var i = 0; i < $scope.formCollection.length; i++) {
  		    
  		    if($scope.formCollection[i].FPTYPE == ""){
  		    	$scope.formCollection[i].FPTYPE = "TEXTFIELD";
  		    }
  		    
  		    if($scope.formCollection[i].FPREQ == true){$scope.formCollection[i].FPREQ = "Y"}else{$scope.formCollection[i].FPREQ = "N"};	    
  		    
  		    var element = document.getElementById($scope.formCollection[i].FPID + "FOVALUE");
  		    var elementCounter = 0;
  		    
  		    var OptionElements = [];
  		    while(element != null){
  		    	elementCounter = elementCounter + 1;
  		    	element = document.getElementById(i + "FOVALUE" + elementCounter);
  		    	if(element != null){
  		    		var OptionElement = element.childNodes[0].value;
  		    		if(OptionElement != ""){
  		    			OptionElements.push(OptionElement);
  		    		}
  		    	} 	
  		    }	    
  		    $scope.formCollection[i].FOVALUES = OptionElements;
  			var FormElement = { FPID: $scope.formCollection[i].FPID, FPSEQ: $scope.formCollection[i].FPSEQ, FPNAME: $scope.formCollection[i].FPNAME, FPTYPE: $scope.formCollection[i].FPTYPE, FPREQ: $scope.formCollection[i].FPREQ, FOVALUES: OptionElements };
  			FormElements.push(FormElement);
  		}
  				
  		var JSONResponse = JSON.stringify(FormElements);
  		
  		if($scope.formName.text != ""){
  			var PorT = "";
  			var PorTID = "";

  			try{
  				PorT = $scope.PorT.values.PORT;
  				PorTID = $scope.PorT.values.TKTYPE;
  			} catch(Exception){
  				
  			};
  			$http({ 
	    		url:"./rest/SaveForm/save",
	    		method: "POST",
	    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "formID":$scope.formID, "FormName":$scope.formName.text,
	    				"External":External, "ExternalHash":"", "PorT":PorT, "PorTID":PorTID, "responses":FormElements},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( data ) {
	    		try{
		    	 	$scope.formName.text = "";
	      			$scope.currentid = 1;
	      			$scope.formCollection = []; 
		    		$("#formcontents").html( "<p style='margin-left: 35%;'>Form successfully created.</p>" ); 
		    		$('#buttondiv').empty();
		    		$scope.currentlyInNew = false;
		    		$scope.isEditForm = false;
		    		setTimeout(function(){
		    			$state.go('forms');
		    		}, 3000);
		    	} catch(err){
		    		console.log(err);
		    	}  
	    	});
  		}
  	};
  	
  	$scope.changeCheck = function(SEQ){
    	for(var i = 0; i < $scope.PFRMMSTR.PFRMPROP.length; i++){
    		if($scope.PFRMMSTR.PFRMPROP[i].FPSEQ == SEQ){
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == undefined){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("Y");
    				break;
    			}
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == "Y"){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES = [];
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("N");
    				break;
    			}
    			if($scope.PFRMMSTR.PFRMPROP[i].FOVALUES[0] == "N"){
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES = [];
    				$scope.PFRMMSTR.PFRMPROP[i].FOVALUES.push("Y");
    				break;
    			}
    		}
    	}
     };
     
     $scope.changeRadio =  function(seq, value){
    	 var found = false;
    	 for(var i = 0; i < $scope.radioValues.length; i++){
    		 if($scope.radioValues[i].seq == seq){
    			 found = true;
    			 $scope.radioValues[i].value = value;
    		 }
    	 }
    	 if(!found){
    		 $scope.radioValues.push({seq : seq, value : value});
    	 }
     };

  	$scope.resetForm = function(){
  			$scope.formName.text = "";
  			$scope.currentid = 1;
  			$scope.formCollection = [];   		
  	        $scope.addElement();                                             
  	};

  	$scope.configureExportPresentation = function(){
  		$('#configureExportTableBody').empty();  
  		for (var i = 0; i < $scope.currentid; i++) {	
  		    var FPNAME = document.getElementById(i + "FPNAME").value;
  			$('#exportDataTable').append('<tr id = "Export' + i + '">' +                             
  			                               '<td><p>' + FPNAME + '</p></td>' +
  			                               '<td><input id = "' + i + 'ROW" class="form-control"></input></td>' +
  			                               '<td><input id = "' + i + 'COLUMN" class="form-control"></input></td>' +                              
  			                             '</tr>');
  		}
  	};
  	  
  	  $scope.closeModal = function() {
  		 $sessionStorage.formid = 0;
  		 
  		 $state.go("dashboard");
  	  };

});


function parseURL(sParam, sPageURL)
{
	sPageURL = sPageURL.slice(sPageURL.indexOf("?") + 1, sPageURL.length);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function maketoken() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	  for( var i=0; i < 10; i++ ) {
		  text += possible.charAt(Math.floor(Math.random() * possible.length));
	  }
	  return text;
};