angular.module('cobraApp').controller('ProcessCtrl',
function ($scope, $filter, $sessionStorage, $state, $rootScope, $http, $compile, $parse, $uibModal, $timeout) {
	
	$scope.theadmargin = "text-align:center !important;";  
	$scope.tbodyscroll = "overflow-y:auto; overflow-x: auto;";
	  
	$scope.init = function(){
		$scope.Process = {};
		$scope.taskTemplates = [];
		$scope.descriptions = [];
		$scope.PRELDESCs = [];
		$scope.jobfunctionMasters = [];
		$scope.names = [];
		$scope.ProcTypes = [];
		$scope.SubProcTypes = [];
		$scope.Frequencies = [];
		$scope.Cats = [];
		$scope.SubCats = [];
		$scope.SubCat2s = [];
		$scope.SubCat3s = [];
		$scope.UDFVals = [];
		$scope.fields = [];
		$scope.required = [];
		$scope.processSource = "";
		$scope.storedTask = 0;
		$scope.selected = {text:""};
		$scope.numRequired = 1;
		$scope.procField = "";
		$scope.pinfo = {};
		$scope.proctype = {};
		$scope.statusInfo = "";
		$scope.statusColor = "";
//		$scope.popup = {};
		$scope.dtS = undefined;
		$scope.dtE = undefined;
		$scope.checkbox = {selected:false};
		$scope.note = {storedText:""};
		$scope.showPopover = false;
		$scope.subProcImg = "./images/367-code-tree@2x-16x16.png";
		
		$scope.dynamicPopover = {
				content: '',
			    templateUrl: 'myPopoverTemplate.html',
			    title: 'Change Status'
			  };
		
		$scope.titlePopover = {
				content: '',
			    templateUrl: 'processMainPopover.html',
			    title: 'Process No: '
			  };
		
		var modalInstance = null;
		
		switch($rootScope.ProcessStatus){
			case "Behind Schedule":
				$scope.processSource = "./images/tasks/TriangleExc.png";
				$scope.statusColor = "red";
				$scope.statusInfo = "Process is " + $rootScope.ProcessDays + " Behind Schedule";
				$scope.titlePopover.templateUrl = "processMainPopover.html";
				break;
			case "Paused":
				$scope.processSource = "./images/tasks/Red_pause_big.png";
				$scope.statusColor = "gray";
				$scope.statusInfo = "Process is currently paused";
				$scope.titlePopover.templateUrl = "processMainPopover.html";
				break;
			case "On Time":
				$scope.processSource = "./images/tasks/inProcess.png";
				$scope.statusColor = "black";
				$scope.statusInfo = "Process running on schedule";
				$scope.titlePopover.templateUrl = "processMainPopover.html";
				break;
			case "Not Started":
				$scope.processSource = "./images/tasks/Gray_clock.png";
				$scope.statusColor = "black";
				$scope.statusInfo = "Process not started";
				$scope.titlePopover.templateUrl = "processMainSecondPopover.html";
				break;
		}
		$http({ 
			url:"./rest/ProcessMainService/getProcess",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "user":$sessionStorage.userid, "TKNO":$sessionStorage.KEYN1},
			headers : {'Content-Type' : 'application/json'}}) 
		.then(function( response ) {
			$scope.Process = response.data;
			if($scope.Process.RecentNote != undefined){
				$scope.note.storedText = $scope.Process.RecentNote;
			}
			$scope.Process.EstCompDateFmt = $filter('date')($scope.Process.EstCompDate);
			for(var i = 0; i < $scope.Process.Tasks.length; i++){
				$scope.Process.Tasks[i].selected = false;
				switch($scope.Process.Tasks[i].Status){
					case "0":
						if($scope.Process.Tasks[i].ActStartDate != ""){
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/inProcess_small.png";
							$scope.Process.Tasks[i].showPopover = true;
						} else {
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Red_start.png";
							$scope.Process.Tasks[i].showPopover = false;
							$scope.Process.Tasks[i].Status = "6";
						}
						break;
					case "1":
						$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Green_check.png";
						$scope.Process.Tasks[i].showPopover = true;
						break;
					case "3":
						$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_inactive.png";
						$scope.Process.Tasks[i].showPopover = true;
						break;
					case "4":
						$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_clock.png";
						$scope.Process.Tasks[i].showPopover = false;
						break;
					case "5":
						$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Red_pause.png";
						$scope.Process.Tasks[i].showPopover = true;
						break;
				}
				$scope.Process.Tasks[i].ActStartDateFmt = $filter('date')($scope.Process.Tasks[i].ActStartDate);
				$scope.Process.Tasks[i].ActCompDateFmt = $filter('date')($scope.Process.Tasks[i].ActCompDate);
			}
			$scope.displayedProcess = [].concat($scope.Process.Tasks);	
			$scope.loadJobFunctions();
	  });
		setTimeout(function(){
			$http({ 
				url:"./rest/NotesService/getAllTags",
				method: "POST",
				data: {"lib":$sessionStorage.datasource},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.PRELDESCs = response.data;
				for(var i = 0; i < $scope.PRELDESCs.length; i++){
					$scope.descriptions.push($scope.PRELDESCs[i].PRDESC)
				}
				$("#processTags").tagit({
					availableTags: $scope.descriptions
				});
				var tagCheck = [];
				if($scope.Process.relTos != undefined){
					for(var i = 0; i < $scope.Process.relTos.length; i++){
						var found = false;
						if(tagCheck.indexOf($scope.Process.relTos[i].Desc) > -1){
							found = true;
						}
						if(!found){
							$("#processTags").tagit("createTag", $scope.Process.relTos[i].Desc);
							tagCheck.push($scope.Process.relTos[i].Desc);
						}
					}
				}
		  });
		}, 500);
	}
	
	$scope.loadJobFunctions = function() {
		$.getJSON( "./rest/ProcessTaskService/getJobFunctions/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout, function( data ) {	  
			$.each( data, function( key, val ) {	  	 		 	
				$scope.jobfunction = {
						role: val.KAJOBROLE,
						name: val.DisplayText
				};
				$scope.jobfunctionMasters.push($scope.jobfunction);
				$scope.names.push($scope.jobfunction.name.trim());
				if($scope.Process.AssignedTo.trim() == $scope.jobfunction.role.trim()){
					$scope.Process.AssignedTo = $scope.jobfunction.name.trim();
				}
				for(var i = 0; i < $scope.Process.Tasks.length; i++){
					if($scope.Process.Tasks[i].Assigned.trim() == $scope.jobfunction.role.trim()){
						$scope.Process.Tasks[i].Assigned = $scope.jobfunction.name.trim();
					}
				}
			 });
			$scope.loadUDFs();
		  });
	  };
	 
	  $scope.closeModal = function() {	 
		  $state.go("dashboard");
	  };
	  	  
	  $scope.getters = {
			  taskStartDateSetter:function(row) {
				  return new Date(row.ActStartDate);
			  },
			  taskEndDateSetter:function(row) {
				  return new Date(row.ActCompDate);
			  }
	  };
	  
	  $scope.setProcStatus = function(Statusin){
		  $scope.Process.Status = Statusin;
		switch(Statusin){
			case "5":
				$scope.processSource = "./images/tasks/Red_pause_big.png";
				$scope.statusColor = "gray";
				$scope.statusInfo = "Process is currently paused";
				$scope.titlePopover.templateUrl = "processMainPopover.html";
				break;
			case "0":
				$scope.processSource = "./images/tasks/inProcess.png";
				$scope.statusColor = "black";
				$scope.statusInfo = "Process running on schedule";
				$scope.titlePopover.templateUrl = "processMainPopover.html";
				break;
			case "3":
				$scope.processSource = "./images/tasks/Gray_inactive.png";
				$scope.statusColor = "black";
				$scope.statusInfo = "Process inactivated";
				$scope.titlePopover.templateUrl = "";
		}
	  };
	  
	  $scope.setStatus = function(Statusin){
		  var string = "popoverMaster"+$scope.storedTask;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == $scope.storedTask){
				  $scope.Process.Tasks[i].previousStat = $scope.Process.Tasks[i].Status;
				  $scope.Process.Tasks[i].Status = Statusin;
				  $scope.Process.Tasks[i].selected = false;
				  switch(Statusin){
				  	case "0":
				  		//start
				  		$scope.Process.Tasks[i].StatusIcon = "./images/tasks/inProcess_small.png";
				  		$scope.Process.Tasks[i].showPopover = true;
				  		break;
				  	case "1":
				  		// show menu
				  		$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Green_check.png";
				  		$scope.Process.Tasks[i].showPopover = true;
				  		break;
				  	case "3":
				  		// do nothing
				  		$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_inactive.png";
				  		$scope.Process.Tasks[i].showPopover = true;
				  		break;
				  	case "4":
				  		//do nothing
				  		$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_clock.png";
				  		$scope.Process.Tasks[i].showPopover = false;
				  		break;
				  	case "5":
				  		// show menu
				  		$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Red_pause.png";
				  		$scope.Process.Tasks[i].showPopover = true;
				  		break;
				  }
			  }
		  }
//		  $timeout(function(){
//			  $scope.$apply(function(){
//				  alert(string);
//				 $scope[string] = false;
//			  });
//		  });
	  };
	  
	  $scope.storeRow = function(TSTSKNO){
		  if($scope.storedTask != TSTSKNO){
		 	$scope.storedTask = TSTSKNO;
		 	for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  $scope.Process.Tasks[i].selected = false;
			  if($scope.Process.Tasks[i].TSTSKNO == $scope.storedTask){
				  $scope.Process.Tasks[i].selected = true;
				  $scope.storedStatus = $scope.Process.Tasks[i].Status;
				  if($scope.storedStatus == "0"){
					  $scope.dynamicPopover.templateUrl = 'inProcessPopver.html';
				  }
				  if($scope.storedStatus == "1"){
					  $scope.dynamicPopover.templateUrl = 'completePopver.html'; 
				  }
				  if($scope.storedStatus == "5"){
					  $scope.dynamicPopover.templateUrl = 'pausedPopver.html';
				  }
			  }
		 	}
		  }
	  };
	  
	  $scope.startTask = function(){
		  
	  };
	  
	  $scope.loadTaskTemplates = function(){
		  var array = [];
		  for(var i = 0; i < $scope.Process.Tasks.length; i ++){
			  array.push($scope.Process.Tasks[i].TSTSKNO);
		  }
		  var largest = Math.max.apply(Math, array);
		  var numero = largest+1;
		  $scope.tmp = {TSTKNO:$scope.Process.TKNO,
				  		TSTSKNO:numero,
				  		Status: "0",
				  		StatusIcon: "./images/tasks/Red_start.png",
				  		Depend: "N",
				  		Mode: "I"};
		  $scope.Process.Tasks.push($scope.tmp);
		  setTimeout(function(){
			  $scope.resetFields(numero);
		  }, 100);
	  };
	  
	  $scope.resetFields = function(numero){
		  var field = $('#Desc'+numero);
		  field.attr("type","input");
		  var compileTxt = "<input id='Desc"+numero+"sub' value='' style='width:100%;color:black;' ng-blur='removeDescBox("+numero+")'/></input>";
		  var compileDone = $compile(compileTxt)($scope);
		  field.html(compileDone);
		  field = $('#Assigned'+numero);
		  field.attr("type","input");
		  compileTxt = "<select id='Assigned"+numero+"sub' style='width:100%;color:black;' ng-model='selected.text"+numero+"' ng-options='opt as opt for opt in names' ng-change='removeAssignBox("+numero+")'/></select>";
		  compileDone = $compile(compileTxt)($scope);
		  field.html(compileDone);
		  field = $('#Start'+numero);
		  field.attr("type","input");
		  compileTxt = "<input type='date' id='Start"+numero+"sub' ng-model='dtS"+numero+"' is-open='true' style='color:black;' ng-blur='updateStart("+numero+")'/></input>";
		  compileDone = $compile(compileTxt)($scope);
		  field.html(compileDone);
		  field = $('#End'+numero);
		  field.attr("type","input");
		  compileTxt = "<input type='date' id='End"+numero+"sub' ng-model='dtE"+numero+"' is-open='true' style='color:black;' ng-blur='updateEnd("+numero+")'/></input>";
		  compileDone = $compile(compileTxt)($scope);
		  field.html(compileDone);
		  field = $('#Est'+numero);
		  field.attr("type","input");
		  compileTxt = "<input id='Est"+numero+"sub' value='' style='width:100%;color:black;' ng-blur='removeEstBox("+numero+")'/></input>";
		  compileDone = $compile(compileTxt)($scope);
		  field.html(compileDone);
	  };
	  
	  $scope.editTitle =  function(){
		  var field = $('#pageheader');
		  if(field.attr("type") != "input"){
			  field.attr("type","input");
			  var compileTxt = "<input id='pageheadersub' value='"+$scope.Process.ShortDesc+"' style='width:50%;color:black;' ng-blur='removeEditTitle()' autofocus/></input>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeEditTitle = function(){
		  var field = $('#pageheader');
		  var subField = $('#pageheadersub');
		  field.attr("type","");
		  var text = subField.val();
		  $scope.Process.ShortDesc = text;
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.titleChange =  function(TSTSKNO, text){
		  var field = $('#Desc'+TSTSKNO);
		  var comp = false;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				 if($scope.Process.Tasks[i].Status == "1"){
					 comp = true;
				 } 
			  }
		  }
		  if(field.attr("type") != "input" && !comp){
			  field.attr("type","input");
			  var compileTxt = "<input id='Desc"+TSTSKNO+"sub' value='"+text+"' style='width:100%;color:black;' ng-blur='removeDescBox("+TSTSKNO+")'/></input>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeDescBox = function(TSTSKNO){
		  var field = $('#Desc'+TSTSKNO);
		  var subField = $('#Desc'+TSTSKNO+'sub');
		  field.attr("type","");
		  var text = subField.val();
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				  $scope.Process.Tasks[i].Desc = text;
			  }
		  }
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.reassign = function(TSTSKNO, textin){
		  var field = $('#Assigned'+TSTSKNO);
		  var string = "text"+TSTSKNO;
		  $scope.selected[string] = textin;
		  var comp = false;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				 if($scope.Process.Tasks[i].Status == "1"){
					 comp = true;
				 } 
			  }
		  }
		  if(field.attr("type") != "input" && !comp){
			  field.attr("type","input");
			  var compileTxt = "<select id='Assigned"+TSTSKNO+"sub' style='width:100%;color:black;' ng-model='selected.text"+TSTSKNO+"' ng-options='opt as opt for opt in names' ng-change='removeAssignBox("+TSTSKNO+")'/></select>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeAssignBox = function(TSTSKNO){
		  var field = $('#Assigned'+TSTSKNO);
		  var subField = $('#Assigned'+TSTSKNO+'sub');
		  field.attr("type","");
		  var string = "text"+TSTSKNO;
		  var text = $scope.selected[string];
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				  $scope.Process.Tasks[i].Assigned = text;
			  }
		  }
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.openPopup = function(TSTSKNO){
		  
//		  $scope.popup.opened = true;
	  };
	  
	  $scope.openStart = function(TSTSKNO){
		  var field = $('#Start'+TSTSKNO);
		  var comp = false;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				  	var string = "dtS"+TSTSKNO;
				  	$scope[string] = new Date($scope.Process.Tasks[i].ActStartDate);
				  	if($scope.Process.Tasks[i].Status == "1"){
				  		comp = true;
				  	}
			  }
		  }
		  if(field.attr("type") != "input" && !comp){
			  field.attr("type","input");
			  var compileTxt = "<input type='date' id='Start"+TSTSKNO+"sub' ng-model='dtS"+TSTSKNO+"' is-open='true' style='color:black;' ng-blur='updateStart("+TSTSKNO+")'/></input>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.updateStart = function(TSTSKNO){
		  var field = $('#Start'+TSTSKNO);
		  var subField = $('#Start'+TSTSKNO+'sub');
		  field.attr("type","");
		  var string = "dtS"+TSTSKNO;
		  var text = $filter('date')($scope[string]);
		  if(text != "Invalid Date"){
			  for(var i = 0; i < $scope.Process.Tasks.length; i++){
				  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
					  $scope.Process.Tasks[i].ActStartDate = $scope[string];
					  $scope.Process.Tasks[i].ActStartDateFmt = text;
				  }
			  }
		  }
		  subField.remove();
		  if(text != "Invalid Date"){
			  field.html(text);
		  } else {
			  field.html("");
		  }
	  };
	  
	  $scope.openEnd = function(TSTSKNO){
		  var field = $('#End'+TSTSKNO);
		  var comp = false;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				  var string = "dtE"+TSTSKNO;
				  $scope[string] = new Date($scope.Process.Tasks[i].ActCompDate);
				  if($scope.Process.Tasks[i].Status == "1"){
					  comp = true;
				  }
			  }
		  }
		  if(field.attr("type") != "input" && !comp){
			  field.attr("type","input");
			  var compileTxt = "<input type='date' id='End"+TSTSKNO+"sub' ng-model='dtE"+TSTSKNO+"' is-open='true' style='color:black;' ng-blur='updateEnd("+TSTSKNO+")'/></input>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.updateEnd = function(TSTSKNO){
		  var field = $('#End'+TSTSKNO);
		  var subField = $('#End'+TSTSKNO+'sub');
		  field.attr("type","");
		  var string = "dtE"+TSTSKNO;
		  var text = $filter('date')($scope[string]);
		  if(text != "Invalid Date"){
			  for(var i = 0; i < $scope.Process.Tasks.length; i++){
				  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
					  $scope.Process.Tasks[i].ActCompDate = $scope[string];
					  $scope.Process.Tasks[i].ActCompDateFmt = text;
				  }
			  }
		  }
		  subField.remove();
		  if(text != "Invalid Date"){
			  field.html(text);
		  } else {
			  field.html("");
		  }
	  };
	  
	  $scope.estChange =  function(TSTSKNO, text){
		  var field = $('#Est'+TSTSKNO);
		  var comp = false;
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				 if($scope.Process.Tasks[i].Status == "1"){
					 comp = true;
				 } 
			  }
		  }
		  if(field.attr("type") != "input" && !comp){
			  field.attr("type","input");
			  var compileTxt = "<input id='Est"+TSTSKNO+"sub' value='"+text+"' style='width:100%;color:black;' ng-blur='removeEstBox("+TSTSKNO+")'/></input>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeEstBox = function(TSTSKNO){
		  var field = $('#Est'+TSTSKNO);
		  var subField = $('#Est'+TSTSKNO+'sub');
		  field.attr("type","");
		  var text = subField.val();
		  for(var i = 0; i < $scope.Process.Tasks.length; i++){
			  if($scope.Process.Tasks[i].TSTSKNO == TSTSKNO){
				  $scope.Process.Tasks[i].EstHours = text;
			  }
		  }
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.loadUDFs = function(){
		  $http({ 
	    		url:"./rest/AdminService/getRequiredFields",
	    		method: "POST",
	    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( response ) {
	    		$scope.fields = response.data[0];
	    		$scope.ufields = response.data[1];
	    		var count = 0;
	    		var addlcount = 0;
	    		var appender = "required"+ $scope.numRequired;
	    		for(var i = 0; i < 10; i++){
	    			var string = "required"+i;
	    			$scope[string] = [];
	    		}

	    		for(var i = 0; i < $scope.fields.length; i++){
	    			if($scope.fields[i].DFREQ == "Y"){
	    				switch($scope.fields[i].DFNAMID) {
	    					case "Start Date":
	    						if($scope.Process.ActStartDate != ""){
	    							$scope.tmp = {
	    									Field:"Start Date",
	    									Data: $filter('date')($scope.Process.ActStartDate),
	    									Type:"",
	    									Sort:"StartDate"
	    							};
	    							$scope[appender].push($scope.tmp);
	    							count++;
	    						} else {
	    							$scope.tmp = {
	    									Field:"Start Date",
	    									Data:$filter('date')($scope.Process.EstStartDate),
	    									Type:"",
	    									Sort: "StartDate"
	    							};
	    							$scope[appender].push($scope.tmp);
	    							count++;
	    						}
	    						break;		
	    					default :
	    						var string = $scope.fields[i].DFNAMID.replace(/\s/g, '');
	    						$scope.tmp = {
	    								Field:$scope.fields[i].DFNAMID,
	    								Data:$scope.Process[string],
	    								Type:"",
	    								Sort:string
	    						};
	    						$scope[appender].push($scope.tmp);
	    						count++;
	    						break;
	    				}
	    				if(count == 5){
	    					count = 0;
	    					$scope.numRequired++;
	    					appender = "required"+ $scope.numRequired;
	    				}
	    			}
	    		}
	    		for(var i = 0; i < $scope.ufields.length; i++){
	    			if($scope.ufields[i].UREQ == "Y"){
	    				var string = $scope.ufields[i].UNAMID.replace(/\s/g, '');
	    				var found = false;
	    				for(var k = 0; k < $scope.Process.UDFs.length; k++){
	    					if($scope.Process.UDFs[k].DSEQ == $scope.ufields[i].USEQ){
	    						found = true;
	    						$scope.tmp = {
	    								Field:$scope.ufields[i].UNAMID,
	    								Data:$scope.Process.UDFs[k].DATA,
	    								Type:$scope.ufields[i].UTYPE,
	    								Sort:string,
	    								DSVAL:$scope.Process.UDFs[k].DSVAL
	    						};
	    						$scope[appender].push($scope.tmp);
	    						count++;
	    					}
	    				}
	    				if(!found){
	    					$scope.tmp = {
    								Field:$scope.ufields[i].UNAMID,
    								Data:"",
    								Type:$scope.ufields[i].UTYPE,
    								Sort:string,
    								DSVAL:""
    						};
    						$scope[appender].push($scope.tmp);
    						count++;
	    				}
	    				if(count == 5){
	    					count = 0;
	    					$scope.numRequired++;
	    					appender = "required"+ $scope.numRequired;
	    				}
	    			}
	    		}
	    		$scope.loadDropDowns();
	    	});
	  };
	  
	  $scope.handleClick =  function(FROMin, data){
		  $scope.procField = FROMin;
		  var field = $('#'+FROMin);
		  var compileTxt = "";
		  if(field.attr("type") != "input"){
			  $scope.pinfo = {};
			  field.attr("type","input");
			  if(FROMin == "StartDate" || FROMin == "EndDate"){
				  if(FROMin == "StartDate"){
					  if($scope.Process.ActStartDate != ""){
						  $scope.dtS = new Date($scope.Process.ActStartDate);
					  } else {
						  $scope.dtS = new Date($scope.Process.EstStartDate);
					  }
					  compileTxt = "<input type='date' id='"+FROMin+"sub' ng-model='dtS' is-open='true' style='color:black;' ng-blur='updateProcDate()'/></input>";
				  } else {
					  $scope.dtE = new Date($scope.Process.EstCompDate);
					  compileTxt = "<input type='date' id='"+FROMin+"sub' ng-model='dtE' is-open='true' style='color:black;' ng-blur='updateProcDate()'/></input>";
				  }
			  } else {
				  switch(FROMin){
				  	case "ProcessType" :
				  		for(var i = 0; i < $scope.ProcTypes.length; i ++){
				  			if($scope.ProcTypes[i].TKTMPNM == data){
				  				$scope.proctype = $scope.ProcTypes[i];
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='proctype' ng-options='opt as opt.TKTMPNM for opt in ProcTypes' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "ProcessSubType" :
				  		for(var i = 0; i < $scope.SubProcTypes.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubProcTypes[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubProcTypes[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubProcTypes' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "Frequency" :
				  		for(var i = 0; i < $scope.Frequencies.length; i ++){
				  			if(data != undefined){
				  				if($scope.Frequencies[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.Frequencies[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in Frequencies' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "Category" :
				  		for(var i = 0; i < $scope.Cats.length; i ++){
				  			if(data != undefined){
				  				if($scope.Cats[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.Cats[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in Cats' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory" :
				  		for(var i = 0; i < $scope.SubCats.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCats[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCats[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCats' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory2" :
				  		for(var i = 0; i < $scope.SubCat2s.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCat2s[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCat2s[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCat2s' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory3" :
				  		for(var i = 0; i < $scope.SubCat3s.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCat3s[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCat3s[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCat3s' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	default :
				  		compileTxt = "<input id='"+FROMin+"sub' value='"+data+"' style='width:100%;color:black;' ng-blur='removeClick()'/></input>"; 
				  		break;
				  }
			  }
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.handleClickDropdown =  function(FROMin, data){
		  $scope.procField = FROMin;
		  var field = $('#'+FROMin);
		  var compileTxt = "";
		  if(field.attr("type") != "input"){
			  $scope.pinfo = {};
			  $scope.fields = [];
			  field.attr("type","input");
			  var inter = 0;
			  for(var i = 0; i < $scope.ufields.length; i ++){
				  var string = $scope.ufields[i].UNAMID.replace(/\s/g, '');
				  if(string == FROMin){
					  inter = $scope.ufields[i].USEQ;
				  }
			  }
			  for(var i = 0; i < $scope.UDFVals.length; i++){
				  if($scope.UDFVals[i].DPSEQ == inter){
					  $scope.fields.push($scope.UDFVals[i]);
				  }
			  }
			  compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.DSVAL for opt in fields' ng-blur='removeClickedDropdown()'/></select>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeClick = function(){
		  var field = $('#'+ $scope.procField);
		  var subField = $('#'+ $scope.procField+'sub');
		  field.attr("type","");
		  var text = subField.val();
		  for(var i = 0; i < $scope.fields.length; i++){
			  var string = $scope.fields[i].DFNAMID.replace(/\s/g, '');
			  if(string == $scope.procField){
				  $scope.Process[string] = text;
			  }
		  }
		  for(var i = 0; i < $scope.ufields.length; i++){
			  var string = $scope.ufields[i].UNAMID.replace(/\s/g, '');
			  if(string ==  $scope.procField){
				  var found = false;
				  for(var k = 0; k < $scope.Process.UDFs.length; k++){
  					if($scope.Process.UDFs[k].DSEQ == $scope.ufields[i].USEQ){
  						found = true;
  						$scope.Process.UDFs[k].DATA = text;
  						$scope.Process.UDFs[k].Mode = "U";
  					}
				  }
				  if(!found){
					  $scope.tmp = {
							  	DTKNO: $scope.Process.TKNO,
					    		DTSKNO: 0,
					    		DTSKSNO: 0,
					    		DSEQ: 0,
					    		DATA: text,
					    		DSVAL: "",
					    		Mode: "I"
					  }
					  $scope.Process.UDFs.push($scope.tmp);
				  }
			  }
		  }
		  subField.remove();
		  field.html(text);
		  $scope.procField = "";
	  };
	  
	  $scope.removeDropdown = function(){
		  var field = $('#'+ $scope.procField);
		  var subField = $('#'+ $scope.procField+'sub');
		  field.attr("type","");
		  var text = "";
		  if($scope.procField == "ProcessType"){
			  text = $scope.proctype.TKTMPNM;
		  } else {
			  text = $scope.pinfo.SIFFUL;
		  }
		  $scope.Process[$scope.procField] = text;
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.removeClickedDropdown = function(){
		  var field = $('#'+ $scope.procField);
		  var subField = $('#'+ $scope.procField+'sub');
		  field.attr("type","");
		  var text = "";
		  text = $scope.pinfo.DSVAL;
		  var found = false;
		  for(var i = 0; i < $scope.Process.UDFs.length; i++){
			  if($scope.pinfo.DPSEQ == $scope.Process.UDFs[i].DSEQ){
				  found = true;
				  $scope.Process.UDFs[i].DATA = "'"+$scope.pinfo.DSSEQ+"'";
				  $scope.Process.UDFs[i].MODE = "U";			  }
		  }
		  if(!found){
			  $scope.tmp = {
					  	DTKNO: $scope.Process.TKNO,
			    		DTSKNO: 0,
			    		DTSKSNO: 0,
			    		DSEQ: 0,
			    		DATA: "'" + $scope.pinfo.DSSEQ+"'",
			    		DSVAL: text,
			    		Mode: "I"
			  }
		  }
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.updateProcDate = function(){
		  var field = $('#'+$scope.procField);
		  var subField = $('#'+$scope.procField+'sub');
		  field.attr("type","");
		  var text = "";
		  if($scope.procField == "StartDate"){
			  $scope.Process.ActStartDate = $scope.dtS;
			  text = $filter('date')($scope.dtS);
		  } else {
			  $scope.Process.EstCompDate = $scope.dtE;
			  text = $filter('date')($scope.dtE);
		  }
		  subField.remove();
		  field.html(text);
		  $scope.procField = "";
	  };
	  
	  $scope.getDispNums = function(){
		  return new Array($scope.numRequired);  
	  };
	  
	  $scope.getName = function(number){
		  var newNum = number+1;
		  var appender = "required"+newNum;
		  return $scope[appender];
	  };
	  
	  $scope.loadDropDowns = function(){
		  $http({ 
	    		url:"./rest/ProcessMainService/getPINFOCodes",
	    		method: "POST",
	    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( response ) {
	    		$scope.SubProcTypes = response.data[0];
	    		$scope.Cats = response.data[1];
	    		$scope.SubCats = response.data[2];
	    		$scope.SubCat2s = response.data[3];
	    		$scope.SubCat3s = response.data[4];
	    		$scope.Frequencies = response.data[5];
	    		$scope.ProcTypes = response.data[6];
	    		$scope.UDFVals = response.data[7];
	    	});
	  };
	  
	  $scope.ReassignMain = function(textin){
		  var field = $('#Assigned');
		  $scope.selected.text = $scope.Process.AssignedTo;
		  if(field.attr("type") != "input"){
			  field.attr("type","input");
			  var compileTxt = "<select id='Assignedsub' style='width:100%;color:black;' ng-model='selected.text' ng-options='opt as opt for opt in names' ng-blur='removeAssignMain()'/></select>";
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeAssignMain = function(){
		  var field = $('#Assigned');
		  var subField = $('#Assignedsub');
		  field.attr("type","");
		  var text = $scope.selected.text;
		  $scope.Process.AssignedTo = text;
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.setValid = function(){
		  // TODO if we're holding, add a note
		  
		  if($scope.note.storedText != $scope.Process.RecentNote){
			  $scope.Process.newNote = $scope.note.storedText;
		  }
		  for(var i = 0; i < $scope.jobfunctionMasters.length; i++){
			  if($scope.Process.AssignedTo == $scope.jobfunctionMasters[i].name){
				  $scope.Process.AssignedTo = $scope.jobfunctionMasters[i].role;
			  }
			  var pausedFound = false;
			  var count = 1;
			  for(var k = 0; k < $scope.displayedProcess.length; k++){
				  $scope.displayedProcess[k].Order = count;
				  if($scope.displayedProcess[k].Assigned != undefined){
					  if($scope.displayedProcess[k].Assigned == $scope.jobfunctionMasters[i].name){
						  $scope.displayedProcess[k].Assigned = $scope.jobfunctionMasters[i].role;
					  }
				  }
				  if($scope.Process.Status == "5"){
					  if($scope.displayedProcess[k].Status == "5"){
						  pausedFound = true;
					  }
				  }
				  count ++;
			  }
			  if(!pausedFound){
				  if($scope.Process.Status == "5"){
					  $scope.Process.Status = "0";
				  }
			  }
		  }
		  $scope.Process.Tasks = $scope.displayedProcess;
		  //Frequencies
		  for(var key in $scope.Frequencies){
			  if($scope.Process.Frequency == $scope.Frequencies[key].SIFFUL){
				  $scope.Process.Freq = $scope.Frequencies[key].SIFCD;
			  }
		  }
		  $scope.Process.SubType = "";
		  for(var key in $scope.SubProcTypes){
			  if($scope.Process.ProcessSubType == $scope.SubProcTypes[key].SIFFUL){
				  $scope.Process.SubType = $scope.SubProcTypes[key].SIFCD;
			  }
		  }
		  $scope.Process.Cat = "";
		  for(var key in $scope.Cats){
			  if($scope.Process.Category == $scope.Cats[key].SIFFUL){
				  $scope.Process.Cat = $scope.Cats[key].SIFCD;
			  }
		  }
		  $scope.Process.SubCat = "";
		  for(var key in $scope.SubCats){
			  if($scope.Process.SubCategory == $scope.SubCats[key].SIFFUL){
				  $scope.Process.SubCat = $scope.SubCats[key].SIFCD;
			  }
		  }
		  $scope.Process.SubCat2 = "";
		  for(var key in $scope.SubCat2s){
			  if($scope.Process.SubCategory2 == $scope.SubCat2s[key].SIFFUL){
				  $scope.Process.SubCat2 = $scope.SubCat2s[key].SIFCD;
			  }
		  }
		  $scope.Process.SubCat3 = "";
		  for(var key in $scope.SubCat3s){
			  if($scope.Process.SubCategory3 == $scope.SubCat3s[key].SIFFUL){
				  $scope.Process.SubCat3 = $scope.SubCat3s[key].SIFCD;
			  }
		  }
		  for(var key in $scope.ProcTypes){
			  if($scope.Process.ProcessType == $scope.ProcTypes[key].TKTMPNM){
				  $scope.Process.TKTYPE = $scope.ProcTypes[key].TKTYPE;
			  }
		  }
//		  for(var i = 1; i <= $scope.numRequired; i++){
//			  var string = "required"+i;
//			  for(var k = 0; k < $scope[string].length; k++){
//				  for(var j = 0; j < $scope.ufields.length; j++){
//					  if($scope[string][k].Field == $scope.ufields[j].UNAMID){
//						  var found = false;
//						  for(var l = 0; l < $scope.Process.UDFs.length; l++){
//							  if($scope.Process.UDFs[l].DSEQ == $scope.ufields[j].USEQ){
//								  found = true;
//								  $scope.Process.UDFs[l].DATA = $scope[string][k].Data;
//							  }
//						  }
//					  }  
//				  }
//			  }
//		  }
		  
		  $http({ 
				url:"./rest/ProcessMainService/updateProcess",
				method: "POST",
				data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "Process":$scope.Process},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.reloadOnUpdate();
			});
	  };
	  
	  $scope.reloadOnUpdate = function(){
		  $http({ 
				url:"./rest/ProcessMainService/getProcess",
				method: "POST",
				data: {"lib":$sessionStorage.datasource, "user":$sessionStorage.userid, "TKNO":$sessionStorage.KEYN1},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.Process = response.data;
				if($scope.Process.RecentNote != undefined){
					$scope.note.storedText = $scope.Process.RecentNote;
				}
				$scope.Process.EstCompDateFmt = $filter('date')($scope.Process.EstCompDate);
				for(var i = 0; i < $scope.Process.Tasks.length; i++){
					$scope.Process.Tasks[i].selected = false;
					switch($scope.Process.Tasks[i].Status){
						case "0":
							if($scope.Process.Tasks[i].ActStartDate != ""){
								$scope.Process.Tasks[i].StatusIcon = "./images/tasks/inProcess_small.png";
								$scope.Process.Tasks[i].showPopover = true;
							} else {
								$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Red_start.png";
								$scope.Process.Tasks[i].showPopover = false;
								$scope.Process.Tasks[i].Status = "6";
							}
							break;
						case "1":
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Green_check.png";
							break;
						case "3":
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_inactive.png";
							break;
						case "4":
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Gray_clock.png";
							break;
						case "5":
							$scope.Process.Tasks[i].StatusIcon = "./images/tasks/Red_pause.png";
							break;
					}
					$scope.Process.Tasks[i].ActStartDateFmt = $filter('date')($scope.Process.Tasks[i].ActStartDate);
					$scope.Process.Tasks[i].ActCompDateFmt = $filter('date')($scope.Process.Tasks[i].ActCompDate);
				}
				$scope.displayedProcess = [].concat($scope.Process.Tasks);	
				switch($scope.Process.Status){
					case "0":
						if($scope.Process.Days < 0){
							$scope.processSource = "./images/tasks/TriangleExc.png";
							$scope.statusColor = "red";
							$scope.statusInfo = "Process is " + $rootScope.ProcessDays + " Behind Schedule";
						} else {
							$scope.processSource = "./images/tasks/inProcess.png";
							$scope.statusColor = "black";
							$scope.statusInfo = "Process running on schedule";
						}
						break;
					case "5":
						$scope.processSource = "./images/tasks/Red_pause_big.png";
						$scope.statusColor = "gray";
						$scope.statusInfo = "Process is currently paused";
						break;
				}
				for(var k = 0; k < $scope.jobfunctionMasters.length; k++){
					if($scope.Process.AssignedTo.trim() == $scope.jobfunctionMasters[k].role.trim()){
						$scope.Process.AssignedTo = $scope.jobfunctionMasters[k].name.trim();
					}
					for(var i = 0; i < $scope.Process.Tasks.length; i++){
						if($scope.Process.Tasks[i].Assigned.trim() == $scope.jobfunctionMasters[k].role.trim()){
							$scope.Process.Tasks[i].Assigned = $scope.jobfunctionMasters[k].name.trim();
						}
					}
				}
			});
	  };
	  
	  $scope.addlInfoClick = function(){
		  var modalInstanceProc = $uibModal.open({
			  animation: true,
		      templateUrl: './pages/modals/addlInfoModal.html',
		      controller: 'AddlInfoCtrl',
		      resolve: {
		    	  Process: function () {
			        	return $scope.Process;
				    },
				  SubProcTypes: function(){
					  return $scope.SubProcTypes;
				  },
				  Cats: function(){
					  return $scope.Cats;
				  },
				  SubCats: function(){
					  return $scope.SubCats;
				  },
				  SubCat2s: function(){
					  return $scope.SubCat2s;
				  },
				  SubCat3s: function(){
					  return $scope.SubCat3s;
				  },
				  Frequencies: function(){
					  return $scope.Frequencies;
				  },
				  ProcTypes: function(){
					  return $scope.ProcTypes;
				  },
				  Fields: function(){
					  return $scope.fields;
				  },
				  UFields: function(){
					  return $scope.ufields;
				  }
		      }
		    });
		modalInstanceProc.result.then(function(Process){
			  $scope.Process = Process;
			  console.log($scope.Process);
		  });  
	  };
	  
	  
	$scope.init();
});