angular.module('cobraApp').controller('EditWidgetModalCtrl', function ($scope, $uibModalInstance, editwidget, sessionStorage) {
	
	$scope.editwidget = editwidget;
	$scope.jobfunctions =[];
	$scope.groups = [];
	$scope.processtypes = [];
	$scope.categories = [];
	$scope.tasktypes = [];
	$scope.columns = [];
	$scope.ucolumns = [];
	$scope.mytitle = {text: $scope.editwidget.Title};
	$scope.myjobfunction = {};
	$scope.myprocesstype = {};
	$scope.mytasktype = {};
	$scope.mygroup = {};
	$scope.mycategory = {};
	$scope.mycolumns = [];
	$scope.column = {};
	$scope.Types = [{type: "Task"},{type: "Process"}];
	$scope.editPorT = {};
	$scope.status = [{type: 0, name: "Open"},{type:1, name: "Completed"}, {type:2, name: "All"}];
	$scope.mystatus = {type:0,name:""};
	$scope.mycolor = "#ff7373";
	$scope.mynotstart = false;
	$scope.data = {};
	
	$scope.isEditProcess = false;
	  
	$.getJSON( "./rest/ProcessTaskService/getEditWidget/" + sessionStorage.datasource +  "/" + sessionStorage.userid + "/" + sessionStorage.sessionID + "/" + sessionStorage.sessionToken + "/" + sessionStorage.sessionTimeout + "/" + $scope.editwidget.SeqNum, function( data ) {

		$scope.jobfunctions = data[0];
		$scope.groups = data[1];
		$scope.processtypes = data[2];
		$scope.categories = data[3];
		$scope.tasktypes = data[5]; 
		$scope.mycolumns = data[6];
		$scope.columns = data[7];
		$scope.ucolumns = data[8];
		
		for(var i = 0; i < $scope.jobfunctions.length; i++){
			if($scope.jobfunctions[i].KAJOBROLE == $scope.editwidget.Assigned.trim()){
				$scope.myjobfunction = $scope.jobfunctions[i];
			}
		}
		
		for(var i = 0; i < $scope.groups.length; i++){
			if($scope.groups[i].GRPNAME == $scope.editwidget.Role.trim()){
				$scope.mygroup = $scope.groups[i];
			}
		}

		for(var i = 0; i < $scope.processtypes.length; i++){
		  	if($scope.processtypes[i].TKTYPE == $scope.editwidget.ProcessType){
		  		$scope.myprocesstype = $scope.processtypes[i];
		  	}
		}


		for(var i = 0; i < $scope.tasktypes.length; i++){
		  	if($scope.tasktypes[i].TSTYPE == $scope.editwidget.ProcessType){
		  		$scope.mytasktype = $scope.tasktypes[i];
		  	}
		}
		
		for(var i = 0; i < $scope.categories.length; i++){
		  	if($scope.categories[i].SIFCD == $scope.editwidget.Cat1.trim()){
		  		$scope.mycategory = $scope.categories[i];
		  	}
		}
		
		if($scope.editwidget.SeqNum == 0){
			for(var i = 0; i < $scope.columns.length; i++){
				if($scope.columns[i].DFDISP == "Y"){
					$scope.columns[i].checked = true;
				}
			}
			for(var i = 0; i < $scope.ucolumns.length; i++){
				if($scope.ucolumns[i].UDISP == "Y"){
					$scope.ucolumns[i].checked = true;
				}
			}
		} else {
			for(var i = 0; i < $scope.columns.length; i++){
				$scope.columns[i].checked = false;
				for(var k = 0; k < $scope.mycolumns.length; k++) {
					if($scope.columns[i].DFNAMID == $scope.mycolumns[k].NAMID){
						$scope.columns[i].checked = true;
					}
				}
			}
			for(var i = 0; i < $scope.ucolumns.length; i++){
				$scope.ucolumns[i].checked = false;
				for(var k = 0; k < $scope.mycolumns.length; k++) {
					if($scope.ucolumns[i].UNAMID == $scope.mycolumns[k].NAMID){
						$scope.ucolumns[i].checked = true;
					}
				}
			}
		}

		if($scope.editwidget.PorT != ""){
			$scope.editPorT.type = $scope.editwidget.PorT;
			if($scope.editwidget.PorT == "Task"){
				$scope.isEditProcess = false;
			} else {
				$scope.isEditProcess = true;
			}
		} else {
			$scope.editPorT = "Task";
			$scope.isEditProcess = false;
		}
		$scope.mycolor = $scope.editwidget.Color;
		$scope.mystatus = $scope.status[$scope.editwidget.status];
		if($scope.editwidget.ShowNotStart == "" || $scope.editwidget.ShowNotStart == "N"){
			$scope.mynotstart = false;
		} else {
			$scope.mynotstart = true;
		}
		$scope.data ={
				myport : $scope.editPorT,
				myjobfunction : $scope.myjobfunction,
				mygroup : $scope.mygroup,
				myprocesstype : $scope.myprocesstype,
				mytasktype : $scope.mytasktype,
				mycategory : $scope.mycategory,
				mycolor : $scope.mycolor,
				mystatus : $scope.mystatus,
				mynotstart : $scope.mynotstart
		}
		$scope.$apply();
	});
	  	
	$scope.ok = function () {
		$scope.editwidget.Title = $scope.mytitle.text;
		if($scope.data.myjobfunction.KAJOBROLE == undefined){
			$scope.editwidget.Assigned =  "";
		} else {
			$scope.editwidget.Assigned = $scope.data.myjobfunction.KAJOBROLE.trim();
		}
		if($scope.data.myport.type == "Task"){
			$scope.editwidget.PorT = "T";
			$scope.editwidget.ProcessType = $scope.data.mytasktype.TSTYPE;
		} else{
			$scope.editwidget.PorT = "P";
			$scope.editwidget.ProcessType = $scope.data.myprocesstype.TKTYPE;
		}
		if($scope.data.mygroup == null){
			$scope.editwidget.Role =  "";
		} else {
			$scope.editwidget.Role = $scope.data.mygroup.GRPNAME.trim();
		}
		if($scope.data.mycategory == null){
			$scope.editwidget.Cat1 =  "";
		} else {
			$scope.editwidget.Cat1 = $scope.data.mycategory.SIFCD;
		}
		$scope.editwidget.Color = $scope.data.mycolor.substring(1,$scope.data.mycolor.length);
		 if($scope.editwidget.SeqNum == 0){
			 $scope.editwidget.Mode = "I";
		 } else { 
			 $scope.editwidget.Mode = "U";
		 }
		 $scope.editwidget.columns = [];
		 $scope.columnsOut = [];
		 
		 
		 for(var i = 0; i < $scope.columns.length; i++){
			 if($scope.columns[i].checked){
				 $scope.column = {
						DSSEQ : $scope.editwidget.SeqNum,
						DSORDER : 0,
						DSTYPE : "F",
						DSSEQNUM : $scope.columns[i].DFSEQ,
						NAMID : $scope.columns[i].DFNAMID.trim(),
						WIDTH : $scope.columns[i].DFWIDTH,
				}
				$scope.columnsOut.push($scope.column);
			}
		 }
		 for(var i = 0; i < $scope.columnsOut.length; i++){
			$scope.columnsOut[i].DSORDER = i+1; 
		 }
		 $scope.editwidget.columns = $scope.columnsOut;
		$scope.editwidget.status = $scope.data.mystatus.type; 
		if($scope.data.mynotstart){
			$scope.editwidget.ShowNotStart = "Y";
		} else {
			$scope.editwidget.ShowNotStart = "N";
		}
		var JSONResponse = angular.toJson($scope.editwidget);
		$uibModalInstance.close(JSONResponse);
	  };
	  
	  $scope.updateProcess = function(){
		  if($scope.data.myport == "Process"){
			  $scope.isEditProcess = true;
		  } else {
			  $scope.isEditProcess = false;
		  }
	  };

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };
});