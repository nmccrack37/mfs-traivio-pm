angular.module('cobraApp').controller('AddlInfoCtrl', function ($scope, $filter, $uibModalInstance, $compile, Process, SubProcTypes, Cats, SubCats, SubCat2s, SubCat3s,
		Frequencies, ProcTypes, Fields, UFields) {
	
	$scope.Process = Process;
	$scope.SubProcTypes = SubProcTypes;
	$scope.Cats = Cats;
	$scope.SubCats = SubCats;
	$scope.SubCat2s = SubCat2s;
	$scope.SubCat3s = SubCat3s;
	$scope.Frequencies = Frequencies;
	$scope.ProcTypes = ProcTypes;
	$scope.fields = Fields;
	$scope.ufields = UFields;
	$scope.numAddl = 1;
	
	var count = 0;
	var appender = "addl"+ $scope.numAddl;
	for(var i = 0; i < 10; i++){
		var string = "addl"+i;
		$scope[string] = [];
	}

	for(var i = 0; i < $scope.fields.length; i++){
		if($scope.fields[i].DFREQ != "Y"){
			switch($scope.fields[i].DFNAMID) {
				case "Start Date":
					if($scope.Process.ActStartDate != ""){
						$scope.tmp = {
							Field:"Start Date",
							Data: $filter('date')($scope.Process.ActStartDate),
							Type:"",
							Sort:"StartDate"
					};
					$scope[appender].push($scope.tmp);
					count++;
				} else {
					$scope.tmp = {
							Field:"Start Date",
							Data:$filter('date')($scope.Process.EstStartDate),
							Type:"",
							Sort: "StartDate"
					};
					$scope[appender].push($scope.tmp);
					count++;
				}
				break;		
			default :
				if($scope.fields[i].DFNAMID != undefined){
					var string = $scope.fields[i].DFNAMID.replace(/\s/g, '');
					$scope.tmp = {
						Field:$scope.fields[i].DFNAMID,
						Data:$scope.Process[string],
						Type:"",
						Sort:string
					};
					$scope[appender].push($scope.tmp);
					count++;
				}
					break;
		}
		if(count == 3){
			count = 0;
			$scope.numAddl++;
			appender = "addl"+ $scope.numAddl;
		}
		}
	}
	for(var i = 0; i < $scope.ufields.length; i++){
		if($scope.ufields[i].UREQ != "Y"){
			var string = $scope.ufields[i].UNAMID.replace(/\s/g, '');
			var found = false;
			for(var k = 0; k < $scope.Process.UDFs.length; k++){
				if($scope.Process.UDFs[k].DSEQ == $scope.ufields[i].USEQ){
					found = true;
					$scope.tmp = {
							Field:$scope.ufields[i].UNAMID,
							Data:$scope.Process.UDFs[k].DATA,
							Type:$scope.ufields[i].UTYPE,
							Sort:string
					};
					$scope[appender].push($scope.tmp);
					count++;
				}
			}
			if(!found){
				$scope.tmp = {
						Field:$scope.ufields[i].UNAMID,
						Data:"",
						Type:$scope.ufields[i].UTYPE,
						Sort:string
				};
				$scope[appender].push($scope.tmp);
				count++;
			}
			if(count == 5){
				count = 0;
				$scope.numAddl++;
				appender = "addl"+ $scope.numAddl;
			}
		}
	};
	
	$scope.handleClick =  function(FROMin, data){
		  $scope.procField = FROMin;
		  var field = $('#'+FROMin);
		  var compileTxt = "";
		  if(field.attr("type") != "input"){
			  field.attr("type","input");
			  if(FROMin == "StartDate" || FROMin == "EndDate"){
				  if(FROMin == "StartDate"){
					  if($scope.Process.ActStartDate != ""){
						  $scope.dtS = new Date($scope.Process.ActStartDate);
					  } else {
						  $scope.dtS = new Date($scope.Process.EstStartDate);
					  }
				  } else {
					  $scope.dtS = new Date($scope.Process.EstCompDate);
				  }
				  compileTxt = "<input type='date' id='"+FROMin+"sub' ng-model='dtS' is-open='true' style='color:black;' ng-blur='updateProcDate()'/></input>";
			  } else {
				  switch(FROMin){
				  	case "ProcessType" :
				  		for(var i = 0; i < $scope.ProcTypes.length; i ++){
				  			if($scope.ProcTypes[i].TKTMPNM == data){
				  				$scope.proctype = $scope.ProcTypes[i];
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='proctype' ng-options='opt as opt.TKTMPNM for opt in ProcTypes' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "ProcessSubType" :
				  		for(var i = 0; i < $scope.SubProcTypes.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubProcTypes[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubProcTypes[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubProcTypes' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "Frequency" :
				  		for(var i = 0; i < $scope.Frequencies.length; i ++){
				  			if(data != undefined){
				  				if($scope.Frequencies[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.Frequencies[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in Frequencies' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "Category" :
				  		for(var i = 0; i < $scope.Cats.length; i ++){
				  			if(data != undefined){
				  				if($scope.Cats[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.Cats[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in Cats' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory" :
				  		for(var i = 0; i < $scope.SubCats.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCats[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCats[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCats' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory2" :
				  		for(var i = 0; i < $scope.SubCat2s.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCat2s[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCat2s[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCat2s' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	case "SubCategory3" :
				  		for(var i = 0; i < $scope.SubCat3s.length; i ++){
				  			if(data != undefined){
				  				if($scope.SubCat3s[i].SIFFUL == data.trim()){
				  					$scope.pinfo = $scope.SubCat3s[i];
				  				}
				  			}
				  		}
				  		compileTxt = "<select id='"+FROMin+"sub' style='width:100%;color:black;' ng-model='pinfo' ng-options='opt as opt.SIFFUL for opt in SubCat3s' ng-blur='removeDropdown()'/></select>";
				  		break;
				  	default :
				  		compileTxt = "<input id='"+FROMin+"sub' value='"+data+"' style='width:100%;color:black;' ng-blur='removeClick()'/></input>"; 
				  		break;
				  }
			  }
			  var compileDone = $compile(compileTxt)($scope);
			  field.html(compileDone);
		  }
	  };
	  
	  $scope.removeClick = function(){
		  var field = $('#'+ $scope.procField);
		  var subField = $('#'+ $scope.procField+'sub');
		  field.attr("type","");
		  var text = subField.val();
		  for(var i = 0; i < $scope.fields.length; i++){
			  var string = $scope.fields[i].DFNAMID.replace(/\s/g, '');
			  if(string == $scope.procField){
				  $scope.Process[string] = text;
			  }
		  }
		  for(var i = 0; i < $scope.ufields.length; i++){
			  var string = $scope.ufields[i].UNAMID.replace(/\s/g, '');
			  if(string ==  $scope.procField){
				  var found = false;
				  for(var k = 0; k < $scope.Process.UDFs.length; k++){
  					if($scope.Process.UDFs[k].DSEQ == $scope.ufields[i].USEQ){
  						found = true;
  						$scope.Process.UDFs[k].DATA = text;
  						$scope.Process.UDFs[k].Mode = "U";
  					}
				  }
				  if(!found){
					  $scope.tmp = {
							  	DTKNO: $scope.Process.TKNO,
					    		DTSKNO: 0,
					    		DTSKSNO: 0,
					    		DSEQ: 0,
					    		DATA: text,
					    		Mode: "I"
					  }
					  $scope.Process.UDFs.push($scope.tmp);
				  }
			  }
		  }
		  subField.remove();
		  field.html(text);
		  $scope.procField = "";
	  };
	  
	  $scope.removeDropdown = function(){
		  var field = $('#'+ $scope.procField);
		  var subField = $('#'+ $scope.procField+'sub');
		  field.attr("type","");
		  var text = "";
		  if($scope.procField == "ProcessType"){
			  text = $scope.proctype.TKTMPNM;
		  } else {
			  text = $scope.pinfo.SIFFUL;
		  }
		  $scope.Process[$scope.procField] = text;
		  subField.remove();
		  field.html(text);
	  };
	  
	  $scope.updateProcDate = function(){
		  var field = $('#'+$scope.procField);
		  var subField = $('#'+$scope.procField+'sub');
		  field.attr("type","");
		  var text = $filter('date')($scope.dtS);
		  if($scope.procField == "StartDate"){
			  $scope.Process.ActStartDate = $scope.dtS;
		  } else {
			  $scope.Process.EstCompDate = $scope.dtS;
		  }
		  subField.remove();
		  field.html(text);
		  $scope.procField = "";
	  };
	  	
	  $scope.ok = function () {
		  $uibModalInstance.close($scope.Process);
	  };

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };
	  
	  $scope.getAddlDispNums = function(){
		  return new Array($scope.numAddl);  
	  };
	  
	  $scope.getAddlName = function(number){
		  var newNum = number+1;
		  var appender = "addl"+newNum;
		  return $scope[appender];
	  };
	
});