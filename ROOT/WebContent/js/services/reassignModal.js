angular.module('cobraApp').controller('ReassignModalCtrl', function ($scope, $filter, $uibModalInstance, sessionStorage) {
	
	$scope.selected = "";
	$scope.jobfunctions = [];
	$scope.jobfunction = {
			  role: "",
			  name: ""
	  };
	$scope.jobfunctionsMaster = [];
	
	$scope.jobfunctions.push("Select Users...");
	$scope.jobfunctions.push("");
	  
	$.getJSON( "./rest/ProcessTaskService/getJobFunctions/" + sessionStorage.datasource +  "/" + sessionStorage.userid + "/" + sessionStorage.sessionID + "/" + sessionStorage.sessionToken + "/" + sessionStorage.sessionTimeout, function( data ) {	  
		$.each( data, function( key, val ) {	  	 		 
	 		
			$scope.jobfunction = {
					role: val.KAJOBROLE,
					name: val.DisplayText
			};
			$scope.jobfunctionsMaster.push($scope.jobfunction);
			var jobfunction = val.DisplayText;
		 	$scope.jobfunctions.push(jobfunction);
		 	
	 	  });
		$scope.$apply();
	  });
	  	
	  $scope.ok = function () {
		  
		  var JSONResponse = "";
		  for(var key in $scope.jobfunctionsMaster){
			  if($scope.selected == $scope.jobfunctionsMaster[key].name){
				  JSONResponse = $scope.jobfunctionsMaster[key].role;
				  break;
			  }
		  }
		  
		  $uibModalInstance.close(JSONResponse);
	  };

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };
	
});