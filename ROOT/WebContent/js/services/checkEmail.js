angular.module('cobraApp').controller('CheckEmailModalCtrl', function ($scope, $filter, $uibModalInstance, processTask) {
	
	$scope.user = processTask.Assigned;
	$scope.task = processTask.TaskName;

	$scope.$apply();
	  	
	  $scope.ok = function () {
		  $uibModalInstance.close();
	  };

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };
	  
}).controller('SubProcModalCtrl', function ($scope, $filter, $modalInstance) {
	  
	$scope.ok = function () {
		$modalInstance.close(true);
	};

	$scope.cancel = function () {
		$modalInstance.close(false);
	};
	  
});