angular.module('cobraApp').controller('CreateNewModalCtrl', function ($scope, $filter, $uibModalInstance, editwidget, sessionStorage) {

	$scope.relatedstrings = [];
	  $scope.editwidget = editwidget;
	  $scope.openedStart = false;
	  $scope.openedEnd = false;
	  $scope.openedRemind = false;
	  $scope.openedRecur = false;
	  $scope.isProcess = false;
	  $scope.isProcessReady = false;
	  $scope.isTaskReady = false;
	  $scope.recur = false;
	  $scope.whereToP = "";
	  $scope.whereToT = "";
	  
	  $scope.createrecord = {}
	  $scope.mytitle = {value:""};
	  $scope.myprocesstype = {value:""};
	  $scope.mytasktype = {value:""};
	  $scope.processtypes = [];
	  $scope.tasktypes = [];
	  $scope.jobfunctions =[];
	  $scope.myjobfunction = {value:""};
	  $scope.groups = {};
	  $scope.columns = [];
	  $scope.frequencies = [];
	  $scope.myfrequency = {value:""};
	  $scope.myinterval = "";
	  $scope.mymonth = "";
	  $scope.myday = "";
	  $scope.numrequired = 1;
	  
	  $scope.descriptions = [];
	  $scope.PRELDESCs = [];
	  $scope.UDFs = [];
	  $scope.UDFData = [];
	  
	  $scope.taskerror = "";
	  $scope.processerror = "";
	  
	  var blank = "0";
	  
	  $scope.dtS = new Date();
	  $scope.dtE = undefined;
	  $scope.dtR = undefined;
	  $scope.dtRS = undefined;
	  $scope.format = "MM/dd/yyyy";
	  
	  $.getJSON( "./rest/ProcessTaskService/getEditWidget/" + sessionStorage.datasource +  "/" + sessionStorage.userid + "/" + sessionStorage.sessionID + "/" + sessionStorage.sessionToken + "/" + sessionStorage.sessionTimeout + "/" + blank, function( data ) {
		  
		  $scope.jobfunctions = data[0];
		  $scope.groups = data[1];
		  $scope.processtypes = data[2];
		  $scope.categories = data[3];
		  $scope.PRELDESCs = data[4];
		  $scope.tasktypes = data[5];
		  $scope.UDFs = data[8];
		  $scope.frequencies = data[9];
		  
		  for(var i = 0; i < $scope.jobfunctions.length; i++){
			  if($scope.jobfunctions[i].KAJOBROLE == $scope.editwidget.Assigned){
				  $scope.myjobfunction.value = $scope.jobfunction;
			  }
		  }
		  for(var i = 0; i < $scope.PRELDESCs.length; i++){
			  $scope.descriptions.push($scope.PRELDESCs[i].PRDESC);
		  }
		  
		  for(var i = 0; i < $scope.UDFs.length; i++){
			  $scope.UDFs[i].sort = $scope.UDFs[i].UNAMID.replace(/\s/g, '');
		  }
	  		$scope.$apply();
	  	});
	  
	  $scope.setProc = function(valin){
		  if(valin == true){
			  $scope.isProcess = true;
			  $scope.whereToP = "true";
			  $scope.whereToT = "false";
		  } else {
			  $scope.isProcess = false;
			  $scope.whereToP = "false";
			  $scope.whereToT = "true";
		  }
	  };
	  
	  $scope.setVal = function(valin){
		  $scope.recur = valin;
	  };
	  
	  $scope.loadProcess = function(){
		  $scope.isTaskReady = false;
		  $scope.isProcessReady = true;
		  $scope.loadTags();
	  };
	  
//	  $scope.getDispNums = function(){
//		  return new Array($scope.numRequired);  
//	  };
//	  
//	  $scope.getName = function(number){
//		  var newNum = number+1;
//		  var appender = "required"+newNum;
//		  return $scope[appender];
//	  };
	  
	  $scope.createNewSubmit = function () {

		  var dateOutS = $filter('date')($scope.dtS,'yyyy-MM-dd');
		  var dateOutE;
		  var dateOutR;
		  var dateOutRS;
		  try {
		  	dateOutE = $filter('date')($scope.dtE,'yyyy-MM-dd');
		  	if(dateOutE == undefined){
		  		dateOutE = "";
		  	}
		  } catch(err){
			  dateOutE = "";
		  }
		  try {
			 dateOutR = $filter('date')($scope.dtR,'yyyy-MM-dd');
			 if(dateOutR == undefined){
				 dateOutR = "";
			  }
		  } catch(err){
			dateOutR = "";
		  }
		  try {
			dateOutRS = $filter('date')($scope.dtRS,'yyyy-MM-dd');
			if(dateOutRS == undefined){
				dateOutRS = "";
			}
		  } catch(err){
			  dateOutRS = "";
		  }
		  var type = "";
		  var jf = "";
		  var valid = true;
		  if($scope.myjobfunction.value != undefined){
			  jf = $scope.myjobfunction.value.KAJOBROLE;
		  }
		  if(!$scope.isProcess){
			  if($scope.mytasktype.value.TSTYPE == undefined){
				  valid = false;
				  $scope.taskerror = "Please select a task type."
			  }
			  type = "T";
			  $scope.createrecord = {
	    				Title: fixedEncodeURIComponent($scope.mytitle.value),
	    				PorT: type,
	  				  	Type: $scope.mytasktype.value.TSTYPE,
	  				  	Assigned: jf,
	  				  	Start: dateOutS,
	  				  	Due: dateOutE,
	  				  	Remind: dateOutR
	  		  	};
	    	} else{
	    		if($scope.myprocesstype.value.TKTYPE == undefined){
					  valid = false;
					  $scope.processerror = "Please select a process type."
				  }
	    		type = "P";
	    		$scope.createrecord = {
	    				Title: fixedEncodeURIComponent($scope.mytitle.value),
	    				PorT: type,
	  				  	Type: $scope.myprocesstype.value.TKTYPE,
	  				  	Assigned: jf,
	  				  	Start: dateOutS,
	  				  	Due: dateOutE,
	  				  	Remind: dateOutR
	  		  	};
	    	}
		  var currentTags = $("#createTags").tagit("assignedTags");
		  var DescsOut = [];
		  for(var i = 0; i < currentTags.length; i++){
			  for(var key in $scope.PRELDESCs){
				  if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
					  DescsOut.push($scope.PRELDESCs[key]);
					  break;
				  }
			  }
		  }
		  for(var i = 0; i < $scope.UDFs.length; i++){
			  var field = $("#"+ $scope.UDFs[i].UNAMID.replace(/\s/g, ''));
			  if(field.val() != undefined){
				  $scope.tmp = {
						 DSEQ: $scope.UDFs[i].USEQ,
						 DATA: field.val()
				  };
				  $scope.UDFData.push($scope.tmp);
			  }
		  }
		  if(valid){
			  $scope.taskerror = "";
			  $scope.processerror = "";
			  var JSONResponse = [];
			  var JSONResponse1 = angular.toJson($scope.createrecord);
			  JSONResponse.push(JSONResponse1);
			  var JSONResponse2 = angular.toJson(DescsOut);
			  JSONResponse.push(JSONResponse2);
			  var JSONResponse3 = angular.toJson($scope.UDFData);
			  JSONResponse.push(JSONResponse3);
			  $uibModalInstance.close(JSONResponse);
		  }
		  
	  };
	  
	  function fixedEncodeURIComponent (str) {  
	    	return encodeURIComponent(str).replace(/[!'()*]/g, escape);  
	    } 

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };
	  
	  $scope.selectTask = function(){
		  $scope.isTaskReady = true;
		  $scope.isProcessReady = false;
		  try{
			  var someDate = new Date();
			  someDate.setDate($scope.dtS.getDate() + $scope.mytasktype.TSDYCMP);
			  $scope.dtE = someDate;
			  $scope.dtR = someDate;
		  } catch(err) {}
		  $scope.loadTags();
	  };
	  
	  $scope.loadTags = function(){
		  setTimeout(function(){
			  $("#createTags").tagit({
			  	availableTags: $scope.descriptions,
			  	onlyAvailableTags: true
			  });
		  }, 250);
	  };
	  
	  $scope.resetEnd = function() {
		  if(!$scope.isProcess){
			  try{
				  var someDate = new Date();
				  someDate.setDate($scope.dtS.getDate() + $scope.mytasktype.TSDYCMP);
				  $scope.dtE = someDate;
				  $scope.dtR = someDate;
			  } catch(err){}
		  }
	  };
	  
	  $scope.openStartCal = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.openedStart = true;
		  };
	$scope.openEndCal = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		if($scope.PorT.type == "Task"){
			$scope.openedEnd = true;
		}
	};
	
	$scope.openRemindCal = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		if($scope.PorT.type == "Task"){
			$scope.openedRemind = true;
		}
	};
	
	$scope.openRecurCal = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openedRecur = true;
	};
	
	$scope.updateRemind = function(){
		$scope.dtR = $scope.dtE;
	};
	
	$scope.setValid = function() {
		var dateOutRS = $filter('date')($scope.dtRS,'yyyy-MM-dd');
		if(dateOutRS == null){
			$scope.dtRS = Date.parse("01/01/1900");
		}
		if(!$scope.isProcess){
			var dateOutE = $filter('date')($scope.dtE,'yyyy-MM-dd');
			if(dateOutE == null){
				$scope.dtE = Date.parse("01/01/1900");
			}
			
			var dateOutR = $filter('date')($scope.dtR,'yyyy-MM-dd');
			if(dateOutR == null){
				$scope.dtR = Date.parse("01/01/1900");
			}
		}
	};
	
});